pragma solidity ^0.8.0;
contract Example {
    mapping(address => uint8) balances;

    constructor() {
        balances[msg.sender] = 10;
    }

    function transfer(address from, address to, uint8 val) public {
        require(balances[from] >= val);
        balances[from] -= val;
        balances[to] += val;
    }
}
