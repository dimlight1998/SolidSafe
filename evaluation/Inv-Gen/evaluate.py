import sys
import shutil
import tempfile
from loguru import logger
import re
from datetime import datetime
import subprocess
from typing import Literal, Union
import pathlib
import os

DIR = os.path.dirname(os.path.realpath(__file__))

# configuration part
TIMEOUT_SEC = 1800
SOLIDSAFE_JAR = os.path.join(
    DIR, "../../target/scala-3.3.1/solidsafe-assembly-0.1.0-SNAPSHOT.jar"
)
VERISOL = "/home/zhang/.bin/VeriSol"
SMT_CHECKER = os.path.join(DIR, "../../env/solc-static-linux")

Tag = Literal["arith", "assert"]

Result = Union[
    tuple[Literal["verified"], float],
    tuple[Literal["bugfound"], float],
    tuple[Literal["unknown"], float],
    Literal["timeout"],
    Literal["internalerror"],
]


def run_solidsafe(sol_path: str, tag: Tag) -> Result:
    cmd = [
        "java",
        "-jar",
        SOLIDSAFE_JAR,
        sol_path,
        "-i",
        "--bmc",
        "--solc-exe",
        "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/solc-static-linux",
        "--boogie-exe",
        "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/boogie",
        "--corral-exe",
        "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/corral",
    ]
    if tag == "arith":
        cmd.append("-a")
    proc = None
    try:
        logger.info(f"start on {sol_path}")
        logger.info(f"cmd: {' '.join(cmd)}")
        ts_start = datetime.now()
        proc = subprocess.run(
            cmd, text=True, capture_output=True, check=True, timeout=TIMEOUT_SEC
        )
        ts_stop = datetime.now()
        duration = ts_stop - ts_start
        logger.success(proc.stdout)
        logger.error(proc.stderr)
        logger.info(f"finished with {duration.total_seconds()} secs")
        for line in proc.stdout.splitlines():
            if "verified" in line:
                return ("verified", duration.total_seconds())
            elif "unknown" in line:
                return ("unknown", duration.total_seconds())
            elif "bug found" in line:
                return ("bugfound", duration.total_seconds())
        return "internalerror"
    except subprocess.TimeoutExpired:
        logger.warning("TIMEOUT")
        return "timeout"
    except subprocess.CalledProcessError:
        assert proc is not None
        logger.error(proc.stdout)
        return "internalerror"


def run_smt_checker(sol_path: str, tag: Tag) -> Result:
    cmd = [
        SMT_CHECKER,
        "--model-checker-engine=all",
        "--model-checker-invariants=contract",
        "--model-checker-show-unproved",
        "--model-checker-bmc-loop-iterations=2",
        sol_path,
    ]
    if tag == "arith":
        cmd.append("--model-checker-targets=underflow,overflow")
    elif tag == "assert":
        cmd.append("--model-checker-targets=assert")
    proc = None
    try:
        logger.info(f"start on {sol_path}")
        ts_start = datetime.now()
        proc = subprocess.run(
            cmd, text=True, capture_output=True, check=True, timeout=TIMEOUT_SEC
        )
        ts_stop = datetime.now()
        duration = ts_stop - ts_start
        logger.success(proc.stdout)
        logger.error(proc.stderr)
        logger.info(f"finished with {duration.total_seconds()} secs")
        for line in proc.stderr.splitlines():
            if re.match(r"^Warning: BMC.*", line):
                return ("bugfound", duration.total_seconds())
        verified = True
        for line in proc.stderr.splitlines():
            if re.match(r"^Warning: CHC.*", line):
                verified = False
        if verified:
            return ("verified", duration.total_seconds())
        else:
            return ("unknown", duration.total_seconds())
    except subprocess.TimeoutExpired as proc:
        logger.warning("TIMEOUT")
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        return "timeout"
    except subprocess.CalledProcessError as proc:
        logger.error(proc.returncode)
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        return "internalerror"


def run_verismart(sol_path: str, tag: Tag) -> Result:
    sol_path = sol_path.rstrip(".base.sol") + ".verismart.sol"
    temp = tempfile.mktemp(suffix=".sol")
    shutil.copy(sol_path, temp)
    logger.info(f"copied to {temp}")
    cmd = [
        "sudo",
        "docker",
        "run",
        "-v",
        "/tmp:/ws",
        "--rm",
        "verismart:latest",
        "-input",
        "/ws" + temp[4:],
    ]
    if sol_path.endswith(".bug.verismart.sol"):
        cmd.extend(["-mode", "exploit", "-exploit_timeout", "90"])
    if tag == "arith":
        cmd.append("io")
    else:
        cmd.append("assert")
    proc = None
    try:
        logger.info(f"start on {sol_path}")
        ts_start = datetime.now()
        proc = subprocess.run(
            cmd, text=True, capture_output=True, check=True, timeout=TIMEOUT_SEC
        )
        ts_stop = datetime.now()
        duration = ts_stop - ts_start
        logger.success(proc.stdout)
        logger.error(proc.stderr)
        logger.info(f"finished with {duration.total_seconds()} secs")
        for line in proc.stdout.splitlines():
            if match := re.match(r"^\# Alarm / Query\s+:\s*(\d+)\s*/\s*\d+", line):
                if int(match.group(1)) == 0:
                    return ("verified", duration.total_seconds())
                else:
                    return ("unknown", duration.total_seconds())
        return "internalerror"
    except subprocess.TimeoutExpired as proc:
        logger.warning("TIMEOUT")
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        return "timeout"
    except subprocess.CalledProcessError as proc:
        logger.error(proc.returncode)
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        return "internalerror"


def run_verisol(sol_path: str, tag: Tag) -> Result:
    sol_path = sol_path.rstrip(".base.sol") + ".verisol.sol"
    lines = pathlib.Path(sol_path).read_text().splitlines()
    contract_name = None
    for line in reversed(lines):
        if m := re.match(r"^contract (\w+)", line):
            contract_name = m.group(1)
            break
    cmd = [VERISOL, sol_path, contract_name, "/contractInfer"]
    proc = None
    ts_start = None
    try:
        logger.info(f"start on {sol_path}")
        ts_start = datetime.now()
        proc = subprocess.run(
            cmd, text=True, capture_output=True, check=True, timeout=TIMEOUT_SEC
        )
        ts_stop = datetime.now()
        duration = ts_stop - ts_start
        logger.success(proc.stdout)
        logger.error(proc.stderr)
        logger.info(f"finished with {duration.total_seconds()} secs")
        proof_found = None
        bmc_success = None
        for line in proc.stdout.splitlines():
            if "Did not find a proof" in line:
                proof_found = False
            elif "Formal Verification successful upto" in line:
                bmc_success = True
        if not proof_found and bmc_success:
            return ("unknown", duration.total_seconds())
        elif not proof_found and not bmc_success:
            return ("bugfound", duration.total_seconds())
        return "internalerror"
    except subprocess.TimeoutExpired as proc:
        logger.warning("TIMEOUT")
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        subprocess.run("killall z3.exe", shell=True)
        return "timeout"
    except subprocess.CalledProcessError as proc:
        logger.error(proc.returncode)
        logger.warning(proc.stdout)
        logger.error(proc.stderr)
        for line in proc.stdout.splitlines():
            if "Found a counterexample" in line:
                ts_stop = datetime.now()
                assert ts_start is not None
                return ("bugfound", (ts_stop - ts_start).total_seconds())
        return "internalerror"


ToolName = Literal["SolidSafe", "VeriSmart", "VeriSol", "SMT-Checker"]


def run_tool_on(tool_name: ToolName, sol_path: str, tag: Tag) -> Result:
    result = None
    match tool_name:
        case "SolidSafe":
            result = run_solidsafe(sol_path, tag)
        case "VeriSmart":
            result = run_verismart(sol_path, tag)
        case "VeriSol":
            result = run_verisol(sol_path, tag)
        case "SMT-Checker":
            result = run_smt_checker(sol_path, tag)
        case _:
            assert False
    logger.info(f"summary: {tool_name}, {sol_path}, {result}")
    return result


def evaluate_on(dir: str, tag: Tag) -> list[tuple[str, int, Result]]:
    sorted_files = sorted(os.listdir(dir))
    results: list[tuple[str, int, Result]] = []
    for file in sorted_files:
        if file.endswith("bug.base.sol"):
            file_path = os.path.join(dir, file)
            lines = pathlib.Path(file_path).read_text().splitlines()
            # results.append((file_path, len(lines), run_tool_on("SolidSafe", file_path, tag)))
            results.append((file_path, len(lines), run_tool_on("VeriSmart", file_path, tag)))
            # results.append((file_path, len(lines), run_tool_on("VeriSol", file_path, tag)))
            # results.append((file_path, len(lines), run_tool_on("SMT-Checker", file_path, tag)))
    return results


if __name__ == "__main__":
    logger.add("invgen-{time}.log")
    dir = os.path.dirname(os.path.realpath(__file__))
    combined = []
    combined.extend(evaluate_on(os.path.join(dir, "CVE"), "arith"))
    # combined.extend(evaluate_on(os.path.join(dir, "VeriSmart"), "arith"))
    # combined.extend(evaluate_on(os.path.join(dir, "Other"), "assert"))
    from rich import print

    print(combined)
