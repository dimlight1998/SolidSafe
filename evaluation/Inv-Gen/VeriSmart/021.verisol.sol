pragma solidity ^0.5.0;

contract owned {
    address public owner;

    constructor() public {
        owner = msg.sender;
    }

    modifier onlyOwner {
        if (msg.sender != owner) revert();
        _;
    }

    function transferOwnership(address newOwner) public onlyOwner {
        owner = newOwner;
    }
}

interface tokenRecipient { function sendApproval(address _from, uint256 _value, address _token) external; }

contract MyToken is owned { 
    /* Public variables of the token */
    string public name;
    string public symbol;
    uint8 public decimals;
	uint8 public disableconstruction;
    /* This creates an array with all balances */
    mapping (address => uint256) public balanceOf;

    /* This generates a public event on the blockchain that will notify clients */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /* Initializes contract with initial supply tokens to the creator of the contract */
    function MyTokenLoad(uint256 initialSupply, string memory tokenName, uint8 decimalUnits, string memory tokenSymbol, address centralMinter) public {
		if(disableconstruction != 2){
            if(centralMinter != address(0) ) owner = msg.sender;         // Sets the minter
            balanceOf[msg.sender] = initialSupply;              // Give the creator all initial tokens                    
            name = tokenName;                                   // Set the name for display purposes     
            symbol = tokenSymbol;                               // Set the symbol for display purposes    
            decimals = decimalUnits;                            // Amount of decimals for display purposes        
		}
    }
    constructor() public {
        MyTokenLoad(10000000000000,'Kraze',uint8(8),'KRZ',address(0));
		disableconstruction=2;
    }
    /* Send coins */
    function transfer(address _to, uint256 _value) public {
        if (balanceOf[msg.sender] < _value) revert();           // Check if the sender has enough   
        assert(balanceOf[_to] + _value <= 115792089237316195423570985008687907853269984665640564039457584007913129639935);
        if (balanceOf[_to] + _value < balanceOf[_to]) revert(); // Check for overflows
        balanceOf[msg.sender] -= _value;                     // Subtract from the sender
        balanceOf[_to] += _value;                            // Add the same to the recipient            
        emit Transfer(msg.sender, _to, _value);                   // Notify anyone listening that this transfer took place
    }

    /* This unnamed function is called whenever someone tries to send ether to it */
    function() external payable {
        revert();     // Prevents accidental sending of ether
    }
}
