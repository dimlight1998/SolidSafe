pragma solidity ^0.5.0;

contract testingToken {
	mapping (address => uint256) public balanceOf;
	address public owner;
	constructor() public {
		owner = msg.sender;
		balanceOf[msg.sender] = 1000;
	}
	function send(address _to, uint256 _value) public {
		if (balanceOf[msg.sender]<_value) revert();
		assert(balanceOf[_to] + _value <= 115792089237316195423570985008687907853269984665640564039457584007913129639935);
		if (balanceOf[_to]+_value<balanceOf[_to]) revert();
		if (_value<0) revert();
		balanceOf[msg.sender] -= _value;
		balanceOf[_to] += _value;
	}
}
