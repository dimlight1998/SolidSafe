pragma solidity ^0.5.0;

interface ERC20Constant {
    function totalSupply() external returns (uint supply);
    function balanceOf( address who ) external returns (uint value);
    function allowance(address owner, address spender) external returns (uint _allowance);
}
interface ERC20Stateful {
    function transfer( address to, uint value) external returns (bool ok);
    function transferFrom( address from, address to, uint value) external returns (bool ok);
    function approve(address spender, uint value) external returns (bool ok);
}
interface ERC20Events {
    event Transfer(address indexed from, address indexed to, uint value);
    event Approval( address indexed owner, address indexed spender, uint value);
}
contract ERC20 is ERC20Constant, ERC20Stateful, ERC20Events {}

contract ERC20Base is ERC20
{
    mapping( address => uint ) _balances;
    mapping( address => mapping( address => uint ) ) _approvals;
    uint _supply;
    constructor( uint initial_balance ) public {
        _balances[msg.sender] = initial_balance;
        _supply = initial_balance;
    }
    function totalSupply() public returns (uint supply) {
        return _supply;
    }
    function balanceOf( address who ) public returns (uint value) {
        return _balances[who];
    }
    function transfer( address to, uint value) public returns (bool ok) {
        if( _balances[msg.sender] < value ) {
            revert();
        }
        if( !safeToAdd(_balances[to], value) ) {
            revert();
        }
        _balances[msg.sender] -= value;
        _balances[to] += value;
        emit Transfer( msg.sender, to, value );
        return true;
    }
    function transferFrom( address from, address to, uint value) public returns (bool ok) {
        // if you don't have enough balance, throw
        if( _balances[from] < value ) {
            revert();
        }
        // if you don't have approval, throw
        if( _approvals[from][msg.sender] < value ) {
            revert();
        }
        if( !safeToAdd(_balances[to], value) ) {
            revert();
        }
        // transfer and return true
        _approvals[from][msg.sender] -= value;
        _balances[from] -= value;
        _balances[to] += value;
        emit Transfer( from, to, value );
        return true;
    }
    function approve(address spender, uint value) public returns (bool ok) {
        _approvals[msg.sender][spender] = value;
        emit Approval( msg.sender, spender, value );
        return true;
    }
    function allowance(address owner, address spender) public returns (uint _allowance) {
        return _approvals[owner][spender];
    }
    function safeToAdd(uint a, uint b) internal pure returns (bool) {
        assert(a + b <= 115792089237316195423570985008687907853269984665640564039457584007913129639935);
        return (a + b >= a);
    }
}

contract Token is ERC20Base(1000000000) {
    
    string public name = "SNGLS Test Token";
    string public symbol = "SNGLS";
    uint public decimals = 0;

}
