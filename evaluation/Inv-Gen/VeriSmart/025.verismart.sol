pragma solidity ^0.7.0;

contract testingToken {
	mapping (address => uint256) public balanceOf;
	address public owner;
	constructor() {
		owner = msg.sender;
		balanceOf[msg.sender] = 1000;
	}
	function send(address _to, uint256 _value) public {
		if (balanceOf[msg.sender]<_value) revert();
		if (balanceOf[_to]+_value<balanceOf[_to]) revert();
		if (_value<0) revert();
		balanceOf[msg.sender] -= _value;
		balanceOf[_to] += _value;
	}
}
