pragma solidity ^0.7.0;
contract C
{
  uint a;

  constructor () {}

  function a1() public { a = 1; }

  function a2() public { a = 2; }

  function a3() public { a = 3; }

  function a4() public { a = 4; }

  function plusA(uint x) public view returns (uint) {
    require(x < 1000);
    assert(a + x <= 1000000);
    return a + x;
  }
}
