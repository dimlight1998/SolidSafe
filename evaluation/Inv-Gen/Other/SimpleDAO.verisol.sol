pragma solidity ^0.5.0;

contract SimpleDAO {
  mapping (address => uint) public balances;

  constructor() public {
    require(address(this).balance >= 0);
  }

  function donate(address to) public payable {
    balances[to] += msg.value;
  }

  function withdraw(uint amount) public {
    require(msg.sender != address(this) && balances[msg.sender] >= amount);
    balances[msg.sender] -= amount;
    msg.sender.transfer(amount);
  }

  function observe(address x) public view {
    assert(address(this).balance >= balances[x]);
  }
}
