// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

// NOTE: this invariant is due to an imperfection of the translator
/// @notice invariant forall i: uint ::  (i >= 0 && i < pledgedAmount.length) || #SUM#(pledgedAmount[i]) == 0
contract CrowdFund {
    struct Campaign {
        // Creator of campaign
        address creator;
        // Amount of tokens to raise
        uint goal;
        // Total amount pledged
        uint pledged;
        // Timestamp of start of campaign
        uint startAt;
        // Timestamp of end of campaign
        uint endAt;
        // True if goal was reached and creator has claimed the tokens.
        bool claimed;
    }

    address public token;
    // Mapping from id to Campaign
    Campaign[] public campaigns;
    // Mapping from campaign id => pledger => amount pledged
    mapping(address => uint)[] public pledgedAmount;

    function IERC20_transfer(address, address, uint) internal returns (bool) { }
    function IERC20_transferFrom(address, address, address, uint) internal returns (bool) { }

    constructor() {
        address _token = 0x1111122222333334444411111222223333344444;
        token = _token;
    }

    function launch(uint _goal, uint _startAt, uint _endAt) external {
        require(_startAt >= block.timestamp, "start at < now");
        require(_endAt >= _startAt, "end at < start at");
        require(_endAt <= block.timestamp + 90 days, "end at > max duration");

        campaigns.push();
        campaigns[campaigns.length - 1].creator = msg.sender;
        campaigns[campaigns.length - 1].goal = _goal;
        campaigns[campaigns.length - 1].pledged = 0;
        campaigns[campaigns.length - 1].startAt = _startAt;
        campaigns[campaigns.length - 1].endAt = _endAt;
        campaigns[campaigns.length - 1].claimed = false;
        pledgedAmount.push();
    }

    function cancel(uint _id) external {
        require(campaigns[_id].creator != address(0));
        require(campaigns[_id].creator == msg.sender, "not creator");
        require(block.timestamp < campaigns[_id].startAt, "started");

        campaigns[_id].creator = address(0);
    }

    function pledge(uint _id, uint _amount) external {
        require(_id >= 0); // TODO: introduce in translator
        require(_id < campaigns.length);
        require(campaigns[_id].creator != address(0));
        require(block.timestamp >= campaigns[_id].startAt, "not started");
        require(block.timestamp <= campaigns[_id].endAt, "ended");

        campaigns[_id].pledged += _amount;
        pledgedAmount[_id][msg.sender] += _amount;
        IERC20_transferFrom(token, msg.sender, address(this), _amount);
    }

    function unpledge(uint _id, uint _amount) external {
        require(_id >= 0); // TODO: introduce in translator
        require(_id < campaigns.length);
        require(campaigns[_id].creator != address(0));
        require(block.timestamp <= campaigns[_id].endAt, "ended");

        campaigns[_id].pledged -= _amount;
        pledgedAmount[_id][msg.sender] -= _amount;
        IERC20_transfer(token, msg.sender, _amount);
    }

    function claim(uint _id) external {
        require(_id < campaigns.length);
        require(campaigns[_id].creator != address(0));
        require(campaigns[_id].creator == msg.sender, "not creator");
        require(block.timestamp > campaigns[_id].endAt, "not ended");
        require(campaigns[_id].pledged >= campaigns[_id].goal, "pledged < goal");
        require(!campaigns[_id].claimed, "claimed");

        campaigns[_id].claimed = true;
        IERC20_transfer(token, campaigns[_id].creator, campaigns[_id].pledged);
    }

    function refund(uint _id) external {
        require(_id >= 0); // TODO: introduce in translator
        require(_id < campaigns.length);
        require(campaigns[_id].creator != address(0));
        require(block.timestamp > campaigns[_id].endAt, "not ended");
        require(campaigns[_id].pledged < campaigns[_id].goal, "pledged >= goal");

        uint bal = pledgedAmount[_id][msg.sender];
        assert(bal < campaigns[_id].goal);
        campaigns[_id].pledged -= pledgedAmount[_id][msg.sender];
        pledgedAmount[_id][msg.sender] = 0;
        IERC20_transfer(token, msg.sender, bal);
    }
}
