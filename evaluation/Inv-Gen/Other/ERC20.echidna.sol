// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract ERC20 {
  mapping (address => uint) _balances;
  mapping (address => mapping (address => uint)) _allowed;
  uint _totalSupply;

  constructor() {
  }

  function totalSupply() public view returns (uint) {
    return _totalSupply;
  }

  function balanceOf(address owner) public view returns (uint) {
    assert (_balances[owner] <= _totalSupply);
    return _balances[owner];
  }

  function allowance(address owner, address spender) public view returns (uint) {
    return _allowed[owner][spender];
  }

  function transfer(address to, uint value) public returns (bool) {
    require(value <= _balances[msg.sender]);
    require(to != address(0));

    _balances[msg.sender] = _balances[msg.sender] - value;
    _balances[to] = _balances[to] + value;
    return true;
  }

  function approve(address spender, uint value) public returns (bool) {
    require(spender != address(0));

    _allowed[msg.sender][spender] = value;
    return true;
  }

  function transferFrom(address from, address to, uint value) public returns (bool) {
    require(value <= _balances[from]);
    require(value <= _allowed[from][msg.sender]);
    require(to != address(0));

    _balances[from] = _balances[from] - value;
    _balances[to] = _balances[to] + value;
    _allowed[from][msg.sender] = _allowed[from][msg.sender] - value;
    return true;
  }

  function increaseAllowance(address spender, uint addedValue) public returns (bool) {
    require(spender != address(0));

    _allowed[msg.sender][spender] = _allowed[msg.sender][spender] + addedValue;
    return true;
  }

  function decreaseAllowance(address spender, uint subtractedValue) public returns (bool) {
    require(spender != address(0));

    _allowed[msg.sender][spender] = _allowed[msg.sender][spender] - subtractedValue;
    return true;
  }

  function _mint(address account, uint amount) internal {
    require(account != address(0));
    _totalSupply = _totalSupply + amount;
    _balances[account] = _balances[account] + amount;
  }

  function _burn(address account, uint amount) internal {
    require(account != address(0));
    require(amount <= _balances[account]);

    _totalSupply = _totalSupply - amount;
    _balances[account] = _balances[account] - amount;
  }

  function _burnFrom(address account, uint amount) internal {
    require(amount <= _allowed[account][msg.sender]);

    _allowed[account][msg.sender] = _allowed[account][msg.sender] - amount;
    _burn(account, amount);
  }
}
