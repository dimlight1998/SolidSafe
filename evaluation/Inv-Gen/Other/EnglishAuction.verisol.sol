// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.5.0;

contract EnglishAuction {
    address public nft;
    uint public nftId;

    address payable public seller;
    uint public endAt;
    bool public started;

    address public highestBidder;
    uint public highestBid;
    uint public startingBid;
    mapping(address => uint) public bids;

    constructor(address _nft, uint _nftId, uint _startingBid) public {
        require(msg.sender != address(this));
        nft = _nft;
        nftId = _nftId;

        seller = msg.sender;
        startingBid = _startingBid;
        require(address(this).balance == 0);
    }

    function safeTransferFrom(address _nft, address from, address to, uint tokenId) private {}
    function transferFrom(address _nft, address from, address to, uint tokenId) private {}

    function start() external {
        require(!started, "started");
        require(msg.sender == seller, "not seller");

        transferFrom(nft, msg.sender, address(this), nftId);
        started = true;
        endAt = block.timestamp + 7 days;
    }

    function bid() external payable {
        require(started, "not started");
        require(block.timestamp < endAt, "ended");
        require(msg.value > startingBid, "value < starting");
        require(msg.value > highestBid, "value < highest");
        bids[highestBidder] += highestBid;
        highestBidder = msg.sender;
        highestBid = msg.value;
    }

    function withdraw() external {
        require(address(this) != msg.sender);
        uint bal;
        bal = bids[msg.sender];
        bids[msg.sender] = 0;
        msg.sender.transfer(bal);
    }

    function end() external {
        require(address(this) != msg.sender);
        require(started, "not started");
        require(block.timestamp >= endAt, "not ended");

        if (highestBidder != address(0)) {
            safeTransferFrom(nft, address(this), highestBidder, nftId);
            assert(address(this).balance >= highestBid);
            seller.transfer(highestBid);
            highestBid = 0;
        } else {
            safeTransferFrom(nft, address(this), seller, nftId);
        }
    }
}
