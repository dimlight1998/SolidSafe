pragma solidity ^0.5.0;
contract C
{
  uint a;

  constructor () public {}

  function a1() public { a = 1; }

  function a2() public { a = 2; }

  function a3() public { a = 3; }

  function a4() public { a = 4; }

  function plusA(uint x) public view returns (uint) {
    require(x < 1000);
    assert(a + x <= 115792089237316195423570985008687907853269984665640564039457584007913129639935);
    return a + x;
  }
}
