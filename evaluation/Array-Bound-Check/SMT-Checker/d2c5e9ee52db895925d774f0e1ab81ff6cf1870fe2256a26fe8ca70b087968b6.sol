// File: contracts/interfaces/ISingleStakeFactory.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
interface ISingleStakeFactory {
    event ContractCreated(address _newContract);
    event ManagerSet(address _farmManager);
    event FeeSet(uint256 newFee);
    event FeesRecovered(uint256 balanceRecovered);
    function getSingleStake(address creator, address stakingToken) external view returns(address);
    function allFarms(uint index) external view returns(address);
    function farmManager() external view returns(address);
    function getCreator(address farmAddress) external view returns(address);
    function createNewSingleStake(address _stakingTokenAddress,
        address[] memory _rewardTokenAddresses,
        uint256 _rewardsDuration,
        uint16 _depositFeeBps,
        uint16[] memory _withdrawalFeesBps,
        uint32[] memory _withdrawalFeeSchedule) external;
    function allFarmsLength() external view returns (uint);
    function setManager(address managerAddress) external;
    function overrideOwnership(address farmAddress) external;
    function setFee(uint256 newFee) external;
    function withdrawFees() external;
}
// File: @openzeppelin/contracts/token/ERC20/IERC20.sol
// OpenZeppelin Contracts (last updated v4.6.0) (token/ERC20/IERC20.sol)
pragma solidity ^0.8.0;
interface IERC20 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from,
        address to,
        uint256 amount) external returns (bool);
}
// File: contracts/interfaces/IStaking.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
interface IStaking {
    function stakingToken() external returns(IERC20);
    function totalSupply() external returns(uint256);
    function balances(address account) external returns(uint256);
    function stakeWithPermit(uint256 _amount, uint _deadline, uint8 _v, bytes32 _r, bytes32 _s) external returns (uint256);
    function stake(uint256 _amount) external returns (uint256);
    function withdraw(uint256 _amount) external returns (uint256);
    function exit() external;
    function recoverERC20(address _tokenAddress, address _recipient, uint256 _amount) external;
    // Emitted on staking
    event Staked(address indexed account, uint256 amount);
    // Emitted on withdrawal (including exit)
    event Withdrawn(address indexed account, uint256 amount);
    // Emitted on token recovery
    event Recovered(address indexed token, address indexed recipient, uint256 amount);
}
// File: contracts/interfaces/IStakingFee.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
interface IStakingFee is IStaking {
    function feesUnit() external returns(uint16);
    function maxFee() external returns(uint16);
    function withdrawalFeeSchedule(uint256) external returns(uint256);
    function withdrawalFeesBps(uint256) external returns(uint256);
    function depositFeeBps() external returns(uint256);
    function collectedFees() external returns(uint256);
    function userLastStakedTime(address user) external view returns(uint32);
    function depositFee(uint256 _depositAmount) external view returns (uint256);
    function withdrawalFee(address _account, uint256 _withdrawalAmount) external view returns (uint256);
    function recoverFees(address _recipient) external;
    function setFees(uint16 _depositFeeBps, uint16[] memory _withdrawalFeesBps, uint32[] memory _withdrawalFeeSchedule) external;
    // Emitted when fees are (re)configured    
    event FeesSet(uint16 depositFeeBps, uint16[] withdrawalFeesBps, uint32[] feeSchedule);
    // Emitted when a deposit fee is collected
    event DepositFeesCollected(address indexed user, uint256 amount);
    // Emitted when a withdrawal fee is collected
    event WithdrawalFeesCollected(address indexed user, uint256 amount);
    // Emitted when fees are recovered by governance
    event FeesRecovered(uint256 amount);
}
// File: contracts/interfaces/IStakingRewards.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
interface IStakingRewards is IStakingFee {
    function rewardTokens(uint256) external view returns(IERC20);
    function rewardTokenAddresses(address rewardAddress) external view returns(bool);
    function periodFinish() external view returns(uint256);
    function rewardsDuration() external view returns(uint256);
    function lastUpdateTime() external view returns(uint256);
    function rewardRates(address rewardAddress) external view returns(uint256);
    function rewardPerTokenStored(address rewardAddress) external view returns(uint256);
    // wallet address => token address => amount
    function userRewardPerTokenPaid(address walletAddress, address tokenAddress) external view returns(uint256);
    function rewards(address walletAddress, address tokenAddress) external view returns(uint256);
    function lastTimeRewardApplicable() external view returns (uint256);
    function rewardPerToken(address _tokenAddress) external view returns (uint256);
    function earned(address _tokenAddress, address _account) external view returns (uint256);
    function emitting() external view returns(bool);
    function getReward(address _tokenAddress, address _recipient) external;
    function getRewards(address _recipient) external;
    // Must send reward before calling this!
    function startEmission(uint256[] memory _rewards, uint256 _duration) external;
    function stopEmission(address _refundAddress) external;
    function recoverLeftoverReward(address _tokenAddress, address _recipient) external;
    function addRewardToken(address _tokenAddress) external;
    function rewardTokenIndex(address _tokenAddress) external view returns(int8);
    // Emitted when a reward is paid to an account
    event RewardPaid(address indexed token, address indexed account, uint256 reward);
    // Emitted when a leftover reward is recovered
    event LeftoverRewardRecovered(address indexed recipient, uint256 amount);
    // Emitted when rewards emission is started
    event RewardsEmissionStarted(uint256[] rewards, uint256 duration);
    // Emitted when rewards emission ends
    event RewardsEmissionEnded();
}
// File: contracts/interfaces/ISingleStakingRewards.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
interface ISingleStakingRewards is IStakingRewards {
}
// File: @openzeppelin/contracts/utils/Context.sol
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File: @openzeppelin/contracts/access/Ownable.sol
// OpenZeppelin Contracts (last updated v4.7.0) (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    function _checkOwner() internal view virtual {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: contracts/SingleStakeManager.sol
//
// Copyright (c) 2023 ElkLabs
// License terms: https://github.com/elkfinance/faas/blob/main/LICENSE
//
// Authors:
// - Seth <[emailprotected]>
// - Baal <[emailprotected]>
pragma solidity >=0.8.0;
contract SingleStakeManager is Ownable {
    ISingleStakeFactory public stakeFactory;
    event farmFactorySet(address _factoryAddress);
    event RewardsReceived(address _farm);
    constructor (address factoryAddress) {
        stakeFactory = ISingleStakeFactory(factoryAddress);
    }
    // Utility function for use by Elk in order to change the SingleStakingFactory if needed.
    function setFarmFactory(address factoryAddress) external onlyOwner {
        require(factoryAddress != address(0), "factoryAddress is the zero address");
        stakeFactory = ISingleStakeFactory(factoryAddress);
        emit farmFactorySet(factoryAddress);
    }
    // the known owner of the SingleStakingRewards contract it is trying to interact with.
    modifier checkOwnership(address singleStakeAddress) {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        _;
    }
    function startEmission(address singleStakeAddress, uint256[] memory _rewards, uint256 _duration) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).startEmission(_rewards, _duration);
    }
    // Stops the given farm's emissions and refunds any leftover reward token(s) to the msg.sender.
    function stopEmission(address singleStakeAddress) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).stopEmission(msg.sender);
    }
    function recoverLeftoverReward(address singleStakeAddress, address _tokenAddress) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).recoverLeftoverReward(_tokenAddress, msg.sender);
    }
    function addRewardToken(address singleStakeAddress, address _tokenAddress) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).addRewardToken(_tokenAddress);
    }
    // Ensures any unnecessary tokens are not lost if sent to the farm contract.
    function recoverERC20(address singleStakeAddress, address _tokenAddress, uint256 _amount) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).recoverERC20(_tokenAddress, msg.sender, _amount);
    }
    // Allows the farm owner to set the withdrawal and deposit fees to be used in the farm.
    function setFees(address singleStakeAddress, uint16 _depositFeeBps, uint16[] memory _withdrawalFeesBps, uint32[] memory _withdrawalFeeSchedule) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).setFees(_depositFeeBps, _withdrawalFeesBps, _withdrawalFeeSchedule);
    }
    function recoverFees(address singleStakeAddress) external {
        ISingleStakingRewards rewardsContract = ISingleStakingRewards(singleStakeAddress);
        address lpTokenAddress = address(rewardsContract.stakingToken());
        require(stakeFactory.getSingleStake(msg.sender, lpTokenAddress) == singleStakeAddress, "caller is not owner");
        ISingleStakingRewards(singleStakeAddress).recoverFees(msg.sender);
    }
    // Function for farm users to claim rewards from multiple farms at once.
    function multiClaim(address[] memory _farms) external {
        require(_farms.length < 30, "Too many contracts, use less than 30");
        for (uint i = 0; i < _farms.length; i++) {
require(i >= 0);
assert(i >= 0 && i < _farms.length);
            address farmAddress = address(_farms[i]);
            ISingleStakingRewards(farmAddress).getRewards(msg.sender);
            emit RewardsReceived(_farms[i]);
        }
    }
}
