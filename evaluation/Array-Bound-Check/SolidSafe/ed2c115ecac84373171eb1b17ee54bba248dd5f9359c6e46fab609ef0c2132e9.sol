// Sources flattened with hardhat v2.9.3 https://hardhat.org
// File @openzeppelin/contracts/utils/[emailprotected]
// SPDX-License-Identifier: MIT
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File @openzeppelin/contracts/access/[emailprotected]
// OpenZeppelin Contracts v4.4.1 (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File contracts/OnlyAdmin.sol
pragma solidity ^0.8.0;
contract OnlyAdmin is Ownable {
    uint256 public nAdmins = 0;
    address[] public adminArray;
    mapping (address => bool) public adminTeam;
    mapping (address => bool) public previouslyApproved;
    function approveAdmin (address _admin) public onlyOwner () {
        if (!previouslyApproved[_admin]) {
            previouslyApproved[_admin] = true;
            nAdmins += 1;
            adminArray.push(_admin);
        }
        adminTeam[_admin] = true;
    } 
    function revokeAdmin (address _admin) public onlyOwner () {
        adminTeam[_admin] = false;
    }
    function revokeAllAdmins () public onlyOwner () {
        for (uint256 i = 0; i < adminArray.length; i++) {
            adminTeam[adminArray[i]] = false;
        }
        nAdmins = 0;
        delete adminArray;
    }
    modifier onlyAdmin () {
        if (msg.sender == owner()) {
            _;
        } else {
            require(adminTeam[msg.sender], "Only callable by administrator accounts");
            _;
        }
    }
}
// File @openzeppelin/contracts/token/ERC20/[emailprotected]
// OpenZeppelin Contracts (last updated v4.5.0) (token/ERC20/IERC20.sol)
pragma solidity ^0.8.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from,
        address to,
        uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
// File contracts/TeamAlloc.sol
pragma solidity ^0.8.0;
interface IFASTStaking {
    function stake(uint256 amount, bool lock) external;
    function getReward() external;
}
contract TeamAlloc is OnlyAdmin {
    uint256 public lockedAlloc; // 1000000 == 100%
    uint256 public unlockedAlloc;
    IERC20 public yToken; 
    IFASTStaking public FASTStaking;
    address public teamAddress;
    address public protocolFund; 
    uint256 public yStakingRewards;
    constructor (address _yToken, 
        uint256 _lockedAlloc, 
        address _FASTStaking, 
        address _teamAddress, 
        address _protocolFund) OnlyAdmin () {
        require(_lockedAlloc <= 1000000, "TeamAlloc::constructor: _lockPercent must be < 1000000");
        yToken = IERC20(_yToken);
        lockedAlloc = _lockedAlloc;
        unlockedAlloc = 1000000 - lockedAlloc;
        FASTStaking = IFASTStaking(_FASTStaking);
        teamAddress = _teamAddress;
        protocolFund = _protocolFund;
    }
    function setTeamAddress (address _teamAddress) public onlyOwner () {
        teamAddress = _teamAddress;
    }
    function setProtocolFundAddress (address _protocolFund) public onlyOwner () {
        protocolFund = _protocolFund;
    }
    // allocate to project maintenance fund and lock
    function allocate () public {
        if (msg.sender == owner()) {
            uint256 _yBal = yToken.balanceOf(address(this)) - yStakingRewards;
            uint256 _protocolFundAmount = (_yBal * unlockedAlloc) / 1000000;
            uint256 _lockAmount = _yBal - _protocolFundAmount;
            // send y to protocolFund
            yToken.transfer(protocolFund, _protocolFundAmount);
            // lock team alloc 
            yToken.approve(address(FASTStaking), _lockAmount);
            FASTStaking.stake(_lockAmount, true);
        } else {
            require(adminTeam[msg.sender], "Only callable by administrator accounts");
            uint256 _yBal = yToken.balanceOf(address(this)) - yStakingRewards;
            uint256 _protocolFundAmount = (_yBal * unlockedAlloc) / 1000000;
            uint256 _lockAmount = _yBal - _protocolFundAmount;
            // send y to protocolFund
            yToken.transfer(protocolFund, _protocolFundAmount);
            // lock team alloc 
            yToken.approve(address(FASTStaking), _lockAmount);
            FASTStaking.stake(_lockAmount, true);
        }
    }
    // claim from staking
    function claimFromStaking () public {
        if (msg.sender == owner()) {
            uint256 _preClaimY = yToken.balanceOf(address(this));
            FASTStaking.getReward();
            uint256 _yClaimed = yToken.balanceOf(address(this)) - _preClaimY;
            yStakingRewards += _yClaimed;
        } else {
            require(adminTeam[msg.sender], "Only callable by administrator accounts");
            uint256 _preClaimY = yToken.balanceOf(address(this));
            FASTStaking.getReward();
            uint256 _yClaimed = yToken.balanceOf(address(this)) - _preClaimY;
            yStakingRewards += _yClaimed;
        }
    }
    // distribute yield (any ERC20)
    function distributeYield (address _token, uint256 _amount) public {
        if (msg.sender == owner()) {
            if (_token == address(yToken)) {
                require(_amount <= yStakingRewards, "TeamAlloc::distributeYield: can only claim y recieved as staking yield");
                yStakingRewards -= _amount;
            }
            IERC20(_token).transfer(teamAddress, _amount);
        } else {
            require(adminTeam[msg.sender], "Only callable by administrator accounts");
            if (_token == address(yToken)) {
                require(_amount <= yStakingRewards, "TeamAlloc::distributeYield: can only claim y recieved as staking yield");
                yStakingRewards -= _amount;
            }
            IERC20(_token).transfer(teamAddress, _amount);
        }
    }
    // distribute ETH yield
    receive() external payable {}
    function getEthBalance () public view returns (uint256) {
        return address(this).balance;
    }
    function distributeETH (uint256 _amount) public {
        if (msg.sender == owner()) {
            payable(teamAddress).transfer(_amount);
        } else {
            require(adminTeam[msg.sender], "Only callable by administrator accounts");
            payable(teamAddress).transfer(_amount);
        }
    }
}
