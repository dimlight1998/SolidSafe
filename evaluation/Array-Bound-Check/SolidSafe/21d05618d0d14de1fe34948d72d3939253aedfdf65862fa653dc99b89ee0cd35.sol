// File: @openzeppelin/contracts/utils/introspection/IERC165.sol
// OpenZeppelin Contracts v4.4.1 (utils/introspection/IERC165.sol)
pragma solidity ^0.8.0;
interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
// File: @openzeppelin/contracts/token/ERC721/IERC721.sol
// OpenZeppelin Contracts (last updated v4.6.0) (token/ERC721/IERC721.sol)
pragma solidity ^0.8.0;
interface IERC721 is IERC165 {
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId,
        bytes calldata data) external;
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId) external;
    function transferFrom(address from,
        address to,
        uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function setApprovalForAll(address operator, bool _approved) external;
    function getApproved(uint256 tokenId) external view returns (address operator);
    function isApprovedForAll(address owner, address operator) external view returns (bool);
}
// File: @openzeppelin/contracts/utils/Context.sol
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File: @openzeppelin/contracts/access/Ownable.sol
// OpenZeppelin Contracts v4.4.1 (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: contracts/applications/BoutiqueSwapper.sol
//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
contract BoutiqueSwapper is Ownable {
    uint256 public swapFee;
    uint256 public whitelistSwapFee;
    address public paymentSplitter;
    address public boutique;
    mapping(uint256 => uint256) public swaps;
    mapping(address => bool) public whitelist;
    constructor(uint256 _swapFee,
        uint256 _whitelistSwapFee,
        address _paymentSplitter,
        address _boutique) {
        setMetadata(_swapFee, _whitelistSwapFee, _paymentSplitter, _boutique);
    }
    function setMetadata(uint256 _swapFee,
        uint256 _whitelistSwapFee,
        address _paymentSplitter,
        address _boutique) public onlyOwner {
        swapFee = _swapFee;
        whitelistSwapFee = _whitelistSwapFee;
        paymentSplitter = _paymentSplitter;
        boutique = _boutique;
    }
    function setWhitelistAddresses(address[] memory userAddresses,
        bool isWhitelisted) public onlyOwner {
        for (uint256 i = 0; i < userAddresses.length; i++) {
            whitelist[userAddresses[i]] = isWhitelisted;
        }
    }
    function swap(uint256 tokenId) public payable virtual {
        require(msg.value == getSwapPrice(msg.sender), "Incorrect amount sent");
        require(IERC721(boutique).ownerOf(tokenId) == msg.sender,
            "Sender must be the owner of TokenId");
        swaps[tokenId] += 1;
        payable(paymentSplitter).transfer(msg.value);
    }
    function getSwapPrice(address userAddress) public view returns (uint256) {
        return whitelist[userAddress] ? whitelistSwapFee : swapFee;
    }
}