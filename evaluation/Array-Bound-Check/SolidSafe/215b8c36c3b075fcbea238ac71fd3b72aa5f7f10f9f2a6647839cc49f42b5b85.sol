// SPDX-License-Identifier: GPL-3.0-only
pragma solidity >=0.8.17 <0.9.0;
contract TransactionBatcher {
    event Sent(address target, uint256 value, bytes data, uint64 gasLimit);
    function batchSend(address[] memory targets, uint256[] memory values, bytes[] memory datas, uint64[] memory gasLimit) external {
        for (uint i = 0; i < targets.length; i++) {
            (bool success,) = targets[i].call{value: values[i], gas: gasLimit[i]}(datas[i]);
            require(success, "Failed to send transaction");
            emit Sent(targets[i], values[i], datas[i], gasLimit[i]);
        }
    }
}