// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.15;
// OpenZeppelin Contracts (last updated v4.7.0) (access/Ownable.sol)
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    function _checkOwner() internal view virtual {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// OpenZeppelin Contracts (last updated v4.6.0) (token/ERC20/IERC20.sol)
interface IERC20 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from,
        address to,
        uint256 amount) external returns (bool);
}
contract AirdropClaim is Ownable {
    mapping (address => uint256) public balances;
    IERC20 public govToken;
    bool public enabled;
    event Claimed(address recipient, uint256 amount);
    modifier isEnabled {
        require(enabled, "airdrop is not enabled");
        _;
    }
    constructor(address _address) {
        govToken = IERC20(_address);
    }
    function setAirdrops(address[] memory _recipients, uint256[] memory _amounts) onlyOwner public {
        require(_recipients.length > 0 && _recipients.length == _amounts.length);
        for (uint i = 0; i < _recipients.length; i++) {
            balances[_recipients[i]] += _amounts[i];
        }
    }
    function toggleEnabled() onlyOwner public {
        enabled = !enabled;
    }
    function claimAirdrop() isEnabled public {
        uint256 balance = balances[msg.sender];
        require(balance > 0, "No unclaimed tokens");
        balances[msg.sender] = 0;
        govToken.transfer(msg.sender, balance);
        emit Claimed(msg.sender, balance);
    }
    function transferTokens(address _address, uint256 _amount) onlyOwner public {
        govToken.transfer(_address, _amount);
    }
}