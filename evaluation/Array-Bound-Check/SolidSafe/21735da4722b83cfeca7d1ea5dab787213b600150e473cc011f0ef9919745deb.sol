contract C {
    function f(bytes memory a, bytes memory b, uint[] memory c)
        public
        pure
        returns (uint, bytes1, uint, bytes1, uint, uint)
    {
        return (a.length, a[1], b.length, b[2], c.length, c[3]);
    }
    function g() public returns (uint, bytes1, uint, bytes1, uint, uint) {
        uint[] memory x = new uint[](4);
        x[3] = 7;
        return f(bytes("abc"), bytes("def"), x);
    }
}
// ----
