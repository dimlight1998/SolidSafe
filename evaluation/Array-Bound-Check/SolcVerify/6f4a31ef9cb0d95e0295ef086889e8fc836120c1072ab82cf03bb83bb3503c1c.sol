// Computes binomial coefficients the chinese way
contract C {
    function f(uint256 n, uint256 k) public returns (uint256) {
        uint256[][] memory rows = new uint256[][](n + 1);
        for (uint256 i = 1; i <= n; i++) {
assert(i >= 0 && i < rows.length);
            rows[i] = new uint256[](i);
assert(0 >= 0 && 0 < rows[i].length);
            rows[i][0] = 1;
assert(rows[i].length - 1 >= 0 && rows[i].length - 1 < rows[i].length);
            rows[i][rows[i].length - 1] = 1;
            for (uint256 j = 1; j < i - 1; j++) {
assert(i >= 0 && i < rows.length);
assert(j >= 0 && j < rows[i].length);
assert(i - 1 >= 0 && i - 1 < rows.length);
assert(j - 1 >= 0 && j - 1 < rows[i - 1].length);
assert(j >= 0 && j < rows[i - 1].length);
                rows[i][j] = rows[i - 1][j - 1] + rows[i - 1][j];
            }
        }
assert(n >= 0 && n < rows.length);
assert(k - 1 >= 0 && k - 1 < rows[n].length);
        return rows[n][k - 1];
    }
}
// ----
// f(uint256,uint256): 3, 1 -> 1
// f(uint256,uint256): 9, 5 -> 70
