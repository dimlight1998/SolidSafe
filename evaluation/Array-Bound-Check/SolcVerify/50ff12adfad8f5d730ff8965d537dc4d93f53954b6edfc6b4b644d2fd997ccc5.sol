// Sources flattened with hardhat v2.6.5 https://hardhat.org
// File @openzeppelin/contracts/utils/[emailprotected]
// SPDX-License-Identifier: MIT
pragma solidity ^0.7.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File @openzeppelin/contracts/access/[emailprotected]
pragma solidity ^0.7.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _setOwner(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _setOwner(newOwner);
    }
    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File @openzeppelin/contracts/utils/introspection/[emailprotected]
pragma solidity ^0.7.0;
interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
// File @openzeppelin/contracts/token/ERC721/[emailprotected]
pragma solidity ^0.7.0;
interface IERC721 is IERC165 {
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId) external;
    function transferFrom(address from,
        address to,
        uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function getApproved(uint256 tokenId) external view returns (address operator);
    function setApprovalForAll(address operator, bool _approved) external;
    function isApprovedForAll(address owner, address operator) external view returns (bool);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId,
        bytes calldata data) external;
}
// File @openzeppelin/contracts/security/[emailprotected]
pragma solidity ^0.7.0;
abstract contract ReentrancyGuard {
    // Booleans are more expensive than uint256 or any type that takes up a full
    // word because each write operation emits an extra SLOAD to first read the
    // slot's contents, replace the bits taken up by the boolean, and then write
    // back. This is the compiler's defense against contract upgrades and
    // pointer aliasing, and it cannot be disabled.
    // The values being non-zero value makes deployment a bit more expensive,
    // but in exchange the refund on every call to nonReentrant will be lower in
    // amount. Since refunds are capped to a percentage of the total
    // transaction's gas, it is best to keep them low in cases like this one, to
    // increase the likelihood of the full refund coming into effect.
    uint256 private constant _NOT_ENTERED = 1;
    uint256 private constant _ENTERED = 2;
    uint256 private _status;
    constructor() {
        _status = _NOT_ENTERED;
    }
    modifier nonReentrant() {
        // On the first call to nonReentrant, _notEntered will be true
        require(_status != _ENTERED, "ReentrancyGuard: reentrant call");
        // Any calls to nonReentrant after this point will fail
        _status = _ENTERED;
        _;
        // By storing the original value once again, a refund is triggered (see
        // https://eips.ethereum.org/EIPS/eip-2200)
        _status = _NOT_ENTERED;
    }
}
// File @openzeppelin/contracts/token/ERC721/extensions/[emailprotected]
pragma solidity ^0.7.0;
interface IERC721Enumerable is IERC721 {
    function totalSupply() external view returns (uint256);
    function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256 tokenId);
    function tokenByIndex(uint256 index) external view returns (uint256);
}
// File @openzeppelin/contracts/token/ERC721/extensions/[emailprotected]
pragma solidity ^0.7.0;
interface IERC721Metadata is IERC721 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function tokenURI(uint256 tokenId) external view returns (string memory);
}
// File contracts/DistributorV1.sol
pragma solidity ^0.7.0;
interface IERC721Full is IERC721, IERC721Enumerable, IERC721Metadata {}
contract DistributorV1 is Ownable, ReentrancyGuard {
    IERC721Full nftContract;
    uint256 constant TOTAL_NFTS_COUNT = 10000;
    mapping(uint256 => uint256) public givenRewards;
    uint256 public totalGivenRewardsPerToken = 0;
    bool public isFrozen = false;
    constructor(address nft_address) {
        nftContract = IERC721Full(nft_address);
    }
    function freeze() external onlyOwner {
        isFrozen = true;
    }
    function unfreeze() external onlyOwner {
        isFrozen = false;
    }
    function getGivenRewardsPerToken(uint256 from, uint256 __length)
        external
        view
        returns (uint256[] memory rewards)
    {
        uint256 length = __length;
        if (from + length > TOTAL_NFTS_COUNT) {
            length = TOTAL_NFTS_COUNT - from;
        }
        uint256[] memory _givenRewards = new uint256[](length);
        for (uint256 i = 0; i < length; i++) {
require(i >= 0);
assert(i >= 0 && i < _givenRewards.length);
            _givenRewards[i] =  givenRewards[i];
        }
        return _givenRewards;
    }
    function deposit() external payable {
        require(!isFrozen, "Distributor is frozen.");
        totalGivenRewardsPerToken += msg.value * 1000 / TOTAL_NFTS_COUNT / 1000;
    }
    function getAddressRewards(address owner) external view returns (uint256 amount) {
        uint256 numTokens = nftContract.balanceOf(owner);
        uint256 rewards = 0;
        for (uint256 i = 0; i < numTokens; i++) {
            uint256 tokenId = nftContract.tokenOfOwnerByIndex(owner, i);
            if (tokenId < TOTAL_NFTS_COUNT) {
                rewards +=
                    totalGivenRewardsPerToken -
                    givenRewards[tokenId];
            }
        }
        return rewards;
    }
    function getTokensRewards(uint256[] calldata tokenIds) external view returns (uint256 amount) {
        uint256 numTokens = tokenIds.length;
        uint256 rewards = 0;
        // Rewards of tokens owned by the sender
        for (uint256 i = 0; i < numTokens; i++) {
require(i >= 0);
assert(i >= 0 && i < tokenIds.length);
            uint256 tokenId = tokenIds[i];
            if (tokenId < TOTAL_NFTS_COUNT) {
                rewards +=
                    totalGivenRewardsPerToken -
                    givenRewards[tokenId];
            }
        }
        return rewards;
    }
    function claimRewardsRange(uint256 from, uint256 length) external {
        require(!isFrozen, "Distributor is frozen.");
        uint256 numTokens = nftContract.balanceOf(msg.sender);
        require(from + length <= numTokens, "Out of index");
        uint256 rewards = 0;
        // Rewards of tokens owned by the sender
        for (uint256 i = 0; i < length; i++) {
            uint256 tokenId = nftContract.tokenOfOwnerByIndex(msg.sender,
                i + from);
            if (tokenId < TOTAL_NFTS_COUNT) {
                uint256 tokenReward = totalGivenRewardsPerToken -
                    givenRewards[tokenId];
                rewards += tokenReward;
                givenRewards[tokenId] = totalGivenRewardsPerToken;
            }
        }
        payable(msg.sender).transfer(rewards);
    }
    function claimRewards() external {
        require(!isFrozen, "Distributor is frozen.");
        uint256 numTokens = nftContract.balanceOf(msg.sender);
        uint256 rewards = 0;
        // Rewards of tokens owned by the sender
        for (uint256 i = 0; i < numTokens; i++) {
            uint256 tokenId = nftContract.tokenOfOwnerByIndex(msg.sender, i);
            if (tokenId < TOTAL_NFTS_COUNT) {
                uint256 tokenReward = totalGivenRewardsPerToken -
                    givenRewards[tokenId];
                rewards += tokenReward;
                givenRewards[tokenId] = totalGivenRewardsPerToken;
            }
        }
        payable(msg.sender).transfer(rewards);
    }
    function emergencyWithdraw() external onlyOwner {
        payable(_msgSender()).transfer(address(this).balance);
    }
}
