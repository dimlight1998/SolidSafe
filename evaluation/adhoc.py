from datetime import datetime
import subprocess
import os
from loguru import logger
if __name__ == "__main__":
    logger.add("logs/adhoc-{time}.log")
    for i in sorted(os.listdir("./Inv-Gen/CVE/")):
        if len(i) == len("CVE-2018-13090.verismart.sol"):
            logger.info(i)
            ts_start = datetime.now()
            proc = subprocess.run(f"sudo docker run -v /home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/evaluation/Inv-Gen/CVE:/ws --rm verismart:latest -input /ws/{i} -verbose io", capture_output=True, shell=True, text=True)
            ts_stop = datetime.now()
            dur = ts_stop - ts_start
            logger.success(proc.stdout)
            logger.error(proc.stderr)
            logger.info(f"finished with {dur.total_seconds()} secs")
