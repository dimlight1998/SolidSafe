import pathlib
import signal
from abc import ABC, abstractmethod
import os
import tempfile
import shutil
import subprocess
from loguru import logger
from typing import Optional, Literal, Union
from typing_extensions import override
from datetime import datetime
import re

DIR = os.path.dirname(os.path.realpath(__file__))

HARD_TIMEOUT_SEC = 1800
SOFT_TIMEOUT_SEC = 600
SOLIDSAFE_JAR = os.path.normpath(
    os.path.join(DIR, "../target/scala-3.3.1/solidsafe-assembly-0.1.0-SNAPSHOT.jar")
)
SMT_CHECKER = os.path.normpath(os.path.join(DIR, "../env/solc-static-linux"))
SOLC_VERIFY = os.path.normpath(os.path.join(DIR, "../env/solc-verify.py"))

Tag = Literal["arith", "arith-invgen", "assert", "assert-invgen", "bound"]

Result = Union[
    tuple[Literal["verified"], float],
    tuple[Literal["bugfound"], float],
    tuple[Literal["unknown"], float],
    Literal["timeout"],
    Literal["internalerror"],
]


class Tool(ABC):
    @abstractmethod
    def build_command(self, sol_path: str, tag: Tag) -> list[str]: ...

    @abstractmethod
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]: ...

    def prepare(self, sol_path: str) -> None:
        pass

    def on_timeout(self) -> None:
        pass

    def should_check_return_code(self) -> bool:
        return True

    def evaluate(self, sol_path: str, tag: Tag) -> tuple[str, Result]:
        result = self.evaluate_impl(sol_path, tag)
        logger.info(f"{self.__class__.__qualname__} {sol_path} {result}")
        return (sol_path, result)

    def evaluate_impl(self, sol_path: str, tag: Tag) -> Result:
        self.prepare(sol_path)
        command = self.build_command(sol_path, tag)
        proc = None
        try:
            logger.info(f"evaluate {self.__class__.__qualname__} on {sol_path}")
            logger.info(f"command: {command}")
            ts_start = datetime.now()
            proc = subprocess.run(
                command,
                text=True,
                capture_output=True,
                check=self.should_check_return_code(),
                timeout=HARD_TIMEOUT_SEC,
            )
            ts_stop = datetime.now()
            duration = ts_stop - ts_start
            logger.success(proc.stdout)
            logger.error(proc.stderr)
            logger.info(f"finished with {duration.total_seconds()} secs")
            match self.get_result(proc.returncode, proc.stdout, proc.stderr):
                case "verified":
                    return ("verified", duration.total_seconds())
                case "bugfound":
                    return ("bugfound", duration.total_seconds())
                case "unknown":
                    return ("unknown", duration.total_seconds())
                case None:
                    return "internalerror"
        except subprocess.TimeoutExpired as proc:
            logger.warning("TIMEOUT")
            if proc.stdout is not None:
                logger.warning(proc.stdout.decode())
            if proc.stderr is not None:
                logger.error(proc.stderr.decode())
            self.on_timeout()
            return "timeout"
        except subprocess.CalledProcessError as proc:
            logger.error(proc.returncode)
            logger.warning(proc.stdout)
            logger.error(proc.stderr)
            return "internalerror"


class SolidSafe(Tool):
    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [
            "java",
            "-jar",
            SOLIDSAFE_JAR,
            sol_path,
            "--solc-exe",
            "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/solc-static-linux",
            "--boogie-exe",
            "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/boogie",
            "--corral-exe",
            "/home/zhang/Workspace/Smart-Contract-Verification/SolidSafe/env/corral",
            "--verbose",
        ]
        match tag:
            case "arith":
                command.append("-a")
            case "arith-invgen":
                command.extend(["-a", "-i", "--bmc"])
            case "assert":
                pass
            case "assert-invgen":
                command.extend(["-i", "--bmc"])
            case "bound":
                command.append("-b")
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]:
        for line in stdout.splitlines():
            if re.match(r"^verified$", line):
                return "verified"
            if re.match(r"^unknown$", line):
                return "unknown"
            if re.match(r"^bug found$", line):
                return "bugfound"


class SMTChecker(Tool):
    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [
            SMT_CHECKER,
            "--model-checker-engine=chc",
            "--model-checker-show-unproved",
            # "--model-checker-bmc-loop-iterations=2",
            sol_path,
        ]
        match tag:
            case "arith":
                command.append("--model-checker-invariants=default")
                command.append("--model-checker-targets=underflow,overflow")
            case "arith-invgen":
                command.append("--model-checker-invariants=contract")
                command.append("--model-checker-targets=underflow,overflow")
            case "assert":
                command.append("--model-checker-invariants=default")
                command.append("--model-checker-targets=assert")
            case "assert-invgen":
                command.append("--model-checker-invariants=contract")
                command.append("--model-checker-targets=assert")
            case "bound":
                command.append("--model-checker-invariants=default")
                command.append("--model-checker-targets=popEmptyArray,outOfBounds")
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]:
        for line in stderr.splitlines():
            if re.match(r"^Warning: BMC.*", line):
                return "bugfound"
        verified = True
        for line in stderr.splitlines():
            if re.match(r"^Warning: CHC.*", line):
                verified = False
        if verified:
            return "verified"
        else:
            return "unknown"


class VeriSmart(Tool):
    @override
    def prepare(self, sol_path: str) -> None:
        self.temp = tempfile.mktemp(suffix=".sol")
        shutil.copy(sol_path, self.temp)
        logger.info(f"copied to {self.temp}")

    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [
            "sudo",
            "docker",
            "run",
            "-v",
            "/tmp:/ws",
            "--rm",
            "verismart:latest",
            "-input",
            "/ws" + self.temp[4:],
        ]
        match tag:
            case "arith":
                command.append("io")
            case "arith-invgen":
                command.append("io")
            case "assert":
                command.append("assert")
            case "assert-invgen":
                command.append("assert")
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]:
        for line in stdout.splitlines():
            if match := re.match(r"^\# Alarm / Query\s+:\s*(\d+)\s*/\s*\d+", line):
                if int(match.group(1)) == 0:
                    return "verified"
                else:
                    return "unknown"

    @override
    def on_timeout(self) -> None:
        subprocess.run(
            "sudo docker kill $(sudo docker ps -q)",
            capture_output=True,
            shell=True,
            text=True,
        )


class VeriSmartExploit(Tool):
    @override
    def prepare(self, sol_path: str) -> None:
        self.temp = tempfile.mktemp(suffix=".sol")
        shutil.copy(sol_path, self.temp)
        logger.info(f"copied to {self.temp}")

    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [
            "sudo",
            "docker",
            "run",
            "-v",
            "/tmp:/ws",
            "--rm",
            "verismart:latest",
            "-input",
            "/ws" + self.temp[4:],
        ]
        match tag:
            case "arith":
                command.append("io")
            case "arith-invgen":
                command.append("io")
            case "assert-invgen":
                command.append("assert")
        command.extend(["-mode", "exploit", "-exploit_timeout", str(SOFT_TIMEOUT_SEC)])
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]:
        for line in stdout.splitlines():
            if match := re.match(r"^\# Alarm / Query\s+:\s*(\d+)\s*/\s*\d+", line):
                if int(match.group(1)) == 0:
                    return "verified"
                else:
                    return "unknown"


class SolcVerify(Tool):
    @override
    def should_check_return_code(self) -> bool:
        return False

    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [SOLC_VERIFY, sol_path]
        match tag:
            case "arith":
                command.append("--arithmetic")
                command.append("mod-overflow")
            case "arith-invgen":
                command.append("--arithmetic")
                command.append("mod-overflow")
            case "assert":
                pass
            case "assert-invgen":
                pass
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]:
        for line in reversed(stdout.splitlines()):
            if "No errors found." in line:
                return "verified"
            if "Errors were found by the verifier." in line:
                return "bugfound"
        return None


class Mythril(Tool):
    @override
    def prepare(self, sol_path: str) -> None:
        self.temp = tempfile.mktemp(suffix=".sol")
        shutil.copy(sol_path, self.temp)
        logger.info(f"copied to {self.temp}")

    @override
    def build_command(self, sol_path: str, tag: Tag) -> list[str]:
        command = [
            "sudo",
            "docker",
            "run",
            "-v",
            "/tmp:/ws",
            "--rm",
            "mythril/myth:latest",
            "analyze",
            "-o",
            "json",
            "--execution-timeout",
            str(SOFT_TIMEOUT_SEC),
            "/ws" + self.temp[4:],
        ]
        match tag:
            case "arith":
                command.append("-m")
                command.append("IntegerArithmetics")
            case "arith-invgen":
                command.append("-m")
                command.append("UserAssertions")
            case "assert":
                pass
            case "assert-invgen":
                pass
        return command

    @override
    def get_result(
        self, code: int, stdout: str, stderr: str
    ) -> Optional[Literal["verified", "bugfound", "unknown"]]: ...

    @override
    def on_timeout(self) -> None:
        subprocess.run(
            "sudo docker kill $(sudo docker ps -q)",
            capture_output=True,
            shell=True,
            text=True,
        )


def evaluate_overflow():
    contract_basenames = sorted(
        os.listdir(os.path.join(DIR, "Overflow-Check", "SolidSafe"))
    )
    for contract_basename in contract_basenames:
        # SolidSafe().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "SolidSafe", contract_basename),
        #     "arith",
        # )
        # SolidSafe().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "SolidSafe-Annotated", contract_basename),
        #     "arith",
        # )
        # SolcVerify().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "Solc-Verify", contract_basename),
        #     "arith",
        # )
        # VeriSmart().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "VeriSmart", contract_basename),
        #     "arith",
        # )
        # VeriSmartExploit().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "VeriSmart-Exploit", contract_basename),
        #     "arith",
        # )
        # SMTChecker().evaluate(
        #     os.path.join(DIR, "Overflow-Check", "SMT-Checker", contract_basename),
        #     "arith",
        # )
        Mythril().evaluate(
            os.path.join(DIR, "Overflow-Check", "Mythril", contract_basename),
            "arith",
        )


def evaluate_array_bound():
    contract_basenames = sorted(
        os.listdir(os.path.join(DIR, "Array-Bound-Check", "SMT-Checker"))
    )
    results: list[tuple[str, Result]] = []
    for contract_basename in contract_basenames:
        # results.append(
        #     SolidSafe().evaluate(
        #         os.path.join(
        #             DIR, "Array-Bound-Check", "SMT-Checker", contract_basename
        #         ),
        #         "bound",
        #     )
        # )
        # results.append(
        #     SMTChecker().evaluate(
        #         os.path.join(
        #             DIR, "Array-Bound-Check", "SolidSafe", contract_basename
        #         ),
        #         "bound",
        #     )
        # )
        # results.append(
        #     SolcVerify().evaluate(
        #         os.path.join(
        #             DIR, "Array-Bound-Check", "SolcVerify", contract_basename
        #         ),
        #         "assert"
        #     )
        # )
        # results.append(
        #     VeriSmart().evaluate(
        #         os.path.join(
        #             DIR, "Array-Bound-Check", "VeriSmart", contract_basename
        #         ),
        #         "assert"
        #     )
        # )
        results.append(
            Mythril().evaluate(
                os.path.join(DIR, "Array-Bound-Check", "Mythril", contract_basename),
                "assert"
            )
        )

    def show_file_info(file_path):
        basename: str = os.path.basename(file_path)
        basename_short = (
            f"\\id{{{basename[:4]}}}{{{basename.removesuffix('.sol')[-4:]}}}"
        )
        content = pathlib.Path(file_path).read_text().splitlines()
        contract_name = "<unknown>"
        for line in reversed(content):
            if m := re.match(r"\s*contract (\w+) .*", line):
                contract_name = m.group(1)
                break
        return (
            basename_short + " & \\name{" + contract_name + "} & " + str(len(content))
        )

    def show_result(result: Result):
        match result:
            case ("verified", time):
                return f"\\res{{\\OK}}{{{time:.2f}}}"
            case ("unknown", time):
                return f"\\res{{\\DK}}{{{time:.2f}}}"
            case ("bugfound", time):
                return f"\\res{{\\ER}}{{{time:.2f}}}"
            case "internalerror":
                return f"\\res{{\\XX}}{{-}}"
            case "timeout":
                return f"\\res{{\\TO}}{{-}}"
            case _:
                assert False

    # pretty print
    result_str = "\n"
    for result in results:
        result_str += (
            f"{show_file_info(result[0])} & {show_result(result[1])} & x \\\\\n"
        )
    result_str += "\n"
    logger.info(result_str)


if __name__ == "__main__":
    logger.add("logs/evaluation-{time}.log")
    evaluate_array_bound()
