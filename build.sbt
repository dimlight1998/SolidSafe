val scala3Version = "3.3.1"
val circeVersion  = "0.14.6"

scalacOptions ++= Seq("-Werror", "-feature", "-language:implicitConversions", "-deprecation")

lazy val root = project
  .in(file("."))
  .settings(
    name                                            := "solidsafe",
    version                                         := "0.1.0-SNAPSHOT",
    scalaVersion                                    := scala3Version,
    libraryDependencies += "org.scalameta"          %% "munit"                    % "0.7.29" % Test,
    libraryDependencies += "io.circe"               %% "circe-core"               % circeVersion,
    libraryDependencies += "io.circe"               %% "circe-generic"            % circeVersion,
    libraryDependencies += "io.circe"               %% "circe-parser"             % circeVersion,
    libraryDependencies += "com.lihaoyi"            %% "pprint"                   % "0.8.1",
    libraryDependencies += "com.github.pathikrit"   %% "better-files"             % "3.9.2",
    libraryDependencies += "com.github.scopt"       %% "scopt"                    % "4.1.0",
    libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "2.3.0",
    libraryDependencies += "com.moandjiezana.toml"   % "toml4j"                   % "0.7.2"
  )

inThisBuild(
  List(
    scalaVersion      := "3.3.1",
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision
  )
)
