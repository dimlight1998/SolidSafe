package atomgen

import scala.collection.immutable

case class Components(components: Seq[Seq[Location]])

def toComponents(links: Seq[(Location, Location)], indexLinks: Seq[(Location, Location)]): Components =
  val allLocations                = (links ++ indexLinks).flatMap { case (x, y) => Seq(x, y) }
  val unionFind                   = MutableUnionFind[Location](allLocations)
  def literalTooCommon(v: String) = v == "0" || v == "1" || v == "-1" || v == "address(0)"
  links.foreach {
    case (Literal(v), _) if literalTooCommon(v) => ()
    case (_, Literal(v)) if literalTooCommon(v) => ()
    case (x, y)                                 => unionFind.union(x, y)
  }
  indexLinks.groupMap(_._1)(_._2).values.foreach(locs => locs.tail.foreach(loc => unionFind.union(loc, locs.head)))
  Components(unionFind.clusters.map(_.toSeq).toSeq)
