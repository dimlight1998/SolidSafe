package atomgen

val WeightForAssertion      = 3.0
val WeightForConditionInit  = 2.0
val WeightForConditionDecay = 0.8
val WeightForSumInit        = 0.8
val WeightForSumDecay       = 0.8

def prioritize(func: solidity.FunctionDefinition): Seq[(Location, Double)] =
  val fromAssertion = prioritizeAssertion(func)
  val fromCondition = prioritizeCondition(func)
  val fromFrequency = prioritizeFrequency(func)
  Seq(fromAssertion, fromCondition, fromFrequency).flatten

def prioritizeAssertion(func: solidity.FunctionDefinition): Seq[(Location, Double)] =
  func.body match
    case None => Nil
    case Some(body) =>
      val assertions = body.subnodes[solidity.FunctionCall].filter {
        case solidity.FunctionCall(solidity.Identifier(id, "assert"), _, "functionCall") if id < 0 => true
        case _                                                                                     => false
      }
      assertions.flatMap(assert => extractLocations(assert.arguments.head)).map(loc => (loc, WeightForAssertion))

def prioritizeCondition(func: solidity.FunctionDefinition): Seq[(Location, Double)] =
  def prioritizeWithDecay(weight: Double)(statement: solidity.ASTNode): Seq[(Location, Double)] =
    statement match
      case solidity.Block(statements) =>
        statements.flatMap(prioritizeWithDecay(weight))
      case solidity.IfStatement(condition, trueBody, falseBody) =>
        val condRes  = extractLocations(condition)
        val trueRes  = prioritizeWithDecay(weight * WeightForConditionDecay)(trueBody)
        val falseRes = falseBody.map(prioritizeWithDecay(weight * WeightForConditionDecay)).toSeq.flatten
        condRes.map((_, weight)) ++ trueRes ++ falseRes
      case solidity.WhileStatement(_, condition, body) =>
        val condRes = extractLocations(condition)
        val bodyRes = prioritizeWithDecay(weight * WeightForConditionDecay)(body)
        condRes.map((_, weight)) ++ bodyRes
      case solidity.ForStatement(_, _, condition, _, body) =>
        val condRes = extractLocations(condition)
        val bodyRes = prioritizeWithDecay(weight * WeightForConditionDecay)(body)
        condRes.map((_, weight)) ++ bodyRes
      case solidity.ExpressionStatement(
            solidity.FunctionCall(solidity.Identifier(id, "require"), arguments, "functionCall")
          ) if id < 0 =>
        extractLocations(arguments.head).map((_, weight))
      case _ => Nil
  func.body match
    case None => Nil
    case Some(body) =>
      body.asInstanceOf[solidity.Block].statements.flatMap(prioritizeWithDecay(WeightForConditionInit))

def prioritizeFrequency(func: solidity.FunctionDefinition): Seq[(Location, Double)] =
  def handleStatement(stmt: solidity.ASTNode): Seq[Location] =
    stmt match
      case solidity.Block(stmts)              => stmts.flatMap(handleStatement)
      case solidity.ExpressionStatement(expr) => extractLocations(expr)
      case solidity.IfStatement(cond, trueBody, falseBody) =>
        extractLocations(cond) ++ handleStatement(trueBody) ++ falseBody.map(handleStatement).toSeq.flatten
      case solidity.WhileStatement(_, cond, body) => extractLocations(cond) ++ handleStatement(body)
      case solidity.ForStatement(_, init, cond, loop, body) =>
        handleStatement(init) ++ extractLocations(cond) ++ handleStatement(loop) ++ handleStatement(body)
      case solidity.Return(expr) => expr.map(extractLocations).toSeq.flatten
      case _                     => Nil
  val locations = func.body match
    case None       => Nil
    case Some(body) => handleStatement(body)
  locations.groupBy(x => x).toSeq.map((k, v) => (k, v.size.toDouble))

def prioritizeSum(locationWeight: (Location, Double)): Seq[(Location, Double)] =
  val (location, weight) = locationWeight
  location match
    case UserContent(rootRef, members, numIndices) =>
      for i <- 0 until numIndices
      yield (
        UserLength(rootRef, members, i),
        weight * WeightForSumInit * Math.pow(WeightForSumDecay, numIndices - i - 1)
      )
    case _ => Nil

def extractLocations(expr: solidity.ASTNode): Seq[Location] =
  expr match
    case solidity.MemberAccess(
          solidity.FunctionCall(
            solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("address")),
            Seq(solidity.Identifier(id, "this")),
            _
          ),
          "balance",
          None
        ) if id < 0 =>
      Seq(Special("this.balance"))
    case solidity.FunctionCall(
          solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("address")),
          Seq(solidity.Literal("0", _)),
          _
        ) =>
      Seq(Special("address(0)"))
    case solidity.MemberAccess(solidity.Identifier(id, "msg"), "sender", None)      => Seq(Special("msg.sender"))
    case solidity.MemberAccess(solidity.Identifier(id, "block"), "timestamp", None) => Seq(Special("block.timestamp"))
    case solidity.Identifier(ref, name) if ref < 0                                  => Seq(Special(name))
    case solidity.Identifier(ref, _) if ref > 0                                     => Seq(UserContent(ref, Nil, 0))
    case solidity.Literal(value, _)                                                 => Seq(Literal(value))
    case solidity.UnaryOperation(_, sub)                                            => extractLocations(sub)
    case solidity.BinaryOperation(_, lhs, rhs) => extractLocations(lhs) ++ extractLocations(rhs)
    case solidity.Conditional(cond, trueExpr, falseExpr) =>
      extractLocations(cond) ++ extractLocations(trueExpr) ++ extractLocations(falseExpr)
    case solidity.FunctionCall(expr, arguments, _) =>
      extractLocations(expr) ++ arguments.flatMap(extractLocations)
    case solidity.Assignment(_, lhs, rhs) => extractLocations(lhs) ++ extractLocations(rhs)
    case solidity.IndexAccess(baseExpr, indexExpr) =>
      extractLocations(baseExpr) ++ extractLocations(indexExpr) ++ extractContent(baseExpr)
        .map(uc => uc.copy(numIndices = uc.numIndices + 1))
        .toSeq
    case solidity.MemberAccess(expr, "length", None) =>
      extractLocations(expr) ++ extractContent(expr).map(uc => UserLength(uc.rootRef, uc.members, uc.numIndices)).toSeq
    case solidity.MemberAccess(expr, memberName, _) =>
      extractLocations(expr) ++ extractContent(expr).map(uc => uc.copy(members = uc.members :+ memberName)).toSeq
    case _ => Nil

def extractContent(expr: solidity.ASTNode): Option[UserContent] =
  expr match
    case solidity.Identifier(ref, _) if ref > 0 => Some(UserContent(ref, Nil, 0))
    case solidity.IndexAccess(baseExpr, _) =>
      extractContent(baseExpr).map(uc => uc.copy(numIndices = uc.numIndices + 1))
    case solidity.MemberAccess(expr, "length", None) => None
    case solidity.MemberAccess(expr, memberName, _) =>
      extractContent(expr).map(uc => uc.copy(members = uc.members :+ memberName))
    case _ => None
