package atomgen

abstract class Location
case class UserContent(rootRef: Int, members: Seq[String], numIndices: Int) extends Location
case class UserSum(rootRef: Int, members: Seq[String], numIndices: Int)     extends Location
case class UserLength(rootRef: Int, members: Seq[String], numIndices: Int)  extends Location
case class Special(desc: String)                                            extends Location
case class Literal(value: String)                                           extends Location

object Location:
  def prettyPrinted(using ctx: translator.SourceUnitContext)(loc: Location): String =
    def getName(rootRef: Int): String =
      ctx.nodeMap(rootRef) match
        case vd: solidity.VariableDeclaration => vd.name
        case fd: solidity.FunctionDefinition  => fd.name
        case fallback @ _                     => pprint.pprintln(fallback); assert(false)
    loc match
      case UserContent(rootRef, members, numIndices) =>
        (getName(rootRef) +: members).mkString(".") + "[]" * numIndices
      case UserSum(rootRef, members, numIndices) =>
        (getName(rootRef) +: members).mkString(".") + "[]" * numIndices + "#SUM"
      case UserLength(rootRef, members, numIndices) =>
        (getName(rootRef) +: members).mkString(".") + "[]" * numIndices + "#LENGTH"
      case Special(desc)  => desc
      case Literal(value) => value

case class ClusterResult(
    represent: Option[Location],
    links: Seq[(Location, Location)],
    indexLinks: Seq[(Location, Location)]
)

def clusterFunction(using ctx: translator.SourceUnitContext)(func: solidity.FunctionDefinition): ClusterResult =
  func.body match
    case None       => ClusterResult(None, Nil, Nil)
    case Some(body) => clusterStatement(body)

def clusterStatement(using ctx: translator.SourceUnitContext)(stmt: solidity.ASTNode): ClusterResult =
  stmt match
    case solidity.Block(statements) =>
      val results = statements.map(clusterStatement)
      ClusterResult(None, results.flatMap(_.links), results.flatMap(_.indexLinks))
    case solidity.VariableDeclarationStatement(decls, Some(initExpr)) =>
      // TODO: multi values
      require(decls.length == 1 && decls.head.isDefined)
      val decl    = decls.head.get.asInstanceOf[solidity.VariableDeclaration]
      val initRes = clusterExpression(initExpr)
      initRes.represent match
        case None => initRes
        case Some(loc) =>
          val newLink = (loc, UserContent(decl.id.get, Nil, 0))
          ClusterResult(None, newLink +: initRes.links, initRes.indexLinks)
    case solidity.ExpressionStatement(expr) => clusterExpression(expr)
    case solidity.IfStatement(cond, trueBody, falseBody) =>
      val condRes = clusterExpression(cond)
      val trueRes = clusterStatement(trueBody)
      val falseRes = falseBody match
        case None       => ClusterResult(None, Nil, Nil)
        case Some(body) => clusterStatement(body)
      ClusterResult(
        None,
        condRes.links ++ trueRes.links ++ falseRes.links,
        condRes.indexLinks ++ trueRes.indexLinks ++ falseRes.indexLinks
      )
    case solidity.WhileStatement(_, cond, body) =>
      val condRes = clusterExpression(cond)
      val bodyRes = clusterStatement(body)
      ClusterResult(
        None,
        condRes.links ++ bodyRes.links,
        condRes.indexLinks ++ bodyRes.indexLinks
      )
    case solidity.ForStatement(_, initExpr, condExpr, loopExpr, body) =>
      val initRes = clusterExpression(initExpr)
      val condRes = clusterExpression(condExpr)
      val loopRes = clusterExpression(loopExpr)
      val bodyRes = clusterStatement(body)
      val seq     = Seq(initRes, condRes, loopRes, bodyRes)
      ClusterResult(None, seq.flatMap(_.links), seq.flatMap(_.indexLinks))
    case solidity.Return(expr) =>
      expr match
        case None    => ClusterResult(None, Nil, Nil)
        case Some(e) => clusterExpression(e)
    case _ => ClusterResult(None, Nil, Nil)

def clusterExpression(using ctx: translator.SourceUnitContext)(expr: solidity.ASTNode): ClusterResult =
  expr match
    case solidity.Identifier(ref, name) if ref < 0 => ClusterResult(Some(Special(name)), Nil, Nil)
    case solidity.Identifier(ref, _) if ref > 0    => ClusterResult(Some(UserContent(ref, Nil, 0)), Nil, Nil)
    case solidity.Literal(value, _)                => ClusterResult(Some(Literal(value)), Nil, Nil)
    case solidity.UnaryOperation(_, subExpr)       => clusterExpression(subExpr)
    case solidity.BinaryOperation("+" | "-" | ">" | ">=" | "<" | "<=" | "==" | "!=", lhs, rhs) =>
      val lhsRes = clusterExpression(lhs)
      val rhsRes = clusterExpression(rhs)
      val repPair = for
        lhsRep <- lhsRes.represent
        rhsRep <- rhsRes.represent
      yield (lhsRep, rhsRep)
      ClusterResult(
        repPair.flatMap((l, _) => Some(l)),
        repPair.toSeq ++ (lhsRes.links ++ rhsRes.links),
        lhsRes.indexLinks ++ rhsRes.indexLinks
      )
    case solidity.BinaryOperation(_, lhs, rhs) =>
      val lhsRes = clusterExpression(lhs)
      val rhsRes = clusterExpression(rhs)
      ClusterResult(None, lhsRes.links ++ rhsRes.links, lhsRes.indexLinks ++ rhsRes.indexLinks)
    case solidity.Conditional(cond, trueExpr, falseExpr) =>
      val condRes  = clusterExpression(cond)
      val trueRes  = clusterExpression(trueExpr)
      val falseRes = clusterExpression(falseExpr)
      val repPair = for
        trueRep  <- trueRes.represent
        falseRep <- falseRes.represent
      yield (trueRep, falseRep)
      ClusterResult(
        repPair.flatMap((l, _) => Some(l)),
        repPair.toSeq ++ (condRes.links ++ trueRes.links ++ falseRes.links),
        condRes.indexLinks ++ trueRes.indexLinks ++ falseRes.indexLinks
      )
    case solidity.FunctionCall(
          solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("address")),
          Seq(solidity.Literal("0", _)),
          _
        ) =>
      ClusterResult(Some(Special("address(0)")), Nil, Nil)
    case solidity.FunctionCall(solidity.MemberAccess(_, "transfer", None), Seq(value), _) =>
      val valueRes = clusterExpression(value)
      valueRes.represent match
        case None    => valueRes
        case Some(v) => ClusterResult(None, (v, Special("this.balance")) +: valueRes.links, valueRes.indexLinks)
    case solidity.FunctionCall(expr, arguments, _) =>
      val exprRes      = clusterExpression(expr)
      val argumentsRes = arguments.map(clusterExpression)
      val links        = (exprRes +: argumentsRes).flatMap(_.links)
      val indexLinks   = (exprRes +: argumentsRes).flatMap(_.indexLinks)
      val paramLinks = expr match
        case solidity.Identifier(refId, _)
            if refId > 0 && ctx.nodeMap(refId).isInstanceOf[solidity.FunctionDefinition] =>
          val func = ctx.nodeMap(refId).asInstanceOf[solidity.FunctionDefinition]
          val paramDecls = func.parameters
            .asInstanceOf[solidity.ParameterList]
            .parameters
            .map(node => node.asInstanceOf[solidity.VariableDeclaration])
          val paramLocs = paramDecls.map(vd => UserContent(vd.id.get, Nil, 0))
          argumentsRes.zip(paramLocs).map((cr, loc) => cr.represent.map(l => (l, loc))).flatten
        case _ => Nil
      ClusterResult(None, links ++ paramLinks, indexLinks)
    case solidity.Assignment(_, lhs, rhs) =>
      val lhsRes = clusterExpression(lhs)
      val rhsRes = clusterExpression(rhs)
      val repPair = for
        lhsRep <- lhsRes.represent
        rhsRep <- rhsRes.represent
      yield (lhsRep, rhsRep)
      ClusterResult(
        repPair.flatMap((l, _) => Some(l)),
        repPair.toSeq ++ (lhsRes.links ++ rhsRes.links),
        lhsRes.indexLinks ++ rhsRes.indexLinks
      )
    case solidity.IndexAccess(baseExpr, indexExpr) =>
      val baseRes  = clusterExpression(baseExpr)
      val indexRes = clusterExpression(indexExpr)
      val repPair = for
        baseRep  <- baseRes.represent
        indexRep <- indexRes.represent
      yield (baseRep, indexRep)
      val indexInc = baseRes.represent.map {
        case b @ Special(_)             => b
        case uv @ UserContent(_, _, ni) => uv.copy(numIndices = ni + 1)
      }
      ClusterResult(
        indexInc,
        baseRes.links ++ indexRes.links,
        repPair.toSeq ++ baseRes.indexLinks ++ indexRes.indexLinks
      )
    case solidity.MemberAccess(expr, "length", None) =>
      val exprRes = clusterExpression(expr)
      val rep = exprRes.represent.map { case UserContent(rootRef, members, numIndices) =>
        UserLength(rootRef, members, numIndices)
      }
      exprRes.copy(represent = rep)
    case solidity.MemberAccess(expr, memberName, _) =>
      val exprRes = clusterExpression(expr)
      val rep = exprRes.represent.map {
        case Special(desc)                             => Special(desc + "." + memberName)
        case UserContent(rootRef, members, numIndices) => UserContent(rootRef, members :+ memberName, numIndices)
      }
      exprRes.copy(represent = rep)
    case _ => ClusterResult(None, Nil, Nil)
