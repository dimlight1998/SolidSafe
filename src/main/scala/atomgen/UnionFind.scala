package atomgen

private class MutableUnionFind[A](elements: Iterable[A]):
  import scala.collection.mutable.Map as MMap
  import scala.collection.mutable.Set as MSet
  private val belong: MMap[A, MSet[A]] = MMap.from(for elem <- elements yield (elem -> MSet(elem)))
  def union(x: A, y: A) =
    val merged: MSet[A] = MSet.empty
    merged.addAll(belong(x))
    merged.addAll(belong(y))
    for elem <- merged do belong.update(elem, merged)
  def clsuterMap: Map[A, Set[A]] = belong.map((k, v) => (k, v.toSet)).toMap
  def clusters: Set[Set[A]]      = clsuterMap.values.toSet
