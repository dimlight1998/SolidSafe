import better.files.File
import io.circe.*
import io.circe.generic.auto.*
import io.circe.parser.*
import pprint.*
import solidity.*
import invgen.*
import io.circe.syntax.*

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import translator.TranslationOptions
import invgen.toml
import java.nio.file.Files
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.jdk.CollectionConverters.*
import java.util.concurrent.atomic.AtomicBoolean
import utils.printlnIfVerbose
import utils.encodeSolcErrorException
import utils.encodeSpecSyntaxException
import utils.encodeSpecSemanticException
import utils.encodeCannotBeProven
import utils.encodeVerified

def parseSolidity(using config: cmdline.CmdlineConfig)(): Try[solidity.SourceUnit] =
  for
    jsonString <- solidity.compileSolidityToJson(config.solcExecutable, config.solidityFilePath)
    jsonObj    <- jsonStringToJsonObj(jsonString)
    astJson    <- getSingleAstJson(jsonObj)
    sourceUnit <- astJson.as[solidity.SourceUnit].toTry
  yield sourceUnit

def readSolidity(using config: cmdline.CmdlineConfig)(): Try[String] =
  Try(File(config.solidityFilePath).contentAsString)

def solidityToBoogie(using config: cmdline.CmdlineConfig)(): Try[Seq[boogie.TopLevel]] =
  given TranslationOptions = TranslationOptions(config.enableOverflowCheck, config.enableOutOfBoundCheck, false)
  for
    sourceUnit <- parseSolidity()
    rawText <- readSolidity()
    topLevels  <- Try(translator.translateSourceUnit(sourceUnit, rawText))
  yield topLevels

def topLevelsToFile(topLevels: Seq[boogie.TopLevel], outputBoogiePath: String): Unit =
  val bplOutput = File(outputBoogiePath).clear()
  for
    topLevel          <- topLevels
    (indent, content) <- boogie.prettyPrinted(0, topLevel)
  yield bplOutput.appendLine("  " * indent + content)

def locateLastContract(sourceUnit: solidity.SourceUnit): solidity.ContractDefinition =
  sourceUnit.subnodes[solidity.ContractDefinition].last

@main def main(args: String*): Unit =
  cmdline.parseArgs(args) match
    case None =>
    case Some(config) =>
      if config.translateOnly then translateOnly(using config)
      else if config.clusterOnly then
        parseSolidity(using config)().flatMap(su => ensureConfiguration(using config)(su)) match
          case Success(())        =>
          case Failure(exception) => ???
      else
        runWithConfig(using config) match
          case e: utils.SolcErrorException     => println(e.asJson)
          case e: utils.SpecSyntaxException    => println(e.asJson)
          case e: utils.SpecSemanticException  => println(e.asJson)
          case e: utils.InternalErrorException => println(e.asJson)
          case e: utils.CannotBeProven         => println(e.asJson)
          case e: utils.CounterExecutionFound  => println(e.asJson)
          case e: utils.Verified               => println(e.asJson)
          case e: utils.Cancelled              => println(e.asJson)

def translateOnly(using config: cmdline.CmdlineConfig): Unit =
  solidityToBoogie() match
    case Failure(solcError: utils.SolcErrorException)       => println(solcError.asJson)
    case Failure(specSyntax: utils.SpecSyntaxException)     => println(specSyntax.asJson)
    case Failure(specSemantic: utils.SpecSemanticException) => println(specSemantic.asJson)
    case Failure(exp)                                       => pprint.pprintln(exp)
    case Success(topLevels)                                 => topLevelsToFile(topLevels, config.boogieFilePath)

def runWithConfig(using config: cmdline.CmdlineConfig): utils.VerificationResult =
  val correctnessProve = config.enableInvariantGenerating match
    case false =>
      (shouldExit: AtomicBoolean) =>
        val output = boogie.invokeBoogie(config.boogieExecutable, config.boogieFilePath, shouldExit)
        output match
          case None => utils.Cancelled()
          case Some(output) =>
            printlnIfVerbose("---- stdout ----")
            printlnIfVerbose(output(1))
            printlnIfVerbose("---- stderr ----")
            printlnIfVerbose(output(2))
            boogie.parseBoogieProcResult(output, File(config.boogieFilePath).contentAsString)
    case true =>
      (shouldExit: AtomicBoolean) => invGen(shouldExit)
  val bugSearch = config.enableBoundedModelChecking match
    case false => (_: AtomicBoolean) => utils.Cancelled()
    case true =>
      (shouldExit: AtomicBoolean) =>
        val output = boogie.invokeCorral(
          config.corralExecutable,
          config.boogieFilePath,
          config.boundedModelCheckingUnroll,
          shouldExit
        )
        output match
          case None => utils.Cancelled()
          case Some(output) =>
            printlnIfVerbose("---- stdout ----")
            printlnIfVerbose(output(1))
            printlnIfVerbose("---- stderr ----")
            printlnIfVerbose(output(2))
            boogie.parseCorralOutput(output)
  runCorrectnessProveAndBugSearch(correctnessProve, bugSearch)

def runCorrectnessProveAndBugSearch(using
    config: cmdline.CmdlineConfig
)(
    correctnessProve: AtomicBoolean => utils.VerificationResult,
    bugSearch: AtomicBoolean => utils.VerificationResult
): utils.VerificationResult =
  solidityToBoogie() match
    case Failure(e: utils.SolcErrorException)    => e
    case Failure(e: utils.SpecSyntaxException)   => e
    case Failure(e: utils.SpecSemanticException) => e
    case Failure(exception) => exception.getStackTrace().foreach(line => pprint.pprintln(line)); ???
    case Success(topLevels) =>
      topLevelsToFile(topLevels, config.boogieFilePath)
      val shouldExit = AtomicBoolean()
      enum VerificationKind:
        case CorrectnessProve
        case BugSearch
      val correctnessProveFuture = Future(
        (
          VerificationKind.CorrectnessProve,
          correctnessProve(shouldExit)
        )
      )
      val bugSearchFuture = Future(
        (
          VerificationKind.BugSearch,
          bugSearch(shouldExit)
        )
      )
      val first = Future.firstCompletedOf(Seq(correctnessProveFuture, bugSearchFuture))
      Await.result(first, Duration.Inf) match
        case (VerificationKind.CorrectnessProve, result) =>
          result match
            case _: utils.Verified =>
              shouldExit.set(true)
              result
            case _ =>
              Await.result(bugSearchFuture, Duration.Inf)(1)
        case (VerificationKind.BugSearch, result) =>
          result match
            case _: utils.CounterExecutionFound =>
              shouldExit.set(true)
              result
            case _ =>
              Await.result(correctnessProveFuture, Duration.Inf)(1)

def ensureConfiguration(using config: cmdline.CmdlineConfig)(sourceUnit: SourceUnit): Try[Unit] =
  File(config.tomlFilePath).isRegularFile match
    case true => Success(())
    case false =>
      invgen.clusterSourceUnit(sourceUnit).map { semanticClusters =>
        val weightMap = invgen.normalizeWeightMap(
          semanticClusters.flatMap(cluster => cluster.vars),
          invgen.getWeightMap(sourceUnit)
        )
        val tomlConfiguration = buildTomlConfiguration(semanticClusters, weightMap)
        val lines             = toml.encodeConfiguration(tomlConfiguration)
        lines.foreach(line => File(config.tomlFilePath).appendLine(line))
      }

def invGen(using config: cmdline.CmdlineConfig)(shouldExit: AtomicBoolean): utils.VerificationResult =
  val translationOptions = TranslationOptions(config.enableOverflowCheck, config.enableOutOfBoundCheck, false)
  val result = for
    sourceUnit <- parseSolidity()
    rawText <- readSolidity()
    nodeMap       = Map.from(sourceUnit.collect { case node if node.id.nonEmpty => node.id.get -> node })
    sourceUnitCtx = translator.SourceUnitContext(sourceUnit, nodeMap, Some(rawText))
    lastContract  = locateLastContract(sourceUnit)
    topLevels     = translator.translateSourceUnit(using translationOptions)(sourceUnit, rawText)
    semanticClusters <- invgen.clusterSourceUnit(sourceUnit)
    weightMap = invgen.normalizeWeightMap(
      semanticClusters.flatMap(cluster => cluster.vars),
      invgen.getWeightMap(sourceUnit)
    )
    _ <- ensureConfiguration(sourceUnit)
    genConfig = toml.decodeConfiguration(File(config.tomlFilePath).contentAsString)
  yield
    if config.dumpAllInvariants then
      val resultFile = File.newTemporaryFile(suffix = ".txt")
      genInvFromToml(genConfig).iterator.toSeq.zipWithIndex.foreach { case ((e, w), i) =>
        resultFile.appendLine(f"$w%.3f $i%6d ${exprTreeWithIntros(e)}")
      }
      println(f"${config.solidityFilePath} results written to: ${resultFile.path}")
    def trans(pred: String): boogie.Expression =
      given translator.SourceUnitContext = sourceUnitCtx
      val withPrefix                     = "@notice invariant " + pred
      translator.parseSpecString(lastContract, withPrefix, None) match
        case Success((natspec.SpecificationType.Invariant, parsed)) =>
          translator.translateForallExpression(parsed, false)
        case Success(_) => ???
        case Failure(e) => pprint.pprintln(e); ???
    infiniteRunUntilSuccess(config.boogieExecutable, topLevels, genConfig, trans, shouldExit) match
      case None       => utils.CannotBeProven(Nil)
      case Some(invs) => utils.Verified(invs)
  result match
    case Failure(exception) =>
      utils.InternalErrorException(exception.getStackTrace().map(_.toString()).mkString("\n"))
    case Success(value) => value
