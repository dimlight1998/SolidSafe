package cmdline

case class CmdlineConfig(
    solidityFilePath: String = "",
    solcExecutable: String = "solc",
    boogieExecutable: String = "boogie",
    corralExecutable: String = "corral",
    translateOnly: Boolean = false,
    clusterOnly: Boolean = false,
    enableOverflowCheck: Boolean = false,
    enableOutOfBoundCheck: Boolean = false,
    enableInvariantGenerating: Boolean = false,
    houdiniBatchSize: Int = 32,
    enableBoundedModelChecking: Boolean = false,
    boundedModelCheckingUnroll: Int = 4,
    dumpAllInvariants: Boolean = false,
    boogieFilePath: String = "/tmp/output.bpl",
    tomlFilePath: String = "/tmp/atomgen.toml",
    verboseOutput: Boolean = false
)
