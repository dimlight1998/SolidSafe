package cmdline

def parseArgs(args: Seq[String]): Option[cmdline.CmdlineConfig] =
  import scopt.OParser
  val builder = OParser.builder[cmdline.CmdlineConfig]
  val parser =
    import builder.*
    OParser.sequence(
      programName("solidsafe"),
      arg[String]("contract-path")
        .text("file path of the Solidity contract to be verified")
        .action((filePath, c) => c.copy(solidityFilePath = filePath)),
      opt[String]("solc-exe")
        .text("executable of solidity compiler")
        .action((exe, c) => c.copy(solcExecutable = exe)),
      opt[String]("boogie-exe")
        .text("executable of Boogie verifier")
        .action((exe, c) => c.copy(boogieExecutable = exe)),
      opt[String]("corral-exe")
        .text("executable of Corral model checker")
        .action((exe, c) => c.copy(corralExecutable = exe)),
      opt[Unit]('t', "translate")
        .action((_, c) => c.copy(translateOnly = true))
        .text("do not verify; only translate to Boogie"),
      opt[Unit]('c', "cluster")
        .action((_, c) => c.copy(clusterOnly = true))
        .text("do not verify; only cluster and generate a TOML file"),
      opt[Unit]('a', "arith")
        .action((_, c) => c.copy(enableOverflowCheck = true))
        .text("enable arithmetic overflow/underflow checking"),
      opt[Unit]('b', "bound")
        .action((_, c) => c.copy(enableOutOfBoundCheck = true))
        .text("enable array index out-of-bound checking"),
      opt[Unit]('i', "invgen")
        .action((_, c) => c.copy(enableInvariantGenerating = true))
        .text("enable invariant generating to automate verification"),
      opt[Int]('B', "batch-size")
        .action((b, c) => c.copy(houdiniBatchSize = b))
        .text("number of invariants in a batch for Houdini filtering"),
      opt[Unit]('m', "bmc")
        .action((_, c) => c.copy(enableBoundedModelChecking = true))
        .text("enable bounded model checking to find bugs"),
      opt[Int]('u', "unroll")
        .action((unroll, c) => c.copy(boundedModelCheckingUnroll = unroll))
        .text("number of unroll when using BMC"),
      opt[Unit]("dump")
        .action((_, c) => c.copy(dumpAllInvariants = true))
        .text("print all generated invariants to a file for debugging"),
      opt[String]("bpl")
        .action((s, c) => c.copy(boogieFilePath = s))
        .text("where to store the intermediate Boogie file path from translation"),
      opt[String]("toml")
        .action((s, c) => c.copy(tomlFilePath = s))
        .text("where to store the TOML file of atom generating rule"),
      opt[Unit]("verbose")
        .action((_, c) => c.copy(verboseOutput = true))
        .text("print additional information")
    )
  OParser.parse(parser, args, cmdline.CmdlineConfig())
