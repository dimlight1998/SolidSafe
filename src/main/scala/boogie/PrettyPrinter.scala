package boogie

def prettyPrinted(indent: Int, topLevel: TopLevel): Seq[(Int, String)] =
  topLevel match
    case PreludeText(content) => Seq((indent, content))
    case Axiom(expr)          => Seq((indent, f"axiom ${prettyPrinted(expr)};"))
    case GlobalConstantDecl(name, ty) =>
      Seq((indent, f"const $name: ${prettyPrinted(ty)};"))
    case GlobalVariableDecl(name, ty) =>
      Seq((indent, f"var $name: ${prettyPrinted(ty)};"))
    case Procedure(name, attrs, params, returnParams, requires, ensures, localVariables, statements) =>
      def attrsSeg = attrs.map(s => f"{$s} ").mkString
      def ppParams(params: Seq[(String, boogie.Type)]): String =
        params.map { case (varName, ty) => f"$varName: ${prettyPrinted(ty)}" }.mkString(", ")
      val paramsSeg       = ppParams(params)
      val returnParamsSeg = ppParams(returnParams)
      val statementLines  = statements.flatMap(prettyPrinted(indent + 1, _))
      val localVarLines   = localVariables.map((varName, ty) => (indent + 1, f"var $varName: ${prettyPrinted(ty)};"))
      val bodyOpen        = (indent, "{")
      val bodyClose       = (indent, "}")
      val signature       = (indent, f"procedure $attrsSeg$name($paramsSeg) returns ($returnParamsSeg)")
      val requiresLines = requires.map { case (attrs, expr) =>
        (indent + 1, f"requires ${attrs.map(s => f"{$s} ").mkString}${prettyPrinted(expr)};")
      }
      val ensuresLines = ensures.map { case (attrs, expr) =>
        (indent + 1, f"ensures ${attrs.map(s => f"{$s} ").mkString}${prettyPrinted(expr)};")
      }
      Seq(signature) ++ requiresLines ++ ensuresLines ++ Seq(bodyOpen) ++ localVarLines ++ statementLines ++ Seq(
        bodyClose
      )

def prettyPrinted(ty: Type): String =
  ty match
    case BoogieInt()     => "int"
    case BoogieBoolean() => "bool"
    case Address()       => "address"
    case Mapping(kt, vt) => f"[${prettyPrinted(kt)}]${prettyPrinted(vt)}"
    case Reference()     => "reference"

def prettyPrinted(indent: Int, stmt: Statement): Seq[(Int, String)] =
  stmt match
    case Comment(msg) => Seq((indent, f"// $msg"))
    case Assume(attrs, expr) =>
      val attrsSeg = attrs.map(s => f"{$s} ").mkString
      Seq((indent, f"assume $attrsSeg${prettyPrinted(expr)};"))
    case Assert(attrs, expr) =>
      val attrsSeg = attrs.map(s => f"{$s} ").mkString
      Seq((indent, f"assert $attrsSeg${prettyPrinted(expr)};"))
    case Assign(lhs, rhs) => Seq((indent, f"${prettyPrinted(lhs)} := ${prettyPrinted(rhs)};"))
    case Havoc(expr)      => Seq((indent, f"havoc ${prettyPrinted(expr)};"))
    case Call(attrs, funcName, params, Nil) =>
      val attrsSeg = attrs.map(s => f"{$s} ").mkString
      Seq((indent, f"call $attrsSeg$funcName(${params.map(prettyPrinted).mkString(", ")});"))
    case Call(attrs, funcName, params, returnParams) =>
      val attrsSeg        = attrs.map(s => f"{$s} ").mkString
      val returnParamsSeg = returnParams.map(prettyPrinted).mkString(", ")
      val paramsSeg       = params.map(prettyPrinted).mkString(", ")
      Seq((indent, f"call $attrsSeg$returnParamsSeg := $funcName($paramsSeg);"))
    case Return() => Seq((indent, "return;"))
    case Break()  => Seq((indent, "break;"))
    case If(condition, trueBranch) =>
      Seq((indent, f"if (${prettyPrinted(condition)})"))
        ++ Seq((indent, "{"))
        ++ trueBranch.flatMap(prettyPrinted(indent + 1, _))
        ++ Seq((indent, "}"))
    case IfElse(condition, trueBranch, falseBranch) =>
      Seq((indent, f"if (${prettyPrinted(condition)})"))
        ++ Seq((indent, "{"))
        ++ trueBranch.flatMap(prettyPrinted(indent + 1, _))
        ++ Seq((indent, "}"))
        ++ Seq((indent, "else"))
        ++ Seq((indent, "{"))
        ++ falseBranch.flatMap(prettyPrinted(indent + 1, _))
        ++ Seq((indent, "}"))
    case While(condition, invs, freeInvs, body) =>
      Seq((indent, f"while (${prettyPrinted(condition)})"))
        ++ invs.map { case (attrs, invExpr) =>
          val attrsSeg = attrs.map(s => f"{$s} ").mkString
          (indent + 1, f"invariant $attrsSeg${prettyPrinted(invExpr)};")
        }
        ++ freeInvs.map(freeInv => (indent + 1, f"free invariant ${prettyPrinted(freeInv)};"))
        ++ Seq((indent, "{"))
        ++ body.flatMap(prettyPrinted(indent + 1, _))
        ++ Seq((indent, "}"))
    case Condition(branches) =>
      require(branches.nonEmpty)
      val (headCond, headBody) = branches.head
      val headLines = (indent, f"if (${prettyPrinted(headCond)}) {") +: headBody.flatMap(prettyPrinted(indent + 1, _))
      val tailLines = branches.tail.flatMap { case (branchCond, branchBody) =>
        (indent, f"} else if (${prettyPrinted(branchCond)}) {") +: branchBody.flatMap(prettyPrinted(indent + 1, _))
      }
      val closingLine = (indent, "}")
      headLines ++ tailLines :+ closingLine
    case Location(label, content) => (indent, f"// LOCATION: $label") +: content.flatMap(prettyPrinted(indent, _))

def prettyPrinted(expr: Expression): String =
  expr match
    case NonDetermined()               => "*"
    case Identifier(name)              => name
    case Literal(value)                => value
    case UnaryOperation(op, sub)       => f"$op(${prettyPrinted(sub)})"
    case BinaryOperation(op, lhs, rhs) => f"(${prettyPrinted(lhs)}) $op (${prettyPrinted(rhs)})"
    case IndexAccess(base, index)      => f"${prettyPrinted(base)}[${prettyPrinted(index)}]"
    case Forall(qvars, expr) =>
      qvars match
        case Nil => prettyPrinted(expr)
        case _ =>
          f"(forall ${qvars.map((name, ty) => f"$name:${prettyPrinted(ty)}").mkString(", ")} :: ${prettyPrinted(expr)})"
