package boogie

type BoogieProgram = Seq[TopLevel]

abstract sealed class ASTNode

abstract sealed class TopLevel                                extends ASTNode
case class PreludeText(content: String)                       extends TopLevel
case class Axiom(expr: Expression)                            extends TopLevel
case class GlobalConstantDecl(name: String, boogieType: Type) extends TopLevel
case class GlobalVariableDecl(name: String, boogieType: Type) extends TopLevel
case class Procedure(
    name: String,
    attrs: Seq[String],
    params: Seq[(String, Type)],
    returnParams: Seq[(String, Type)],
    requires: Seq[(Seq[String], Expression)],
    ensures: Seq[(Seq[String], Expression)],
    localVariables: Seq[(String, Type)],
    statements: Seq[Statement]
) extends TopLevel

abstract sealed class Type                         extends ASTNode
case class BoogieInt()                             extends Type
case class BoogieBoolean()                         extends Type
case class Address()                               extends Type
case class Mapping(keyType: Type, valueType: Type) extends Type
case class Reference()                             extends Type

abstract sealed class Statement                         extends ASTNode
case class Comment(msg: String)                         extends Statement
case class Assume(attrs: Seq[String], expr: Expression) extends Statement
case class Assert(attrs: Seq[String], expr: Expression) extends Statement
case class Assign(lhs: Expression, rhs: Expression)     extends Statement
case class Havoc(expr: Expression)                      extends Statement
case class Call(attrs: Seq[String], funcName: String, params: Seq[Expression], returnParams: Seq[Expression])
    extends Statement
case class Return()                                                                               extends Statement
case class Break()                                                                                extends Statement
case class If(condition: Expression, trueBranch: Seq[Statement])                                  extends Statement
case class IfElse(condition: Expression, trueBranch: Seq[Statement], falseBranch: Seq[Statement]) extends Statement
case class While(
    condition: Expression,
    invariants: Seq[(Seq[String], Expression)],
    freeInvariants: Seq[Expression],
    body: Seq[Statement]
) extends Statement
case class Condition(branches: Seq[(Expression, Seq[Statement])]) extends Statement
case class Location(label: String, content: Seq[Statement])       extends Statement

abstract sealed class Expression                                         extends ASTNode
case class NonDetermined()                                               extends Expression
case class Identifier(name: String)                                      extends Expression
case class Literal(value: String)                                        extends Expression
case class UnaryOperation(op: String, sub: Expression)                   extends Expression
case class BinaryOperation(op: String, lhs: Expression, rhs: Expression) extends Expression
case class IndexAccess(base: Expression, index: Expression)              extends Expression
case class Forall(qvars: Seq[(String, Type)], expr: Expression)          extends Expression
