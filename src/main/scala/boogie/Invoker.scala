package boogie

import java.util.concurrent.atomic.AtomicBoolean
import scala.sys.process.*
import java.util.concurrent.TimeUnit
import java.time.Duration

def boogieContainsError(output: String): Boolean =
  val r = """^Boogie program verifier finished with \d+ verified, (\d+) error(s?)$""".r
  r.findFirstMatchIn(output.linesIterator.toSeq.last) match
    case None    => pprint.pprintln(output); assert(false)
    case Some(m) => m.group(1).toInt > 0

def corralContainsError(output: String): Boolean =
  output.linesIterator.toSeq.exists(line => """.* This assertion can fail$""".r.matches(line))

def corralTimeout(output: String): Boolean =
  output.linesIterator.toSeq.exists(line => """^Error, internal bug: Z3 ran out of resources$""".r.matches(line))

def invokeBoogie(
    executable: String,
    bplFilePath: String
): (Int, String, String) =
  invokeBoogieImpl(executable, bplFilePath, None).get

def invokeBoogie(
    executable: String,
    bplFilePath: String,
    shouldExit: AtomicBoolean
): Option[(Int, String, String)] =
  invokeBoogieImpl(executable, bplFilePath, Some(shouldExit))

def invokeCorral(
    executable: String,
    bplFilePath: String,
    recursionBound: Int
): (Int, String, String) =
  invokeCorralImpl(executable, bplFilePath, recursionBound, None).get

def invokeCorral(
    executable: String,
    bplFilePath: String,
    recursionBound: Int,
    shouldExit: AtomicBoolean
): Option[(Int, String, String)] =
  invokeCorralImpl(executable, bplFilePath, recursionBound, Some(shouldExit))

private def invokeBoogieImpl(
    executable: String,
    bplFilePath: String,
    shouldExit: Option[AtomicBoolean]
): Option[(Int, String, String)] =
  runCommandExitable(
    Seq(
      executable,
      "/doModSetAnalysis",
      "/errorLimit:0",
      "/errorTrace:0",
      bplFilePath
    ),
    shouldExit
  )

private def invokeCorralImpl(
    executable: String,
    bplFilePath: String,
    recursionBound: Int,
    shouldExit: Option[AtomicBoolean]
): Option[(Int, String, String)] =
  runCommandExitable(
    Seq(
      executable,
      "/k:1",
      "/printDataValues:1",
      f"/recursionBound:$recursionBound",
      f"/main:${translator.BoogieBuilder.lifeCycle}",
      bplFilePath
    ),
    shouldExit
  )

private def runCommandExitable(
    cmd: Seq[String],
    shouldExit: Option[AtomicBoolean]
): Option[(Int, String, String)] =
  import scala.collection.mutable.ListBuffer
  val stdoutLines = ListBuffer[String]()
  val stderrLines = ListBuffer[String]()
  val proc        = cmd.run(ProcessLogger(out => stdoutLines.append(out), err => stderrLines.append(err)))
  while !shouldExit.map(_.get()).getOrElse(false) && proc.isAlive() do Thread.sleep(200)
  (shouldExit.map(_.get()).getOrElse(false), proc.isAlive()) match
    case (true, _)      => proc.destroy(); None
    case (false, true)  => assert(false)
    case (false, false) => Some((proc.exitValue(), stdoutLines.mkString("\n"), stderrLines.mkString("\n")))
