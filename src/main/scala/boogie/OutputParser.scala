package boogie

def parseBoogieProcResult(output: (Int, String, String), bplContent: String): utils.VerificationResult =
  output match
    case (code, _, stderr) if code != 0 || stderr.nonEmpty => utils.InternalErrorException(stderr)
    case (0, stdout, _)                                    => parseBoogieStdout(stdout, bplContent)

def parseBoogieStdout(stdout: String, bplContent: String): utils.VerificationResult =
  val lastLine = stdout.linesIterator.toSeq.last
  """Boogie program verifier finished with (\d+) verified, (\d+) errors?$""".r.findFirstMatchIn(lastLine) match
    case None => utils.InternalErrorException("unexpected Boogie verifier result (conclusion not found)")
    case Some(m) =>
      (m.group(1).toInt, m.group(2).toInt) match
        case (_, 0)          => utils.Verified(Nil)
        case (_, e) if e > 0 => utils.CannotBeProven(parseBoogieErrors(stdout, bplContent))

def parseBoogieErrors(errors: String, bplContent: String): Seq[utils.Diagnostic] =
  val errorLines   = errors.linesIterator.toSeq
  val contentLines = bplContent.linesIterator.toSeq
  val pairs        = errorLines.zip(errorLines.tail)
  val patAssertion = "^.*\\((\\d+),\\d+\\): Error: This assertion might not hold\\.$".r
  val patPostcond1 = "^\\(\\d+,\\d+\\): Error: A postcondition might not hold on this return path\\.$".r
  val patPostcond2 = "^.*\\((\\d+),\\d+\\): Related location: This is the postcondition that might not hold\\.$".r
  val patPrecond1  = "^.*\\((\\d+),\\d+\\): Error: A precondition for this call might not hold\\.$".r
  val patPrecond2  = "^.*\\((\\d+),\\d+\\): Related location: This is the precondition that might not hold\\.$".r
  val patInvEntry  = "^.*\\((\\d+),\\d+\\): Error: This loop invariant might not hold on entry\\.$".r
  val patInvKeep   = "^.*\\((\\d+),\\d+\\): Error: This loop invariant might not be maintained by the loop\\.$".r
  pairs.flatMap { case (line1, line2) =>
    lazy val assertion = patAssertion.findFirstMatchIn(line1)
    lazy val postcond1 = patPostcond1.findFirstMatchIn(line1)
    lazy val postcond2 = patPostcond2.findFirstMatchIn(line2)
    lazy val precond1  = patPrecond1.findFirstMatchIn(line1)
    lazy val precond2  = patPrecond2.findFirstMatchIn(line2)
    lazy val invEntry  = patInvEntry.findFirstMatchIn(line1)
    lazy val invKeep   = patInvKeep.findFirstMatchIn(line1)
    (assertion, postcond1, postcond2, precond1, precond2, invEntry, invKeep) match
      case (Some(m), _, _, _, _, _, _) =>
        extractBoogieDiagnostics(contentLines(m.group(1).toInt - 1)).toSeq
      case (None, Some(m1), Some(m2), _, _, _, _) =>
        extractBoogieDiagnostics(contentLines(m2.group(1).toInt - 1)).toSeq
      case (None, None, None, Some(m1), Some(m2), _, _) =>
        extractBoogieDiagnostics(contentLines(m1.group(1).toInt - 1)) match
          case None =>
            extractBoogieDiagnostics(contentLines(m2.group(1).toInt - 1))
              .map(diag => diag.copy(message = "this precondition may be violated by external execution environment"))
              .toSeq
          case Some(diagFuncCall) =>
            extractBoogieDiagnostics(contentLines(m2.group(1).toInt - 1))
              .map(diag =>
                diag.copy(message =
                  f"this precondition may be violated by an internal call at position ${diagFuncCall._2}"
                )
              )
              .toSeq :+ diagFuncCall
      case (None, None, None, None, None, Some(m), _) =>
        extractBoogieDiagnostics(contentLines(m.group(1).toInt - 1)) match
          case None => Nil
          case Some(diag @ utils.DiagnosticStartStop("loop_inv", start, stop)) =>
            Seq(diag.copy(message = "this loop invariant may be violated on entry"))
          case Some(diag @ utils.DiagnosticStartStop("contract_inv", start, stop)) =>
            Seq(diag.copy(message = "this contract invariant may be violated after deployment"))
          case Some(diag) => Seq(diag)
      case (None, None, None, None, None, None, Some(m)) =>
        extractBoogieDiagnostics(contentLines(m.group(1).toInt - 1)) match
          case None => Nil
          case Some(diag @ utils.DiagnosticStartStop("loop_inv", start, stop)) =>
            Seq(diag.copy(message = "this loop invariant may not be kept by loop body"))
          case Some(diag @ utils.DiagnosticStartStop("contract_inv", start, stop)) =>
            Seq(diag.copy(message = "this contract invariant may not be kept by transaction"))
          case Some(diag) => Seq(diag)
      case _ => Nil
  }

def extractBoogieDiagnostics(line: String): Option[utils.DiagnosticStartStop] =
  for m <- """.*:message \"(\d+):(\d+):(.*)\"""".r.findFirstMatchIn(line)
  yield utils.DiagnosticStartStop(m.group(3), m.group(1).toInt, m.group(2).toInt)

def parseCorralOutput(output: (Int, String, String)): utils.VerificationResult =
  output match
    case (code, _, stderr) if code > 0 || stderr.nonEmpty => utils.InternalErrorException(stderr)
    case (0, stdout, _) =>
      val lines            = stdout.linesIterator.toSeq
      val assertionCanFail = lines.exists(line => """.*: error PF5001: This assertion can fail$""".r.matches(line))
      assertionCanFail match
        case true  => utils.CannotBeProven(Nil)
        case false => utils.CannotBeProven(Nil)
    case fallback @ _ => utils.InternalErrorException("unexpected Corral checker result (exit code 0 with stderr)")

def parseBoogieReason(output: String): Seq[utils.Diagnostic] =
  output.linesIterator.toSeq.flatMap { line =>
    "^(\\d+):(\\d+):(.*)$".r.findFirstMatchIn(line) match
      case None    => Nil
      case Some(m) => Seq(utils.DiagnosticStartStop(m.group(3), m.group(1).toInt, m.group(2).toInt))
  }
