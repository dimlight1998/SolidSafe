package utils

def linesWithOffset(text: String, initOffset: Int): Seq[(String, Int, Int)] =
  text.linesIterator.foldLeft[(Seq[(String, Int, Int)], Int)]((Nil, initOffset)) { case ((res, offset), line) =>
    (res :+ (line, offset, offset + line.size), offset + line.size + 1)
  }(0)

def computeLocForStructuredDocument(text: String, initOffset: Int, endOffset: Int): Seq[(String, Int, Int)] =
  val skipped = "/// ".size
  val raw     = linesWithOffset(text, initOffset + skipped)
  if raw.size <= 1
  then raw
  else
    assert((endOffset - initOffset - text.size - skipped) % (raw.size - 1) == 0)
    val lineOffset = (endOffset - initOffset - text.size - skipped) / (raw.size - 1)
    raw.zipWithIndex.map { case ((text, start, stop), index) =>
      (text, start + lineOffset * index, stop + lineOffset * index)
    }
