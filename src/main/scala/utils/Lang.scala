package utils

import cmdline.CmdlineConfig

def defer[T, Res](defered: => Unit)(body: => Res): Res =
  try
    body
  finally
    defered

def printlnIfVerbose(using config: CmdlineConfig)(content: String): Unit =
  if config.verboseOutput then println(content)
