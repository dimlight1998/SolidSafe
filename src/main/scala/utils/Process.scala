package utils

/** Run process specified as cmdline; blocks until finished;
  * returns exit code, stdout (as a string) and stderr (as a string).
  */
def runProcess(cmdline: Seq[String]): (Int, String, String) =
  import scala.sys.process.*
  var stdout: Option[String] = None
  var stderr: Option[String] = None
  val proc = cmdline.run(
    new ProcessIO(
      in => in.close(),
      out =>
        stdout = Some(String(out.readAllBytes(), "UTF-8"))
        out.close()
      ,
      err =>
        stderr = Some(String(err.readAllBytes(), "UTF-8"))
        err.close()
    )
  )
  val exitCode = proc.exitValue()
  (exitCode, stdout.get, stderr.get)
