package utils

import io.circe.syntax.*
import io.circe.Encoder
import io.circe.Json

sealed abstract class Diagnostic(message: String)
case class DiagnosticStartStop(message: String, start: Int, stop: Int)   extends Diagnostic(message)
case class DiagnosticLineColumn(message: String, line: Int, column: Int) extends Diagnostic(message)
case class DiagnosticOffset(message: String, offset: Int)                extends Diagnostic(message)

trait VerificationResult
abstract class NatspecSpecException() extends RuntimeException

case class SolcErrorException(diagnostics: Seq[Diagnostic])    extends NatspecSpecException with VerificationResult
case class SpecSyntaxException(diagnostics: Seq[Diagnostic])   extends NatspecSpecException with VerificationResult
case class SpecSemanticException(diagnostics: Seq[Diagnostic]) extends NatspecSpecException with VerificationResult
case class InternalErrorException(message: String)             extends NatspecSpecException with VerificationResult
case class CannotBeProven(diagnostics: Seq[Diagnostic])        extends VerificationResult
case class CounterExecutionFound()                             extends VerificationResult
case class Verified(neededInvs: Seq[String])                   extends VerificationResult
case class Cancelled()                                         extends VerificationResult

case class NatspecNotSpecException() extends RuntimeException

implicit val encodeDiagnostic: Encoder[Diagnostic] = new Encoder[Diagnostic]:
  final def apply(diag: Diagnostic): Json =
    diag match
      case DiagnosticStartStop(message, start, stop) =>
        Json.obj(
          "style"   -> Json.fromString("start-stop"),
          "message" -> Json.fromString(message),
          "start"   -> Json.fromInt(start),
          "stop"    -> Json.fromInt(stop)
        )
      case DiagnosticLineColumn(message, line, column) =>
        Json.obj(
          "style"   -> Json.fromString("line-column"),
          "message" -> Json.fromString(message),
          "line"    -> Json.fromInt(line),
          "column"  -> Json.fromInt(column)
        )
      case DiagnosticOffset(message, offset) =>
        Json.obj(
          "style"   -> Json.fromString("offset"),
          "message" -> Json.fromString(message),
          "offset"  -> Json.fromInt(offset)
        )

implicit val encodeSolcErrorException: Encoder[SolcErrorException] = new Encoder[SolcErrorException]:
  final def apply(exception: SolcErrorException): Json =
    Json.obj("result" -> Json.fromString("solc-error"), "diagnostics" -> exception.diagnostics.asJson)

implicit val encodeSpecSyntaxException: Encoder[SpecSyntaxException] = new Encoder[SpecSyntaxException]:
  final def apply(exception: SpecSyntaxException): Json =
    Json.obj("result" -> Json.fromString("spec-syntax"), "diagnostics" -> exception.diagnostics.asJson)

implicit val encodeSpecSemanticException: Encoder[SpecSemanticException] = new Encoder[SpecSemanticException]:
  final def apply(exception: SpecSemanticException): Json =
    Json.obj("result" -> Json.fromString("spec-semantic"), "diagnostics" -> exception.diagnostics.asJson)

implicit val encodeCannotBeProven: Encoder[CannotBeProven] = new Encoder[CannotBeProven]:
  final def apply(exception: CannotBeProven): Json =
    Json.obj("result" -> Json.fromString("cannot-be-proven"), "diagnostics" -> exception.diagnostics.asJson)

implicit val encodeVerified: Encoder[Verified] = new Encoder[Verified]:
  final def apply(verified: Verified): Json =
    Json.obj("result" -> Json.fromString("verified"), "neededInvs" -> verified.neededInvs.asJson)
