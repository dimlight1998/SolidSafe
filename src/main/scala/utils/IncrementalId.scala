package utils

object IncrementalId:
  private var nextIdMap: Map[String, Int] = Map.empty
  def nextId(channel: String): Int =
    nextIdMap.get(channel) match
      case None =>
        nextIdMap = nextIdMap.updated(channel, 1)
        0
      case Some(n) =>
        nextIdMap = nextIdMap.updated(channel, n + 1)
        n
