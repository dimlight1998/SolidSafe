package invgen

import atomgen as AG

def removeAnnotations(program: boogie.BoogieProgram): boogie.BoogieProgram =
  def implForStatement(statement: boogie.Statement): Option[boogie.Statement] =
    statement match
      case boogie.Assert(_, _)              => None
      case boogie.If(condition, trueBranch) => Some(boogie.If(condition, trueBranch.flatMap(implForStatement)))
      case boogie.IfElse(condition, trueBranch, falseBranch) =>
        Some(boogie.IfElse(condition, trueBranch.flatMap(implForStatement), falseBranch.flatMap(implForStatement)))
      case boogie.While(condition, invariants, freeInvariants, body) =>
        Some(
          boogie.While(
            condition,
            Nil,
            invariants.map(_(1)) ++ freeInvariants,
            body.flatMap(implForStatement)
          )
        )
      case boogie.Condition(branches) =>
        Some(boogie.Condition(branches.map((expr, stmts) => (expr, stmts.flatMap(implForStatement)))))
      case boogie.Location(label, content) => Some(boogie.Location(label, content.flatMap(implForStatement)))
      case other                           => Some(other)
  program.map {
    case proc @ boogie.Procedure(_, _, _, _, _, _, _, statements) =>
      proc.copy(statements = statements.flatMap(implForStatement))
    case other => other
  }

def insertInvariantsForLifeCycle(
    program: boogie.BoogieProgram,
    invariants: Seq[boogie.Expression],
    isFree: Boolean
): boogie.BoogieProgram =
  def implForStatement(statement: boogie.Statement) =
    statement match
      case boogie.Location("lifecycle", Seq(loop: boogie.While)) =>
        val updated = isFree match
          case true  => loop.copy(freeInvariants = invariants ++ loop.freeInvariants)
          case false => loop.copy(invariants = invariants.map((Nil, _)) ++ loop.invariants)
        boogie.Location("lifecycle", Seq(updated))
      case other => other
  program.map {
    case proc: boogie.Procedure => proc.copy(statements = proc.statements.map(implForStatement(_)))
    case other                  => other
  }

def insertAssumptionsBeforeTransaction(
    program: boogie.BoogieProgram,
    assumptions: Seq[boogie.Expression]
): boogie.BoogieProgram =
  def implForStatement(statement: boogie.Statement) =
    statement match
      case boogie.Location("lifecycle", Seq(loop: boogie.While)) =>
        val existing = loop.body
        val updated  = loop.copy(body = assumptions.map(boogie.Assume(Seq(":reason \"instrumented\""), _)) ++ existing)
        boogie.Location("lifecycle", Seq(updated))
      case other => other
  program.map {
    case proc: boogie.Procedure =>
      proc.copy(statements = proc.statements.map(implForStatement(_)))
    case other => other
  }

def insertAssertionsAfterTransaction(
    program: boogie.BoogieProgram,
    assertions: Seq[boogie.Expression]
): boogie.BoogieProgram =
  def implForStatement(statement: boogie.Statement) =
    statement match
      case boogie.Location("lifecycle", Seq(loop: boogie.While)) =>
        val existing = loop.body
        val updated = loop.copy(body =
          existing ++ assertions.map(boogie.Assert(Seq(":message \"instrumented predicate may be violated\""), _))
        )
        boogie.Location("lifecycle", Seq(updated))
      case other => other
  program.map {
    case proc: boogie.Procedure => proc.copy(statements = proc.statements.map(implForStatement(_)))
    case other                  => other
  }

def insertAssertionsAfterConstructor(
    program: boogie.BoogieProgram,
    assertions: Seq[boogie.Expression]
): boogie.BoogieProgram =
  def implForStatement(statement: boogie.Statement) =
    statement match
      case boogie.Location("lifecycle", Seq(loop: boogie.While)) =>
        val inserted = assertions.map(boogie.Assert(Seq(":message \"instrumented predicate may be violated\""), _))
        boogie.Location("lifecycle", inserted :+ loop)
      case other => other
  program.map {
    case proc: boogie.Procedure => proc.copy(statements = proc.statements.map(implForStatement(_)))
    case other                  => other
  }
