package invgen

import scala.collection.mutable.Map as MutMap

import invgen.stream.MemorizedStream

case class Intro(
    name: String,
    ty: String,
    cond: Option[String]
)
case class Variable(
    name: String,
    intros: Seq[Intro]
)
case class Operator(
    name: String,
    arity: Int,
    formatter: Seq[String] => String
)

object Operator:
  def unapply(o: Operator): Option[String] = Some(o.name)

abstract class ExprTree(val weight: Double):
  override def toString: String = this match
    case VariableLeaf(v, _)            => v.name
    case OperatorNode(op, children, _) => op.formatter(children.map(_.toString))
case class VariableLeaf(v: Variable, w: Double) extends ExprTree(w)
case class OperatorNode(
    op: Operator,
    children: Seq[ExprTree],
    w: Double
) extends ExprTree(w)

def collectVariableNames(exprTree: ExprTree): Set[String] =
  exprTree match
    case VariableLeaf(v, _) => Set(v.name)
    case OperatorNode(_, children, _) =>
      children.map(collectVariableNames).toSet.flatten

def collectIntroducedVars(exprTree: ExprTree): Set[Intro] =
  exprTree match
    case VariableLeaf(v, _) => v.intros.toSet
    case OperatorNode(_, children, _) =>
      children.map(collectIntroducedVars).toSet.flatten

def getTrees(
    maxSize: Int,
    conf: Configuration
): MemorizedStream[(ExprTree, Double)] =
  val clusters = conf.variableClusters.toVector.map(_.map(_.v))
  case class GenConfig(
      root: Operator | Variable,
      exactSize: Int,
      clusterId: Option[Int]
  ):
    (root, clusterId) match
      case (op: Operator, Some(_)) if !conf.opConfMap(op).isClustering =>
        throw RuntimeException("non clustering op with cluster id")
      case _ => ()
  def allowedSubNodes(
      clusterId: Option[Int]
  ): Set[Variable | Operator] =
    type ResultElem = Variable | Operator
    val (vars, ops) = clusterId match
      case Some(cid) =>
        (clusters(cid), conf.opConfMap.filter(_._2.isClustering).keys.toSet)
      case None => (conf.allVars, conf.allOps)
    val lhs = vars.asInstanceOf[Set[ResultElem]]
    val rhs = ops.asInstanceOf[Set[ResultElem]]
    lhs union rhs
  val memo = MutMap[GenConfig, MemorizedStream[ExprTree]]()
  def genTrees(genConf: GenConfig): MemorizedStream[ExprTree] =
    def impl(genConf: GenConfig): MemorizedStream[ExprTree] =
      val GenConfig(root, exactSize, clusterId) = genConf
      (root, exactSize, clusterId) match
        case (v: Variable, 1, _) =>
          MemorizedStream(
            Iterator(VariableLeaf(v, conf.varConfMap(v).weight))
          )
        case (op: Operator, exactSize, None) if conf.opConfMap(op).isClustering =>
          val streams =
            for cid <- clusters.indices
            yield genTrees(GenConfig(op, exactSize, Some(cid)))
          MemorizedStream.sumOf(streams)(_.weight)
        case (op: Operator, exactSize, _) if exactSize > op.arity =>
          val allowed            = allowedSubNodes(clusterId).toSeq
          val subRootAllocations = permutations(allowed, op.arity)
          val preFilter          = conf.opConfMap(op).preFilter
          val allStreams = for
            subSizeAllocation <- SplitPosNat(exactSize - 1, op.arity)
            subRootAllocation <- subRootAllocations
            if preFilter(subRootAllocation)
          yield
            val subtrees = subSizeAllocation.zip(subRootAllocation).map { case (subSize, subRoot) =>
              impl(GenConfig(subRoot, subSize, clusterId))
            }
            MemorizedStream
              .productOf(subtrees)(_.weight)
              .map { case (subtrees, w) =>
                OperatorNode(
                  op,
                  subtrees,
                  w * conf.depthDecay + conf.opConfMap(op).weight
                )
              }
              .withFilter(node => conf.opConfMap(op).postFilter(node.children))
          MemorizedStream.sumOf(allStreams)(_.weight)
        case _ =>
          MemorizedStream(Iterator.empty)
    memo.getOrElseUpdate(genConf, impl(genConf))
  val allStreams = for
    exactSize <- 1 to maxSize
    root      <- conf.topLevels
  yield genTrees(GenConfig(root, exactSize, None))
  MemorizedStream.sumOf(allStreams)(_.weight).map(n => (n, n.weight))
