package invgen

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import atomgen as AG

enum SemanticVariable:
  case Content(name: String, ty: translator.TranslatorType, members: Seq[String], numIndices: Int)
  case Sum(name: String, ty: translator.TranslatorType, members: Seq[String], numIndices: Int)
  case Length(name: String, ty: translator.TranslatorType, members: Seq[String], numIndices: Int)
  case ThisBalance
  case BlockTimestamp
  // TODO: add type to literal
  case Literal(value: String)
case class SemanticCluster(name: String, vars: Seq[SemanticVariable])

def syntacticToSemantic(using sourceUnitContext: translator.SourceUnitContext)(
    syntacticVariable: atomgen.Location
): SemanticVariable =
  def findVarDecl(id: Int) = sourceUnitContext.nodeMap(id).asInstanceOf[solidity.VariableDeclaration]
  syntacticVariable match
    case atomgen.UserContent(rootRef, members, numIndices) =>
      val varDecl = findVarDecl(rootRef)
      SemanticVariable.Content(varDecl.name, translator.typeNameToTranslatorType(varDecl.typeName), members, numIndices)
    case atomgen.UserSum(rootRef, members, numIndices) =>
      val varDecl = findVarDecl(rootRef)
      SemanticVariable.Sum(varDecl.name, translator.typeNameToTranslatorType(varDecl.typeName), members, numIndices)
    case atomgen.UserLength(rootRef, members, numIndices) =>
      val varDecl = findVarDecl(rootRef)
      SemanticVariable.Length(varDecl.name, translator.typeNameToTranslatorType(varDecl.typeName), members, numIndices)
    case atomgen.Special("this.balance")    => SemanticVariable.ThisBalance
    case atomgen.Special("block.timestamp") => SemanticVariable.BlockTimestamp
    case atomgen.Special("address(0)")      => SemanticVariable.Literal("address(0)")
    case atomgen.Literal(value)             => SemanticVariable.Literal(value)
    case fallback @ _                       => ???

def buildSemanticCluster(vars: Seq[SemanticVariable]): SemanticCluster =
  SemanticCluster("group" + utils.IncrementalId.nextId("group").toString, vars)

// TODO: deduplicate with translator
def analyzeSourceUnit(
    sourceUnit: solidity.SourceUnit
): (translator.SourceUnitContext, Seq[solidity.ContractDefinition], Map[Int, Int]) =
  val nodeMap = Map.from(sourceUnit.collect { case node if node.id.nonEmpty => node.id.get -> node })
  given translator.SourceUnitContext = translator.SourceUnitContext(sourceUnit, nodeMap, None)
  val allContractIds = sourceUnit.collect {
    case contractDef: solidity.ContractDefinition if contractDef.contractKind == "contract" =>
      contractDef.id.get
  }
  def getDirectlyInherited(id: Int): Set[Int] =
    val contractDef = nodeMap(id).asInstanceOf[solidity.ContractDefinition]
    contractDef.baseContracts
      .map(spec =>
        spec
          .asInstanceOf[solidity.InheritanceSpecifier]
          .baseName
          .asInstanceOf[solidity.IdentifierPath]
          .referencedDeclaration
      )
      .toSet
  def getIndirectlyInherited(id: Int): Set[Int] =
    val directly = getDirectlyInherited(id)
    if directly.isEmpty then Set(id) else Set(id) ++ directly.flatMap(getIndirectlyInherited)
  val contractInheritedByLast = getIndirectlyInherited(allContractIds.last)
  val effectiveContracts =
    contractInheritedByLast.toSeq.sorted.map(id => nodeMap(id).asInstanceOf[solidity.ContractDefinition])
  val overriddenFuncIds = for
    funcDef    <- sourceUnit.subnodes[solidity.FunctionDefinition]
    baseFuncId <- funcDef.baseFunctions.toSeq.flatten
  yield baseFuncId
  val finalFuncIds =
    sourceUnit.subnodes[solidity.FunctionDefinition].map(funcDef => funcDef.id.get).toSet.removedAll(overriddenFuncIds)
  val finalMap = (for
    id     <- finalFuncIds
    baseId <- nodeMap(id).asInstanceOf[solidity.FunctionDefinition].baseFunctions.toSeq.flatten
  yield baseId -> id).toMap
  (translator.SourceUnitContext(sourceUnit, nodeMap, None), effectiveContracts, finalMap)

def clusterSourceUnit(sourceUnit: solidity.SourceUnit): Try[Seq[SemanticCluster]] =
  val (ctx, effectiveContracts, finalMap) = analyzeSourceUnit(sourceUnit)
  given translator.SourceUnitContext      = ctx
  val clusterRes = effectiveContracts.flatMap { contractDef =>
    contractDef.collect {
      case funcDef: solidity.FunctionDefinition if finalMap contains funcDef.id.get =>
        val implFunc   = ctx.nodeMap(finalMap(funcDef.id.get)).asInstanceOf[solidity.FunctionDefinition]
        val clusterRes = AG.clusterFunction(implFunc)
        (clusterRes.links, clusterRes.indexLinks)
      case funcDef: solidity.FunctionDefinition =>
        val clusterRes = AG.clusterFunction(funcDef)
        (clusterRes.links, clusterRes.indexLinks)
    }
  }
  val links                           = clusterRes.unzip._1.flatten
  val indexLinks                      = clusterRes.unzip._2.flatten
  val (domainLinks, domainIndexLinks) = buildDomainLinks(links, indexLinks)
  val components                      = AG.toComponents(links ++ domainLinks, indexLinks ++ domainIndexLinks)
  val normalized                      = normalizedComponents(components, links ++ domainLinks)
  Success(normalized.components.map(comp => buildSemanticCluster(comp.map(syntacticToSemantic))))

def getWeightMap(sourceUnit: solidity.SourceUnit): Map[SemanticVariable, Double] =
  val (ctx, effectiveContracts, finalMap) = analyzeSourceUnit(sourceUnit)
  given translator.SourceUnitContext      = ctx
  val locWeights = effectiveContracts
    .flatMap { contractDef =>
      contractDef.collect {
        case funcDef: solidity.FunctionDefinition if finalMap contains funcDef.id.get =>
          val implFunc = ctx.nodeMap(finalMap(funcDef.id.get)).asInstanceOf[solidity.FunctionDefinition]
          AG.prioritize(implFunc)
        case funcDef: solidity.FunctionDefinition =>
          AG.prioritize(funcDef)
      }
    }
    .flatten
    .groupMapReduce(_(0))(_(1))(_ + _)
    .toSeq
  val sumWeights = locWeights.flatMap(atomgen.prioritizeSum)
  (locWeights ++ sumWeights).map((k, v) => Try(syntacticToSemantic(k)).toOption.map((_, v))).flatten.toMap

def normalizeWeightMap(
    vars: Seq[SemanticVariable],
    weightMap: Map[SemanticVariable, Double]
): Map[SemanticVariable, Double] =
  val dvars = vars.distinct
  val sum   = vars.map(sv => weightMap.getOrElse(sv, 1.0)).sum
  Map.from(dvars.map(v => (v -> (1.0 - Math.pow(weightMap.getOrElse(v, 1.0) / sum, 0.5)))))
