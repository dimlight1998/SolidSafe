package invgen

import scala.util.Failure
import scala.util.Success
import scala.util.Try

/** Wait for the user to input a integer (followed by a new line),
  *  which will be interpreted as the number of lines of following content,
  *  then read that number of lines and return the result.
  */
def readUserSpecifiedLines(): Try[Seq[String]] =
  val numLines = scala.io.StdIn.readInt()
  Success((0 until numLines).map(_ => scala.io.StdIn.readLine()))
