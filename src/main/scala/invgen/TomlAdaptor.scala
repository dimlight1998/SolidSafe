package invgen

import invgen.toml

import translator.TranslatorType as TT

def buildTomlConfiguration(
    semanticClusters: Seq[SemanticCluster],
    weightMap: Map[SemanticVariable, Double]
): toml.TomlConfiguration =
  // clusters with type tags, hence rich
  val richClusters =
    for semanticCluster <- semanticClusters
    yield (semanticCluster.name, semanticCluster.vars.map(translateVariable(weightMap)))
  val clusters   = richClusters.map { case (name, richVars) => toml.TomlCluster(name, richVars.unzip.apply(0)) }
  val operators  = generateOperators(richClusters)
  val generating = generateGenerating(richClusters)
  toml.TomlConfiguration(clusters, operators, generating)

/* Translate a semantic variable, may results in nothing. */
private def translateVariable(weightMap: Map[SemanticVariable, Double])(
    semanticVar: SemanticVariable
): (toml.TomlVariable, TypeTag) =
  def elemTypeToTypeTag(ty: TT): TypeTag =
    ty match
      case TT.SignedInt(_)   => TypeTag.SignedInt
      case TT.UnsignedInt(_) => TypeTag.UnsignedInt
      case TT.Boolean        => TypeTag.Bool
      case TT.Address        => TypeTag.Address
      case fallback @ _      => pprint.pprintln(fallback); assert(false)
  /* `acc` should be `Nil` for top-level invocation. */
  def follow(translatorType: TT, members: Seq[String], numIndices: Int, acc: Seq[Access]): Seq[Access] =
    (translatorType, members, numIndices) match
      case (TT.SignedInt(_), Nil, 0)            => acc
      case (TT.UnsignedInt(_), Nil, 0)          => acc
      case (TT.Boolean, Nil, 0)                 => acc
      case (TT.Address, Nil, 0)                 => acc
      case (TT.BytesRef, Nil, 0)                => acc
      case (TT.StringRef, Nil, 0)               => acc
      case (TT.Array(elem), members, 0)         => follow(elem, members, 0, acc)
      case (TT.Array(elem), members, _)         => follow(elem, members, numIndices - 1, acc :+ Access.ArrayIndex())
      case (TT.Mapping(key, value), members, 0) => follow(value, members, 0, acc)
      case (TT.Mapping(key, value), members, _) =>
        follow(value, members, numIndices - 1, acc :+ Access.MappingIndex(elemTypeToTypeTag(key)))
      case (TT.Struct(members), mhead :: mtail, numIndices) =>
        follow(Map.from(members)(mhead), mtail, numIndices, acc :+ Access.Member(mhead))
      case (TT.Struct(members), Nil, 0) => acc
      case fallback @ _                 => pprint.pprintln(fallback); assert(false)
  def assembleDisplay(rootName: String, accesses: Seq[Access]): String =
    accesses.foldLeft(rootName) {
      case (acc, Access.ArrayIndex())                      => acc + "[#u]"
      case (acc, Access.MappingIndex(TypeTag.Address))     => acc + "[#a]"
      case (acc, Access.MappingIndex(TypeTag.Bool))        => acc + "[#b]"
      case (acc, Access.MappingIndex(TypeTag.SignedInt))   => acc + "[#i]"
      case (acc, Access.MappingIndex(TypeTag.UnsignedInt)) => acc + "[#u]"
      case (acc, Access.Member(name))                      => acc + "." + name
      case fallback @ _                                    => pprint.pprintln(fallback); assert(false)
    }
  def assembleIntros(rootName: String, accesses: Seq[Access]): Seq[toml.TomlIntro] =
    accesses.foldLeft[(Seq[toml.TomlIntro], Seq[Access])]((Nil, Nil)) { (acc, access) =>
      val (intros, prefix) = acc
      access match
        case Access.ArrayIndex() =>
          val cond  = f"#u >= 0 && #u < ${assembleDisplay(rootName, prefix)}.length"
          val intro = toml.TomlIntro("#u", "int", Some(cond))
          (intros :+ intro, prefix :+ access)
        case Access.MappingIndex(TypeTag.Address) =>
          val intro = toml.TomlIntro("#a", "address", None)
          (intros :+ intro, prefix :+ access)
        case Access.MappingIndex(TypeTag.Bool) =>
          val intro = toml.TomlIntro("#b", "bool", None)
          (intros :+ intro, prefix :+ access)
        case Access.MappingIndex(TypeTag.SignedInt) =>
          val intro = toml.TomlIntro("#i", "int", None)
          (intros :+ intro, prefix :+ access)
        case Access.MappingIndex(TypeTag.UnsignedInt) =>
          val intro = toml.TomlIntro("#u", "uint", None)
          (intros :+ intro, prefix :+ access)
        case Access.Member(_) => (intros, prefix :+ access)
    }(0)
  def assembleConditions(rootName: String, accesses: Seq[Access]): Seq[String] =
    accesses.foldLeft[(Seq[String], Seq[Access])]((Nil, Nil)) { (acc, access) =>
      val (conds, prefix) = acc
      access match
        case Access.ArrayIndex() =>
          val cond = f"#a >= 0 && #a < ${assembleDisplay(rootName, prefix)}"
          (conds :+ cond, prefix :+ access)
        case Access.MappingIndex(_) => (conds, prefix :+ access)
        case Access.Member(_)       => (conds, prefix :+ access)
    }(0)
  def getTypeTag(translatorType: TT, members: Seq[String]): TypeTag =
    (translatorType, members) match
      case (TT.SignedInt(_), Nil)               => TypeTag.SignedInt
      case (TT.UnsignedInt(_), Nil)             => TypeTag.UnsignedInt
      case (TT.Boolean, Nil)                    => TypeTag.Bool
      case (TT.Address, Nil)                    => TypeTag.Address
      case (TT.Array(elem), members)            => getTypeTag(elem, members)
      case (TT.Mapping(_, value), members)      => getTypeTag(value, members)
      case (TT.Struct(members), mhead :: mtail) => getTypeTag(Map.from(members)(mhead), mtail)
      case fallback @ _                         => pprint.pprintln(fallback); assert(false)
  val weight = weightMap.get(semanticVar)
  semanticVar match
    case SemanticVariable.Content(name, ty, members, numIndices) =>
      val accesses = follow(ty, members, numIndices, Nil)
      val display  = assembleDisplay(name, accesses)
      (toml.TomlVariable(display, None, assembleIntros(name, accesses), weight), getTypeTag(ty, members))
    case SemanticVariable.Sum(name, ty, members, numIndices) =>
      val accesses  = follow(ty, members, numIndices, Nil)
      val display   = "#SUM" + "#" * numIndices + "(" + assembleDisplay(name, accesses) + ")"
      val sanitized = display.replace("(", "_").replace(")", "_")
      (toml.TomlVariable(sanitized, Some(display), assembleIntros(name, accesses), weight), getTypeTag(ty, members))
    case SemanticVariable.Length(name, ty, members, numIndices) =>
      val accesses = follow(ty, members, numIndices, Nil)
      val display  = assembleDisplay(name, accesses) + ".length"
      (toml.TomlVariable(display, None, assembleIntros(name, accesses), weight), TypeTag.UnsignedInt)
    case SemanticVariable.ThisBalance => (toml.TomlVariable("#BALANCE", None, Nil, weight), TypeTag.UnsignedInt)
    case SemanticVariable.BlockTimestamp =>
      (toml.TomlVariable("block.timestamp", None, Nil, weight), TypeTag.UnsignedInt)
    case SemanticVariable.Literal("true")  => (toml.TomlVariable("true", None, Nil, weight), TypeTag.Bool)
    case SemanticVariable.Literal("false") => (toml.TomlVariable("false", None, Nil, weight), TypeTag.Bool)
    case SemanticVariable.Literal("address(0)") =>
      (toml.TomlVariable("#NULL_ADDR", Some("address(0)"), Nil, weight), TypeTag.Address)
    case SemanticVariable.Literal(value) if value.toIntOption.map(_ >= 0) == Some(true) =>
      (toml.TomlVariable(value, None, Nil, weight), TypeTag.UnsignedInt)
    case SemanticVariable.Literal(value) if value.toIntOption.isDefined =>
      (toml.TomlVariable(value, None, Nil, weight), TypeTag.SignedInt)
    case SemanticVariable.Literal(value) =>
      (toml.TomlVariable(value, None, Nil, weight), TypeTag.Address)

private def generateOperators(richClusters: Seq[(String, Seq[(toml.TomlVariable, TypeTag)])]): Seq[toml.TomlOperator] =
  def selectOrFilter(predicate: ((toml.TomlVariable, TypeTag)) => Boolean): Seq[String] =
    richClusters.flatMap { case (name, richVars) =>
      val filtered = richVars.filter((v, t) => predicate(v, t))
      filtered.size == richVars.size match
        case true  => Seq(name)
        case false => filtered.map(_(0).name)
    }
  def isNameALiteral(name: String) =
    """(true|false|\d+|address\(\d+\))""".r.matches(name)
  extension (ss: Seq[String])
    def mkFragment: String = ss match
      case Nil => ""
      case _   => ss.mkString(", ", ", ", "")
  def hasName(name: String): Boolean = richClusters.exists(_(1).exists(_(0).name == name))
  val operatorEq: toml.TomlOperator =
    val allowed = selectOrFilter(rv => rv(1) != TypeTag.Bool).mkFragment
    val disallowAllLiterals =
      val literals = selectOrFilter(rv => isNameALiteral(rv(0).name)).distinct
      literals match
        case Nil      => Nil
        case literals => Seq(f"not(and(belongsTo($$1${literals.mkFragment}), belongsTo($$2${literals.mkFragment})))")
    toml.TomlOperator(
      "eq",
      "($1 == $2)",
      true,
      Seq(
        f"belongsTo($$1, add$allowed)",
        f"belongsTo($$2, add$allowed)",
        "distinct($1, $2)",
        "commutative($1, $2)",
        "uniqueLeafs($1, $2)"
      ) ++ disallowAllLiterals,
      None
    )
  val operatorAdd: toml.TomlOperator =
    val allowed = selectOrFilter(rv => rv(1) == TypeTag.SignedInt || rv(1) == TypeTag.UnsignedInt).mkFragment
    val disallowZero = hasName("0") match
      case true  => Seq("not(belongsTo($1, 0))", "not(belongsTo($2, 0))")
      case false => Nil
    toml.TomlOperator(
      "add",
      "($1 + $2)",
      true,
      Seq(
        f"belongsTo($$1, add$allowed)",
        f"belongsTo($$1, add$allowed)",
        "distinct($1, $2)",
        "commutative($1, $2)",
        "uniqueLeafs($1, $2)"
      ) ++ disallowZero,
      None
    )
  val operatorLess: toml.TomlOperator =
    val allowed  = selectOrFilter(rv => rv(1) == TypeTag.SignedInt || rv(1) == TypeTag.UnsignedInt).mkFragment
    val unsigned = selectOrFilter(rv => rv(1) == TypeTag.UnsignedInt).mkFragment
    val disallowUnsignedZero = hasName("0") match
      case true  => Seq(f"not(and(belongsTo($$1$unsigned), belongsTo($$2, 0)))")
      case false => Nil
    val disallowAllLiterals =
      val literals = selectOrFilter(rv => isNameALiteral(rv(0).name)).distinct
      literals match
        case Nil      => Nil
        case literals => Seq(f"not(and(belongsTo($$1${literals.mkFragment}), belongsTo($$2${literals.mkFragment})))")
    toml.TomlOperator(
      "less",
      "($1 < $2)",
      true,
      Seq(
        f"belongsTo($$1, add$allowed)",
        f"belongsTo($$2, add$allowed)",
        "distinct($1, $2)",
        "uniqueLeafs($1, $2)"
      ) ++ disallowUnsignedZero ++ disallowAllLiterals,
      None
    )
  val operatorLessEq: toml.TomlOperator =
    val allowed  = selectOrFilter(rv => rv(1) == TypeTag.SignedInt || rv(1) == TypeTag.UnsignedInt).mkFragment
    val unsigned = selectOrFilter(rv => rv(1) == TypeTag.UnsignedInt).mkFragment
    val disallowUnsignedZero = hasName("0") match
      case true =>
        Seq(
          f"not(and(belongsTo($$1$unsigned), belongsTo($$2, 0)))",
          f"not(and(belongsTo($$1, 0), belongsTo($$2$unsigned)))"
        )
      case false => Nil
    val disallowAllLiterals =
      val literals = selectOrFilter(rv => isNameALiteral(rv(0).name)).distinct
      literals match
        case Nil      => Nil
        case literals => Seq(f"not(and(belongsTo($$1${literals.mkFragment}), belongsTo($$2${literals.mkFragment})))")
    toml.TomlOperator(
      "lessEq",
      "($1 <= $2)",
      true,
      Seq(
        f"belongsTo($$1, add$allowed)",
        f"belongsTo($$2, add$allowed)",
        "distinct($1, $2)",
        "uniqueLeafs($1, $2)"
      ) ++ disallowUnsignedZero ++ disallowAllLiterals,
      None
    )
  val operatorOr: toml.TomlOperator =
    val allowed = selectOrFilter(rv => rv(1) == TypeTag.Bool).mkFragment
    val disallowTrue = hasName("true") match
      case true  => Seq("not(belongsTo($1, true))", "not(belongsTo($2, true))")
      case false => Nil
    val disallowFalse = hasName("false") match
      case true  => Seq("not(belongsTo($1, false))", "not(belongsTo($2, false))")
      case false => Nil
    toml.TomlOperator(
      "or",
      "($1 || $2)",
      false,
      Seq(
        f"belongsTo($$1, less, lessEq, eq, or, not$allowed)",
        f"belongsTo($$2, less, lessEq, eq, or, not$allowed)",
        "distinct($1, $2)",
        "commutative($1, $2)"
      ) ++ disallowTrue ++ disallowFalse,
      None
    )
  val operatorNot: toml.TomlOperator =
    val allowed = selectOrFilter(rv => rv(1) == TypeTag.Bool).mkFragment
    val disallowTrue = hasName("true") match
      case true  => Seq("not(belongsTo($1, true))")
      case false => Nil
    val disallowFalse = hasName("false") match
      case true  => Seq("not(belongsTo($1, false))")
      case false => Nil
    toml.TomlOperator(
      "not",
      "(!$1)",
      false,
      Seq(
        f"belongsTo($$1, eq, or$allowed)"
      ) ++ disallowTrue ++ disallowFalse,
      None
    )
  Seq(operatorEq, operatorAdd, operatorLess, operatorLessEq, operatorOr, operatorNot)

private def generateGenerating(richClusters: Seq[(String, Seq[(toml.TomlVariable, TypeTag)])]): toml.TomlGenerating =
  def selectOrFilter(predicate: ((toml.TomlVariable, TypeTag)) => Boolean): Seq[String] =
    richClusters.flatMap { case (name, richVars) =>
      val filtered = richVars.filter((v, t) => predicate(v, t))
      filtered.size == richVars.size match
        case true  => Seq(name)
        case false => filtered.map(_(0).name)
    }
  toml.TomlGenerating(
    selectOrFilter(rv => rv(1) == TypeTag.Bool) ++ Seq("eq", "less", "lessEq", "or", "not"),
    1.0,
    2.0,
    3.0
  )

private enum TypeTag:
  case Bool
  case SignedInt
  case UnsignedInt
  case Address

private enum Access:
  case ArrayIndex()
  case MappingIndex(typeTag: TypeTag)
  case Member(name: String)
