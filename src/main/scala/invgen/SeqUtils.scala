package invgen

import scala.collection.mutable.PriorityQueue as MutPQ
import scala.collection.mutable.Map as MutMap

object SplitPosNat:
  type Result = Seq[Seq[Int]]
  val cache = MutMap[(Int, Int), Result]()
  def apply(sum: Int, nrBuckets: Int): Result =
    cache.get((sum, nrBuckets)) match
      case Some(result) => result
      case None =>
        val result =
          if sum < nrBuckets then Seq.empty
          else if nrBuckets == 1 then Seq(Seq(sum))
          else
            for
              head <- 1 to (sum - nrBuckets + 1)
              rest <- apply(sum - head, nrBuckets - 1)
            yield head +: rest
        cache.put((sum, nrBuckets), result)
        result

def permutations[T](xs: Seq[T], size: Int): Seq[Seq[T]] =
  if size == 0 then Seq(Seq.empty)
  else for head <- xs; tail <- permutations(xs, size - 1) yield head +: tail
