package invgen.stream

import scala.collection.mutable.PriorityQueue as MutPQ

class MemorizedStream[+T](private val iter: Iterator[T]):
  private var memo = Vector[T]()
  def tryGet(i: Int): Option[T] =
    while iter.hasNext && i >= memo.length do memo = memo.appended(iter.next())
    memo.lift(i)
  def apply(i: Int): T =
    tryGet(i) match
      case Some(elem) => elem
      case None       => throw IndexOutOfBoundsException()
  def nonEmpty: Boolean = memo.nonEmpty || iter.hasNext
  def iterator: Iterator[T] =
    val self = this
    class Internal extends Iterator[T]:
      var nextIdx                   = 0
      override def hasNext: Boolean = self.tryGet(nextIdx).isDefined
      override def next(): T =
        nextIdx += 1
        self(nextIdx - 1)
    Internal()
  def withFilter(p: T => Boolean): MemorizedStream[T] = MemorizedStream(
    iterator.withFilter(p)
  )
  def map[U](f: T => U): MemorizedStream[U] = MemorizedStream(iterator.map(f))

object MemorizedStream:
  def sumOf[T](streams: Seq[MemorizedStream[T]])(
      converter: T => Double
  ): MemorizedStream[T] =
    class Internal extends Iterator[T]:
      private val queue =
        MutPQ[(MemorizedStream[T], Int)]()(
          Ordering
            .by[(MemorizedStream[T], Int), Double] { case (li, i) =>
              converter(li(i))
            }
            .reverse
        )
      for stream <- streams if stream.nonEmpty do queue.addOne((stream, 0))
      override def hasNext: Boolean = queue.nonEmpty
      override def next(): T =
        val (stream, idx) = queue.dequeue()
        if stream.tryGet(idx + 1).isDefined then queue.addOne((stream, idx + 1))
        stream(idx)
    MemorizedStream(Internal())

  def productOf[T](streams: Seq[MemorizedStream[T]])(
      converter: T => Double
  ): MemorizedStream[(Seq[T], Double)] =
    class Internal extends Iterator[(Seq[T], Double)]:
      private val queue =
        MutPQ[(Seq[Int], Double)]()(Ordering.by[(Seq[Int], Double), Double](_._2).reverse)
      def calcWeight(indices: Seq[Int]): Double =
        streams.zip(indices).map { case (s, i) => converter(s(i)) }.sum
      if streams.forall(_.nonEmpty) then
        val init = Seq.fill(streams.size)(0)
        queue.addOne((init, calcWeight(init)))
      override def hasNext: Boolean = queue.nonEmpty
      override def next(): (Seq[T], Double) =
        val (indices, w) = queue.dequeue()
        for i <- indices.indices do
          if indices.drop(i + 1).forall(_ == 0) then
            val (pre, suf) = indices.splitAt(i)
            val newIndices = pre ++ ((suf.head + 1) +: suf.tail)
            if streams(i).tryGet(suf.head + 1).isDefined then queue.addOne((newIndices, calcWeight(newIndices)))
        (streams.zip(indices).map { case (s, i) => s(i) }, w)
    MemorizedStream(Internal())
