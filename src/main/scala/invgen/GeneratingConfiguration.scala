package invgen

import io.circe.*
import io.circe.generic.auto.*
import io.circe.syntax.*

type PreFilter  = Seq[Variable | Operator] => Boolean
type PostFilter = Seq[ExprTree] => Boolean
case class VariableConfiguration(v: Variable, weight: Double)
case class OperatorConfiguration(
    op: Operator,
    isClustering: Boolean,
    preFilter: PreFilter,
    postFilter: PostFilter,
    weight: Double
)
case class Configuration(
    variableClusters: Set[Set[VariableConfiguration]],
    operators: Set[OperatorConfiguration],
    depthDecay: Double,
    topLevels: Set[Variable | Operator]
):
  val allOps    = operators.map(oc => oc.op)
  val allVars   = for vcs <- variableClusters; vc <- vcs yield vc.v
  val opConfMap = operators.map(oc => oc.op -> oc).toMap
  val varConfMap =
    (for vcs <- variableClusters; vc <- vcs yield vc.v -> vc).toMap
  val varClusterMap =
    (for vcs <- variableClusters; vc <- vcs yield vc.v -> vcs).toMap

object RuleParser:
  trait Context:
    def checkContained(lhs: Variable | Operator, rhs: String): Boolean
    def checkDistinct(args: Seq[ExprTree]): Boolean
    def checkCommutative(args: Seq[ExprTree]): Boolean
    def checkUniqueLeafs(args: Seq[ExprTree]): Boolean
  import scala.util.parsing.combinator as PC
  abstract class CallTree
  case class CallNode(node: String, children: Seq[CallTree]) extends CallTree
  case class CallLeaf(leaf: String)                          extends CallTree
  case class Placeholder(id: Int)                            extends CallTree
  object CallTreeParser extends PC.RegexParsers:
    def ident: Parser[String]          = """[a-zA-Z0-9_\.\[\]#]+""".r
    def placeholderStr: Parser[String] = """\$\d+""".r
    def placeholder: Parser[Placeholder] =
      placeholderStr ^^ (s => Placeholder(s.substring(1).toInt))
    def leaf: Parser[CallLeaf] = ident ^^ { name => CallLeaf(name) }
    def node: Parser[CallNode] = ident ~ "(" ~ repsep(tree, ",") ~ ")" ^^ { case name ~ _ ~ args ~ _ =>
      CallNode(name, args)
    }
    def tree: Parser[CallTree] = node | leaf | placeholder
  def parseCallTree(str: String): CallTree =
    CallTreeParser.parseAll(CallTreeParser.tree, str) match
      case CallTreeParser.Error(msg, next)   => throw RuntimeException(f"$msg : $str")
      case CallTreeParser.Failure(msg, next) => throw RuntimeException(f"$msg : $str")
      case CallTreeParser.Success(result, _) => result
  def extractAllCallNodes(ct: CallTree): Seq[String] =
    ct match
      case Placeholder(_) => Seq.empty
      case CallLeaf(_)    => Seq.empty
      case CallNode(node, children) =>
        node +: children.flatMap(extractAllCallNodes)
  def isPreFilter(ct: CallTree): Boolean =
    extractAllCallNodes(ct).forall(
      Set("not", "and", "or", "belongsTo") contains _
    )
  def isPostFilter(ct: CallTree): Boolean =
    extractAllCallNodes(ct).forall(
      Set("not", "and", "or", "distinct", "commutative", "uniqueLeafs") contains _
    )
  def toPreFilter(ct: CallTree)(using ctx: Context): PreFilter = args =>
    ct match
      case CallNode("not", Seq(pred)) => !toPreFilter(pred)(args)
      case CallNode("and", children) =>
        children.map(toPreFilter(_)).map(f => f(args)).forall(v => v)
      case CallNode("or", children) =>
        children.map(toPreFilter(_)).map(f => f(args)).exists(v => v)
      case CallNode("belongsTo", Placeholder(id) +: rest) =>
        rest
          .map(cl => cl.asInstanceOf[CallLeaf].leaf)
          .exists(ctx.checkContained(args(id - 1), _))
      case _ => throw RuntimeException("invalid rule string")
  def toPostFilter(ct: CallTree)(using ctx: Context): PostFilter = args =>
    ct match
      case CallNode("not", Seq(pred)) => !toPostFilter(pred)(args)
      case CallNode("and", children) =>
        children.map(toPostFilter(_)).map(f => f(args)).forall(v => v)
      case CallNode("or", children) =>
        children.map(toPostFilter(_)).map(f => f(args)).exists(v => v)
      case CallNode("distinct", children) =>
        ctx.checkDistinct(
          children.map(ph => args(ph.asInstanceOf[Placeholder].id - 1))
        )
      case CallNode("commutative", children) =>
        ctx.checkCommutative(
          children.map(ph => args(ph.asInstanceOf[Placeholder].id - 1))
        )
      case CallNode("uniqueLeafs", children) =>
        ctx.checkUniqueLeafs(children.map(ph => args(ph.asInstanceOf[Placeholder].id - 1)))
      case _ => throw RuntimeException("invalid rule string")
  def parseRulesToFilter(str: Seq[String])(using
      Context
  ): (PreFilter, PostFilter) =
    val callTrees = str map parseCallTree
    assert(
      callTrees.forall(ct => isPreFilter(ct) || isPostFilter(ct)),
      "syntax error in operator rule"
    )
    val preFilter = callTrees
      .filter(isPreFilter)
      .map(toPreFilter)
      .foldLeft[PreFilter](_ => true)((acc, func) => args => acc(args) && func(args))
    val postFilter = callTrees
      .filter(isPostFilter)
      .map(toPostFilter)
      .foldLeft[PostFilter](_ => true)((acc, func) => args => acc(args) && func(args))
    (preFilter, postFilter)
