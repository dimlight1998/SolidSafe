package invgen

import translator.TranslatorType

import scala.annotation.tailrec

@tailrec
private def isIntegral(translatorType: TranslatorType, members: Seq[String], numIndices: Int): Boolean =
  (translatorType, members, numIndices) match
    case (TranslatorType.SignedInt(_), Nil, 0)                       => true
    case (TranslatorType.UnsignedInt(_), Nil, 0)                     => true
    case (TranslatorType.Array(elem), members, idx) if idx > 0       => isIntegral(elem, members, idx - 1)
    case (TranslatorType.Mapping(_, value), members, idx) if idx > 0 => isIntegral(value, members, idx - 1)
    case (TranslatorType.Struct(members), mhead :: mtail, idx)       => isIntegral(Map.from(members)(mhead), mtail, idx)
    case _                                                           => false

private def isStateVar(using ctx: translator.SourceUnitContext)(location: atomgen.Location): Boolean =
  location match
    case atomgen.UserContent(rootRef, _, _) =>
      ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration].stateVariable
    case atomgen.UserSum(rootRef, _, _) => ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration].stateVariable
    case atomgen.UserLength(rootRef, _, _) =>
      ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration].stateVariable
    case atomgen.Special("block.timestamp") => true
    case atomgen.Special("this.balance")    => true
    case atomgen.Special("msg.sender")      => false
    case atomgen.Special("msg.value")       => false
    case atomgen.Special("address(0)")      => true
    case atomgen.Literal(_)                 => true
    case fallback @ _                       => pprint.pprintln(fallback); ???

@tailrec
private def isScalar(translatorType: TranslatorType, members: Seq[String], numIndices: Int): Boolean =
  (translatorType, members, numIndices) match
    case (TranslatorType.SignedInt(_), Nil, 0)                       => true
    case (TranslatorType.UnsignedInt(_), Nil, 0)                     => true
    case (TranslatorType.Boolean, Nil, 0)                            => true
    case (TranslatorType.Address, Nil, 0)                            => true
    case (TranslatorType.Array(elem), members, idx) if idx > 0       => isScalar(elem, members, idx - 1)
    case (TranslatorType.Mapping(_, value), members, idx) if idx > 0 => isScalar(value, members, idx - 1)
    case (TranslatorType.Struct(members), mhead :: mtail, idx)       => isScalar(Map.from(members)(mhead), mtail, idx)
    case _                                                           => false

def buildDomainLinks(using ctx: translator.SourceUnitContext)(
    links: Seq[(atomgen.Location, atomgen.Location)],
    indexLinks: Seq[(atomgen.Location, atomgen.Location)]
): (Seq[(atomgen.Location, atomgen.Location)], Seq[(atomgen.Location, atomgen.Location)]) =
  val allLocations = (links ++ indexLinks).flatMap(locs => Seq(locs._1, locs._2)).distinct
  // sum variable is in the same cluster as content
  val sumAndContent = for location <- allLocations yield location match
    case atomgen.UserContent(rootRef, members, numIndices) if numIndices > 0 =>
      val varDecl        = ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration]
      val translatorType = translator.typeNameToTranslatorType(varDecl.typeName)
      isIntegral(translatorType, members, numIndices) match
        case true  => Some((0 until numIndices).map(i => (location, atomgen.UserSum(rootRef, members, i))))
        case false => None
    case _ => None
  // length of arrays can also be indices
  // TODO: support more forms, such as array in mapping
  val lengthsAsIndices = for location <- allLocations yield location match
    case atomgen.UserContent(rootRef, members, numIndices) if numIndices > 0 =>
      val varDecl = ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration]
      def isArray(tt: TranslatorType, remainIndices: Int): Boolean =
        (tt, remainIndices) match
          case (TranslatorType.Array(_), 0)       => true
          case (TranslatorType.Mapping(_, _), 0)  => false
          case (TranslatorType.Array(elem), r)    => isArray(elem, r - 1)
          case (TranslatorType.Mapping(_, vt), r) => isArray(vt, r - 1)
          case _                                  => false
      translator.typeNameToTranslatorType(varDecl.typeName) match
        case t @ TranslatorType.Array(_) if isArray(t, numIndices - 1) =>
          val indexed = atomgen.UserContent(rootRef, members, numIndices - 1)
          val length  = atomgen.UserLength(rootRef, Nil, numIndices - 1)
          Some((indexed, length))
        case _ => None
    case _ => None
  (sumAndContent.flatten.flatten, lengthsAsIndices.flatten)

def normalizedComponents(using
    ctx: translator.SourceUnitContext
)(components: atomgen.Components, links: Seq[(atomgen.Location, atomgen.Location)]): atomgen.Components =
  val componentSeq = components.components
  // remove non state variables
  val nonStateVarRemoved = componentSeq.map(_.filter(isStateVar))
  // remove variables that is not address, integral or boolean
  val nonScalarRemoved = nonStateVarRemoved.map(_.filter {
    case atomgen.UserContent(rootRef, members, numIndices) if numIndices >= 0 =>
      val varDecl        = ctx.nodeMap(rootRef).asInstanceOf[solidity.VariableDeclaration]
      val translatorType = translator.typeNameToTranslatorType(varDecl.typeName)
      isScalar(translatorType, members, numIndices)
    case _ => true
  })
  // remove empty components
  val nonEmptyComponents = nonScalarRemoved.filter(_.nonEmpty)
  // add literals into clusters
  def insert(
      lInsert: atomgen.Location,
      lRepresent: atomgen.Location,
      components: Seq[Seq[atomgen.Location]]
  ): Seq[Seq[atomgen.Location]] =
    components.foldLeft[Seq[Seq[atomgen.Location]]](Nil)((acc, component) =>
      (component contains lRepresent, component contains lInsert) match
        case (true, false) => acc :+ (component :+ lInsert)
        case _             => acc :+ component
    )
  val literalsInClusters = links.foldLeft(nonEmptyComponents) {
    case (acc, (l1: atomgen.Literal, l2)) => insert(l1, l2, acc)
    case (acc, (l1, l2: atomgen.Literal)) => insert(l2, l1, acc)
    case (acc, _)                         => acc
  }
  // remove component with only literals
  val componentsNotOnlyLiterals = literalsInClusters.filter(!_.forall(_.isInstanceOf[atomgen.Literal]))
  atomgen.Components(componentsNotOnlyLiterals)
