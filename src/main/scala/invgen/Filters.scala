package invgen

import better.files.File
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.annotation.tailrec
import java.util.concurrent.atomic.AtomicBoolean
import utils.defer
import utils.printlnIfVerbose

def ppResult(result: Boolean) =
  result match
    case true  => Console.GREEN + "OK" + Console.RESET
    case false => Console.RED + "FAIL" + Console.RESET

def prefilterWithBoogie(
    boogiePath: String,
    program: boogie.BoogieProgram,
    predicates: Seq[(String, boogie.Expression)]
): Seq[(String, boogie.Expression)] =
  val cleanProgram = removeAnnotations(program)
  def isPredicateHold(predicate: boogie.Expression): Boolean =
    val inserted = insertAssertionsAfterConstructor(cleanProgram, Seq(predicate))
    val tempFile = File.newTemporaryFile(suffix = ".bpl")
    for
      topLevel          <- inserted
      (indent, content) <- boogie.prettyPrinted(0, topLevel)
    do tempFile.appendLine(" " * indent + content)
    val (exitCode, stdout, stderr) = boogie.invokeBoogie(boogiePath, tempFile.toString)
    val result = (exitCode, stdout) match
      case (0, stdout) => !boogie.boogieContainsError(stdout)
      case _           => false
    tempFile.delete()
    result
  val futures = predicates.map((solRepr, bplExpr) => Future(((solRepr, bplExpr), isPredicateHold(bplExpr))))
  Await.result(Future.sequence(futures), Duration.Inf).filter(_._2).map(_._1)

def prefilterWithCorral(
    corralPath: String,
    program: boogie.BoogieProgram,
    predicates: Seq[boogie.Expression]
): Seq[boogie.Expression] =
  val cleanProgram = removeAnnotations(program)
  def isPredicateHold(predicate: boogie.Expression): Boolean =
    val inserted = insertInvariantsForLifeCycle(cleanProgram, Seq(predicate), false)
    val tempFile = File.newTemporaryFile(suffix = ".bpl")
    for
      topLevel          <- inserted
      (indent, content) <- boogie.prettyPrinted(0, topLevel)
    do tempFile.appendLine(" " * indent + content)
    val (exitCode, stdout, stderr) = boogie.invokeCorral(corralPath, tempFile.toString, 4)
    val result = (exitCode, stdout) match
      case (0, stdout) => !boogie.corralContainsError(stdout)
      case (1, stdout) => boogie.corralTimeout(stdout)
      case _           => false
    tempFile.delete()
    result
  val futures = predicates.map(pred => Future((pred, isPredicateHold(pred))))
  Await.result(Future.sequence(futures), Duration.Inf).filter(_._2).map(_._1)

def inductiveFilterUsingHoudini(
    boogiePath: String,
    program: boogie.BoogieProgram,
    verifiedPredicates: Seq[(String, boogie.Expression)],
    likelyPredicates: Seq[(String, boogie.Expression)]
): (Seq[(String, boogie.Expression)], Seq[(String, boogie.Expression)]) =
  val cleanProgram = removeAnnotations(program)
  val withFreeInvs = insertInvariantsForLifeCycle(cleanProgram, verifiedPredicates.map(_(1)), true)
  @tailrec
  def houdiniWith(working: Seq[(String, boogie.Expression)]): Seq[(String, boogie.Expression)] =
    val withWorking = insertAssumptionsBeforeTransaction(withFreeInvs, working.map(_(1)))
    def isPostHold(post: boogie.Expression): Boolean =
      val target   = insertAssertionsAfterTransaction(withWorking, Seq(post))
      val tempFile = File.newTemporaryFile(suffix = ".bpl")
      for
        topLevel          <- target
        (indent, content) <- boogie.prettyPrinted(0, topLevel)
      do tempFile.appendLine(" " * indent + content)
      val (exitCode, stdout, stderr) = boogie.invokeBoogie(boogiePath, tempFile.toString)
      val result = (exitCode, stdout) match
        case (0, stdout) => !boogie.boogieContainsError(stdout)
        case _           => false
      tempFile.delete()
      result
    val futures      = working.map(post => Future((post, isPostHold(post(1)))))
    val maintainable = Await.result(Future.sequence(futures), Duration.Inf).filter(_(1)).map(_(0))
    maintainable.length == working.length match
      case true  => working
      case false => houdiniWith(maintainable)
  val inductive = houdiniWith(likelyPredicates)
  (inductive, likelyPredicates.filter(!inductive.toSet.contains(_)))

def ableToProve(
    boogiePath: String,
    program: boogie.BoogieProgram,
    verifiedPredicates: Seq[boogie.Expression],
    shouldExit: AtomicBoolean
): utils.VerificationResult =
  val withInvs = insertInvariantsForLifeCycle(program, verifiedPredicates, false)
  val tempFile = File.newTemporaryFile(suffix = ".bpl")
  defer(tempFile.delete()) {
    for
      topLevel          <- withInvs
      (indent, content) <- boogie.prettyPrinted(0, topLevel)
    do tempFile.appendLine(" " * indent + content)
    boogie.invokeBoogie(boogiePath, tempFile.toString, shouldExit) match
      case None         => utils.CannotBeProven(Nil)
      case Some(output) => boogie.parseBoogieProcResult(output, tempFile.contentAsString)
  }

def infiniteRunUntilSuccess(using config: cmdline.CmdlineConfig)(
    boogiePath: String,
    program: boogie.BoogieProgram,
    tomlConfiguration: toml.TomlConfiguration,
    translator: String => boogie.Expression,
    shouldExit: AtomicBoolean
): Option[Seq[String]] =
  val iter                                       = genInvFromToml(tomlConfiguration).iterator
  var proved                                     = false
  var outOfInv                                   = false
  var verified: Seq[(String, boogie.Expression)] = Nil
  while !proved && !outOfInv && !shouldExit.get() do
    val originalBatch = fetchNextBatch(iter, config.houdiniBatchSize)
    printlnIfVerbose("======== candidates ========")
    originalBatch.foreach(printlnIfVerbose)
    val translatedBatch = originalBatch.map(expr => (expr, translator(expr)))
    printlnIfVerbose(f"checking new batch of ${translatedBatch.size} based on ${verified.size} verified invariants")
    translatedBatch.isEmpty match
      case true =>
        outOfInv = true
      case false =>
        val boogiePassed = prefilterWithBoogie(boogiePath, program, translatedBatch)
        val (houdiniVerified, _) =
          inductiveFilterUsingHoudini(boogiePath, program, verified, boogiePassed)
        printlnIfVerbose("======== newly verified ========")
        houdiniVerified.foreach((solRepr, _) => printlnIfVerbose(solRepr))
        verified ++= houdiniVerified
        ableToProve(boogiePath, program, verified.map(_(1)), shouldExit) match
          case utils.Verified(_) => proved = true
          case _                 =>
  printlnIfVerbose("======== end of iteration ========")
  proved match
    case true =>
      Some(verified.map(_(0)))
    case false =>
      None
