package invgen.toml

case class TomlConfiguration(
    clusters: Seq[TomlCluster],
    operators: Seq[TomlOperator],
    generating: TomlGenerating
)

case class TomlCluster(name: String, vars: Seq[TomlVariable])
case class TomlIntro(name: String, ty: String, cond: Option[String])
case class TomlVariable(name: String, display: Option[String], intros: Seq[TomlIntro], weight: Option[Double])

case class TomlOperator(
    name: String,
    formatter: String,
    clustering: Boolean,
    rules: Seq[String],
    weight: Option[Double]
)

case class TomlGenerating(
    topLevels: Seq[String],
    defaultVariableWeight: Double,
    defaultOperatorWeight: Double,
    depthPenalty: Double
)
