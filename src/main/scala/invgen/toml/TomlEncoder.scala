package invgen.toml

def encodeConfiguration(configuration: TomlConfiguration): Seq[String] =
  val clusters   = configuration.clusters.map(encodeCluster).flatMap(_ :+ "")
  val operators  = configuration.operators.map(encodeOperator).flatMap(_ :+ "")
  val generating = encodeGenerating(configuration.generating)
  clusters ++ operators ++ generating

private def encodeCluster(cluster: TomlCluster): Seq[String] =
  for
    variable <- cluster.vars
    heading = f"[[clusters.${cluster.name}]]"
    varInfo = encodeVariable(variable)
    line <- heading +: varInfo
  yield line

private def encodeVariable(variable: TomlVariable): Seq[String] =
  val nameLine    = f"name = \"${variable.name}\""
  val displayLine = variable.display.map(d => f"display = \"$d\"")
  val introsLine = variable.intros match
    case Nil    => None
    case intros => Some("intros = [" + intros.map(encodeIntro).mkString(", ") + "]")
  val weightLine = variable.weight match
    case None    => None
    case Some(w) => Some(f"weight = $w%.3f")
  nameLine +: (displayLine.toSeq ++ introsLine.toSeq ++ weightLine.toSeq)

private def encodeIntro(intro: TomlIntro): String =
  val namePart = f"name=\"${intro.name}\""
  val typePart = f"type=\"${intro.ty}\""
  val condPart = intro.cond.map(cond => f"cond=\"$cond\"")
  "{" + (Seq(namePart, typePart) ++ condPart.toSeq).mkString(", ") + "}"

private def encodeOperator(operator: TomlOperator): Seq[String] =
  val heading        = f"[operators.${operator.name}]"
  val formatterLine  = f"formatter = \"${operator.formatter}\""
  val clusteringLine = f"clustering = ${operator.clustering}"
  val rulesLines = operator.rules match
    case Nil   => Nil
    case rules => "rules = [" +: rules.map(r => f"  \"$r\",") :+ "]"
  Seq(heading, formatterLine, clusteringLine) ++ rulesLines

private def encodeGenerating(generating: TomlGenerating): Seq[String] =
  val heading          = "[generating]"
  val topLevelLines    = "topLevels = [" +: generating.topLevels.map(t => f"  \"$t\",") :+ "]"
  val dvwLine          = f"defaultVariableWeight = ${generating.defaultVariableWeight}"
  val dowLine          = f"defaultOperatorWeight = ${generating.defaultOperatorWeight}"
  val depthPenaltyLine = f"depthPenalty = ${generating.depthPenalty}"
  heading +: (topLevelLines ++ Seq(dvwLine, dowLine, depthPenaltyLine))
