package invgen.toml

import scala.jdk.CollectionConverters.*
import com.moandjiezana.toml as toml4j

extension [K, V](map: java.util.Map[K, V]) def toSeq: Seq[(K, V)] = map.asScala.toSeq

extension [T](list: java.util.List[T]) def toSeq: Seq[T] = list.asScala.toSeq

def decodeConfiguration(tomlString: String): TomlConfiguration =
  val toml       = toml4j.Toml().read(tomlString)
  val clusters   = toml.getTable("clusters")
  val operators  = toml.getTable("operators")
  val generating = toml.getTable("generating")
  TomlConfiguration(
    for (k, _) <- clusters.toMap.toSeq yield TomlCluster(k, clusters.getList(k).toSeq.map(decodeVariable)),
    for (k, _) <- operators.toMap.toSeq yield decodeOperator(k, operators.getTable(k)),
    decodeGenerating(generating)
  )

private def decodeVariable(obj: Object): TomlVariable =
  val varInfo = obj.asInstanceOf[java.util.HashMap[String, Object]].asScala
  TomlVariable(
    varInfo("name").asInstanceOf[String],
    varInfo.get("display").asInstanceOf[Option[String]],
    varInfo
      .get("intros")
      .asInstanceOf[Option[java.util.ArrayList[?]]]
      .map(_.toSeq.map(decodeIntro))
      .getOrElse(Nil),
    varInfo.get("weight").asInstanceOf[Option[Double]]
  )

private def decodeIntro(obj: Object): TomlIntro =
  val introInfo = obj.asInstanceOf[java.util.HashMap[String, ?]].asScala
  TomlIntro(
    introInfo("name").asInstanceOf[String],
    introInfo("type").asInstanceOf[String],
    introInfo.get("cond").asInstanceOf[Option[String]]
  )

private def decodeOperator(name: String, body: toml4j.Toml): TomlOperator =
  TomlOperator(
    name,
    body.getString("formatter"),
    body.getBoolean("clustering"),
    body.getList("rules").toSeq.asInstanceOf[Seq[String]],
    body.toMap().asScala.get("weight") match
      case Some(w: Double) => Some(w)
      case Some(i: Int)    => Some(i.toDouble)
      case Some(l: Long)   => Some(l.toDouble)
      case Some(x)         => throw RuntimeException(f"${x} is not double, int or long")
      case None            => None
  )

private def decodeGenerating(generating: toml4j.Toml): TomlGenerating =
  TomlGenerating(
    generating.getList("topLevels").toSeq.asInstanceOf[Seq[String]],
    generating.getDouble("defaultVariableWeight"),
    generating.getDouble("defaultOperatorWeight"),
    generating.getDouble("depthPenalty")
  )
