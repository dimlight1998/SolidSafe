package invgen

import invgen.stream.MemorizedStream

def genInvFromToml(
    tomlConfig: toml.TomlConfiguration
): MemorizedStream[(ExprTree, Double)] =
  val generatingConfig = tomlToAstGenConfiguration(tomlConfig)
  getTrees(7, generatingConfig)

private def tomlToAstGenConfiguration(tomlConfig: toml.TomlConfiguration): Configuration =
  given toml.TomlConfiguration = tomlConfig
  given opMap: Map[String, (Operator, Option[Double])] = Map.from(
    for tomlOperator <- tomlConfig.operators yield
      val matches = """(\$\d+)""".r.findAllMatchIn(tomlOperator.formatter)
      val arity   = matches.toSeq.map(m => m.group(0).tail.toInt).max
      val formatterFunc = (args: Seq[String]) =>
        (arity to 1 by -1).foldLeft[String](tomlOperator.formatter)((acc, curr) =>
          acc.replace(f"$$$curr", args(curr - 1))
        )
      tomlOperator.name -> (Operator(tomlOperator.name, arity, formatterFunc), tomlOperator.weight)
  )
  given clusterMap: Map[String, Seq[Variable]] = Map.from(
    for cluster <- tomlConfig.clusters
    yield
      val vars = cluster.vars.map(tomlVariable =>
        Variable(
          tomlVariable.display.getOrElse(tomlVariable.name),
          tomlVariable.intros.map(tomlToAstGenIntro)
        )
      )
      cluster.name -> vars
  )
  given varMap: Map[String, (Variable, Option[Double])] =
    Map.from(
      for cluster <- tomlConfig.clusters; tomlVariable <- cluster.vars
      yield tomlVariable.name -> (Variable(
        tomlVariable.display.getOrElse(tomlVariable.name),
        tomlVariable.intros.map(tomlToAstGenIntro)
      ), tomlVariable.weight)
    )
  Configuration(
    tomlConfig.clusters.toSet.map(cluster => cluster.vars.map(tomlToAstGenVariable).toSet),
    tomlConfig.operators.toSet.map(operator => tomlToAstGenOperator(operator)),
    tomlConfig.generating.depthPenalty,
    tomlConfig.generating.topLevels.flatMap(resolveName).toSet
  )

private def tomlToAstGenIntro(tomlIntro: toml.TomlIntro): Intro =
  Intro(tomlIntro.name, tomlIntro.ty, tomlIntro.cond)

private def tomlToAstGenVariable(using
    tomlConfiguration: toml.TomlConfiguration,
    varMap: Map[String, (Variable, Option[Double])]
)(
    tomlVariable: toml.TomlVariable
): VariableConfiguration =
  val (variable, weight) = varMap(tomlVariable.name)
  VariableConfiguration(
    variable,
    weight.getOrElse(tomlConfiguration.generating.defaultVariableWeight)
  )

private def tomlToAstGenOperator(using
    tomlConfiguration: toml.TomlConfiguration,
    opMap: Map[String, (Operator, Option[Double])],
    clusterMap: Map[String, Seq[Variable]],
    varMap: Map[String, (Variable, Option[Double])]
)(
    tomlOperator: toml.TomlOperator
): OperatorConfiguration =
  val (preFilter, postFilter) = getFilters(tomlOperator.rules)
  val (op, weight)            = opMap(tomlOperator.name)
  OperatorConfiguration(
    op,
    tomlOperator.clustering,
    preFilter,
    postFilter,
    weight.getOrElse(tomlConfiguration.generating.defaultOperatorWeight)
  )

private def getFilters(using
    tomlConfiguration: toml.TomlConfiguration,
    opMap: Map[String, (Operator, Option[Double])],
    clusterMap: Map[String, Seq[Variable]],
    varMap: Map[String, (Variable, Option[Double])]
)(rules: Seq[String]): (PreFilter, PostFilter) =
  val context = new RuleParser.Context:
    def checkContained(lhs: Variable | Operator, rhs: String): Boolean =
      val rhsNames = expandName(rhs)
      lhs match
        case lhs: Variable => rhsNames contains lhs.name
        case lhs: Operator => rhsNames contains lhs.name
    def checkDistinct(args: Seq[ExprTree]): Boolean =
      args.toSet.size == args.size
    def checkCommutative(args: Seq[ExprTree]): Boolean =
      val hashes = args.map(_.hashCode)
      hashes.zip(hashes.tail).forall((x, y) => x <= y)
    def checkUniqueLeafs(args: Seq[ExprTree]): Boolean =
      def extract(tree: ExprTree): Seq[Variable] = tree match
        case VariableLeaf(v, _)        => Seq(v)
        case OperatorNode(_, trees, _) => trees.flatMap(extract)
      val vars = args.flatMap(extract)
      vars.length == vars.distinct.length
  RuleParser.parseRulesToFilter(rules)(using context)

private def expandName(using
    opMap: Map[String, (Operator, Option[Double])],
    clusterMap: Map[String, Seq[Variable]],
    varMap: Map[String, (Variable, Option[Double])]
)(
    name: String
): Set[String] =
  if varMap contains name then Set(varMap(name)(0).name)
  else if opMap contains name then Set(name)
  else if clusterMap contains name then clusterMap(name).map(vc => vc.name).toSet
  else throw RuntimeException(f"name $name not found")

private def resolveName(using
    opMap: Map[String, (Operator, Option[Double])],
    clusterMap: Map[String, Seq[Variable]],
    varMap: Map[String, (Variable, Option[Double])]
)(
    name: String
): Set[Variable | Operator] =
  if varMap contains name then Set(varMap(name)(0))
  else if opMap contains name then Set(opMap(name)(0))
  else if clusterMap contains name then clusterMap(name).toSet
  else throw RuntimeException(f"name $name not found")

def exprTreeWithIntros(
    exprTree: ExprTree
): String =
  val introsMap = collectIntroducedVars(exprTree).groupBy(intro => intro.name)
  val qvarsWithConds =
    for (name, intros) <- introsMap.toSeq
    yield
      val introsSeq = intros.toSeq
      val ty        = introsSeq.head.ty
      assert(introsSeq.forall(_.ty == ty))
      val conds = introsSeq.flatMap(_.cond).map(cond => f"($cond)")
      (f"$name:$ty", conds)
  val (qvars, conds) = qvarsWithConds.unzip
  (qvars, conds.flatten) match
    case (Nil, Nil)     => exprTree.toString()
    case (qvars, Nil)   => f"forall ${qvars.mkString(", ")} :: ${exprTree.toString()}"
    case (qvars, conds) => f"forall ${qvars.mkString(", ")} :: ${conds.mkString(" && ")} ==> ${exprTree.toString()}"

def fetchNextBatch(
    iter: Iterator[(ExprTree, Double)],
    batchSize: Int
): Seq[String] =
  (0 until batchSize).map(_ => iter.nextOption()).filter(_.isDefined).map(opt => exprTreeWithIntros(opt.get(0)))
