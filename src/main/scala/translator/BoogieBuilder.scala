package translator

object BoogieBuilder:
  def uintMin(width: Int): String                           = f"#uint$width.min"
  def uintMax(width: Int): String                           = f"#uint$width.max"
  def intMin(width: Int): String                            = f"#int$width.min"
  def intMax(width: Int): String                            = f"#int$width.max"
  def initializer: String                                   = "#initializer"
  def fallback: String                                      = "#fallback"
  def external: String                                      = "#external"
  def addrCall: String                                      = "#addrCall"
  def blockhash: String                                     = "#blockhash"
  def abiEncode: String                                     = "#abi.encode"
  def keccak256: String                                     = "#keccak256"
  def lifeCycle: String                                     = "#lifecycle"
  def addressThis: String                                   = "#this"
  def blockTimestamp: String                                = "#block.timestamp"
  def blockTimestampLast: String                            = "#block.timestamp.last"
  def blockNumber: String                                   = "#block.number"
  def blockCoinbase: String                                 = "#block.coinbase"
  def blockDifficulty: String                               = "#block.difficulty"
  def blockGaslimit: String                                 = "#block.gaslimit"
  def balance: String                                       = "#balance"
  def msgSender: String                                     = "#msg.sender"
  def msgValue: String                                      = "#msg.value"
  def msgData: String                                       = "#msg.data"
  def destructed: String                                    = "#destructed"
  def identWithId(ident: String, id: Int): String           = f"$ident^$id"
  def constructorOfContract(contractName: String): String   = f"#constructor`$contractName"
  def interfaceStub(functionName: String, id: Int): String  = f"#stub`$functionName^$id"
  def nameOfLengthVar(baseName: String, depth: Int): String = f"#len${"#" * depth}`$baseName"
  def typeOfLengthVar(depth: Int): boogie.Type =
    depth match
      case 0 => boogie.BoogieInt()
      case d => boogie.Mapping(boogie.BoogieInt(), typeOfLengthVar(d - 1))
  def nameOfSumVar(baseName: String, depth: Int): String = f"#sum${"#" * depth}`$baseName"
  def typeOfSumVar(depth: Int): boogie.Type =
    depth match
      case 0 => boogie.BoogieInt()
      case d => boogie.Mapping(boogie.BoogieInt(), typeOfSumVar(d - 1))
  def quantifiedVar(baseName: String) = f"#quan#$baseName"
  def originalParam(baseName: String) = f"#orig#$baseName"
  def normalizedParams(params: Seq[(String, boogie.Type)]): Seq[(String, boogie.Type)] =
    params.zipWithIndex map {
      case (("", ty), index) => (f"#arg$index", ty)
      case (param, _)        => param
    }
  def normalizedReturnParams(returnParams: Seq[(String, boogie.Type)]): Seq[(String, boogie.Type)] =
    returnParams.zipWithIndex map {
      case (("", ty), index) => (f"#ret$index", ty)
      case (returnParam, _)  => returnParam
    }
  def implicitParams: Seq[(String, boogie.Type)] =
    Seq(("#msg.sender", boogie.Address()), ("#msg.value", boogie.BoogieInt()), ("#msg.data", boogie.Reference()))
  def explicitParamsWithPrefix(procName: String, params: Seq[(String, boogie.Type)]): Seq[(String, boogie.Type)] =
    params.drop(implicitParams.length).map { case (paramName, ty) => (f"#param#$procName#$paramName", ty) }
  def normalizedReturnParamsWithPrefix(
      procName: String,
      returnParams: Seq[(String, boogie.Type)]
  ): Seq[(String, boogie.Type)] =
    returnParams.zipWithIndex map {
      case (("", ty), index)      => (f"#result#$procName##ret$index", ty)
      case ((returnParam, ty), _) => (f"#result#$procName#$returnParam", ty)
    }
  def invocationWithImplicitParams(
      attrs: Seq[String],
      funcName: String,
      params: Seq[boogie.Expression],
      returnParams: Seq[boogie.Expression]
  ): boogie.Statement =
    boogie.Call(attrs, funcName, implicitParams.map((name, _) => boogie.Identifier(name)) ++ params, returnParams)
  def memberSep: String                                        = "."
  def nextTempVar(): String                                    = "#temp" + utils.IncrementalId.nextId("boogieTemp")
  def nextRef(): Int                                           = utils.IncrementalId.nextId("boogieRef")
  def messageAttr(srcBegin: Int, srcEnd: Int, message: String) = f":message \"$srcBegin:$srcEnd:$message\""
  def assert(expr: boogie.Expression, srcBegin: Int, srcEnd: Int, message: String) =
    boogie.Assert(Seq(messageAttr(srcBegin, srcEnd, message)), expr)
