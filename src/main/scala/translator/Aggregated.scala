package translator
import TranslatorType as TT
import Signedness.*
import translator.dsl.*
import translator.dsl.given

case class ContentVar(
    members: Seq[String],
    indices: Seq[boogie.Type],
    contentType: boogie.Type,
    signedness: Signedness
):
  def buildName(rootName: String): String = (rootName +: members).mkString(BoogieBuilder.memberSep)
  def buildExpr(rootName: String, indices: Seq[boogie.Expression]) =
    indices.foldLeft[boogie.Expression](buildName(rootName))(_(_))
  def boogieType: boogie.Type = indices.foldRight(contentType)((elem, acc) => boogie.Mapping(elem, acc))
case class SumVar(
    members: Seq[String],
    indices: Seq[boogie.Type],
    depth: Int,
    assocContent: ContentVar
):
  def buildName(rootName: String): String =
    BoogieBuilder.nameOfSumVar((rootName +: members).mkString(BoogieBuilder.memberSep), depth)
  def buildExpr(rootName: String, indices: Seq[boogie.Expression]) =
    indices.foldLeft[boogie.Expression](buildName(rootName))(_(_))
  def boogieType: boogie.Type =
    indices.foldRight[boogie.Type](boogie.BoogieInt())((elem, acc) => boogie.Mapping(elem, acc))
case class LengthVar(
    members: Seq[String],
    indices: Seq[boogie.Type],
    arrayDepth: Int
):
  def buildName(rootName: String): String =
    BoogieBuilder.nameOfLengthVar((rootName +: members).mkString(BoogieBuilder.memberSep), arrayDepth)
  def buildExpr(rootName: String, indices: Seq[boogie.Expression]) =
    indices.foldLeft[boogie.Expression](buildName(rootName))(_(_))
  def boogieType: boogie.Type =
    indices.foldRight[boogie.Type](boogie.BoogieInt())((elem, acc) => boogie.Mapping(elem, acc))
case class EncodeResult(
    contents: Seq[ContentVar],
    sums: Seq[SumVar],
    lengths: Seq[LengthVar]
)

def encodeBoogieVarsForContent(translatorType: TranslatorType): Seq[ContentVar] =
  translatorType match
    case TT.SignedInt(_)   => Seq(ContentVar(Nil, Nil, boogie.BoogieInt(), Signed))
    case TT.UnsignedInt(_) => Seq(ContentVar(Nil, Nil, boogie.BoogieInt(), Unsigned))
    case TT.EnumInt(_)     => Seq(ContentVar(Nil, Nil, boogie.BoogieInt(), Unsigned))
    case TT.Boolean        => Seq(ContentVar(Nil, Nil, boogie.BoogieBoolean(), Neither))
    case TT.Address        => Seq(ContentVar(Nil, Nil, boogie.Address(), Neither))
    case TT.StringRef      => Seq(ContentVar(Nil, Nil, boogie.Reference(), Neither))
    case TT.BytesRef       => Seq(ContentVar(Nil, Nil, boogie.Reference(), Neither))
    case TT.Array(elem) =>
      for elemResult <- encodeBoogieVarsForContent(elem)
      yield elemResult.copy(indices = boogie.BoogieInt() +: elemResult.indices)
    case TT.Mapping(key, value) =>
      val keyType = simpleTranslatorTypeToBoogieType(key)
      for valueResult <- encodeBoogieVarsForContent(value)
      yield valueResult.copy(indices = keyType +: valueResult.indices)
    case TT.Struct(members) =>
      for
        (memberName, memberType) <- members
        memberResult             <- encodeBoogieVarsForContent(memberType)
      yield memberResult.copy(members = memberName +: memberResult.members)
    case TT.Interface(_) => Seq(ContentVar(Nil, Nil, boogie.Address(), Neither))
    case fallback @ _    => pprint.pprintln(fallback); ???

def encodeBoogieVarsForSum(translatorType: TranslatorType): Seq[SumVar] =
  for
    contentVar <- encodeBoogieVarsForContent(translatorType)
    if contentVar.signedness != Neither && contentVar.indices.nonEmpty
    prefix <- contentVar.indices.inits.toSeq.tail
  yield SumVar(contentVar.members, prefix, prefix.length, contentVar)

def encodeBoogieVarsForLength(translatorType: TranslatorType): Seq[LengthVar] =
  translatorType match
    case TT.Array(elem) =>
      val elemResults =
        for elemResult <- encodeBoogieVarsForLength(elem)
        yield LengthVar(elemResult.members, boogie.BoogieInt() +: elemResult.indices, elemResult.arrayDepth + 1)
      val toplevel = LengthVar(Nil, Nil, 0)
      toplevel +: elemResults
    case TT.Mapping(key, value) =>
      val keyType = simpleTranslatorTypeToBoogieType(key)
      for valueResult <- encodeBoogieVarsForLength(value)
      yield valueResult.copy(indices = keyType +: valueResult.indices)
    case TT.Struct(structMembers) =>
      for
        (memberName, memberType) <- structMembers
        memberResult             <- encodeBoogieVarsForLength(memberType)
      yield memberResult.copy(members = memberName +: memberResult.members)
    case _ => Nil

def encodeBoogieVars(translatorType: TranslatorType): EncodeResult =
  EncodeResult(
    encodeBoogieVarsForContent(translatorType),
    encodeBoogieVarsForSum(translatorType),
    encodeBoogieVarsForLength(translatorType)
  )

def initAggregated(
    translatorType: TranslatorType,
    rootName: String,
    members: Seq[String],
    indices: Seq[boogie.Expression],
    arrayDepth: Int
): Seq[boogie.Expression] =
  def buildName = (rootName +: members).mkString(BoogieBuilder.memberSep)
  def buildExpr = indices.foldLeft[boogie.Expression](buildName)(_(_))
  translatorType match
    case TT.UnsignedInt(_) => Seq(buildExpr #== 0)
    case TT.Boolean        => Seq(buildExpr #== false)
    case TT.Address        => Seq(buildExpr #== 0)
    case TT.StringRef      => Seq(buildExpr #== BoogieBuilder.nextRef())
    case TT.BytesRef       => Seq(buildExpr #== BoogieBuilder.nextRef())
    case TT.Array(elem) =>
      Seq(indices.foldLeft[boogie.Expression](BoogieBuilder.nameOfLengthVar(buildName, arrayDepth))(_(_)) #== 0)
    case TT.Struct(structMembers) =>
      for (name, ty) <- structMembers; relation <- initAggregated(ty, rootName, name +: members, indices, arrayDepth)
      yield relation
    case TT.Interface(_) => Seq(buildExpr #== 0)
    case fallback @ _    => pprint.pprintln(fallback); ???
