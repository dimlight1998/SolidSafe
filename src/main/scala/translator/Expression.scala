package translator

import translator.dsl.*
import translator.dsl.given

/** If `xs` has type `int[]`, then translating `xs` will be an `AggregatedVarGroup`,
  * which maintains information of aggregated types.
  * Such information if useful when `xs` is parameter or return parameter.
  */
case class AggregatedVarGroup(
    rootName: String,
    members: Seq[String],
    contents: Seq[(ContentVar, Seq[boogie.Expression])],
    sums: Seq[(SumVar, Seq[boogie.Expression])],
    lengths: Seq[(LengthVar, Seq[boogie.Expression])]
):
  def onlyContent: boogie.Expression =
    require(contents.length == 1)
    val (cv, indices) = contents.head
    val name          = (rootName +: cv.members).mkString(BoogieBuilder.memberSep)
    indices.foldLeft[boogie.Expression](name)(_(_))
  def sumsManagingOnlyContent: Seq[boogie.Expression] =
    require(contents.length == 1)
    val (cv, indices) = contents.head
    val baseName      = (rootName +: cv.members).mkString(BoogieBuilder.memberSep)
    if cv.signedness != Signedness.Neither && indices.length > 0 then
      for prefix <- indices.inits.toSeq.tail
      yield
        val name = BoogieBuilder.nameOfSumVar(baseName, prefix.length)
        prefix.foldLeft[boogie.Expression](name)(_(_))
    else Nil
  def flatten: Seq[boogie.Expression] =
    val contentVars = for (cv, indices) <- contents yield
      val name = (rootName +: cv.members).mkString(BoogieBuilder.memberSep)
      indices.foldLeft[boogie.Expression](name)(_(_))
    val sumVars = for (sv, indices) <- sums yield
      val name = BoogieBuilder.nameOfSumVar((rootName +: sv.members).mkString(BoogieBuilder.memberSep), sv.depth)
      indices.foldLeft[boogie.Expression](name)(_(_))
    val lengthVars = for (lv, indices) <- lengths yield
      val name =
        BoogieBuilder.nameOfLengthVar((rootName +: lv.members).mkString(BoogieBuilder.memberSep), lv.arrayDepth)
      indices.foldLeft[boogie.Expression](name)(_(_))
    contentVars ++ sumVars ++ lengthVars

case class ScalarExpression(
    expr: boogie.Expression
)

case class ExpressionTranslationResult(
    results: Seq[AggregatedVarGroup | ScalarExpression],
    sideEffects: Seq[boogie.Statement],
    localVars: Seq[(String, boogie.Type)]
):
  def expr: boogie.Expression =
    require(results.length == 1)
    results.head match
      case AggregatedVarGroup(rootName, _, Seq((contentVar, indices)), Nil, Nil) =>
        val ident = boogie.Identifier((rootName +: contentVar.members).mkString(BoogieBuilder.memberSep))
        indices.foldLeft[boogie.Expression](ident)((acc, elem) => boogie.IndexAccess(acc, elem))
      case ScalarExpression(expr) => expr
      case fallback @ _           => pprint.pprintln(fallback); assert(false)
  def flatten: Seq[boogie.Expression] =
    results.flatMap {
      case group: AggregatedVarGroup => group.flatten
      case ScalarExpression(expr)    => Seq(expr)
    }
  def singleAggregatedVarGroup: AggregatedVarGroup =
    require(results.length == 1 && results.head.isInstanceOf[AggregatedVarGroup])
    results.head.asInstanceOf[AggregatedVarGroup]

def withIndex(varGroup: AggregatedVarGroup, boogieIndex: boogie.Expression): AggregatedVarGroup =
  val AggregatedVarGroup(rootName, members, contents, sums, lengths) = varGroup
  val indexedContents =
    for (cv, indices) <- contents yield (cv, indices :+ boogieIndex)
  val indexedSums =
    for (sv, indices) <- sums if sv.indices.length > indices.length yield (sv, indices :+ boogieIndex)
  val indexedLengths =
    for (lv, indices) <- lengths if lv.indices.length > indices.length yield (lv, indices :+ boogieIndex)
  AggregatedVarGroup(rootName, members, indexedContents, indexedSums, indexedLengths)

/** This function translate a solidity expression to a boogie expression.
  *
  * Some additional side-effects should also be generated in the form of statements, such as checking array
  * accessing is always in range.
  */
def translateExpression(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    expr: solidity.ASTNode
): ExpressionTranslationResult =
  expr match
    case solidity.Identifier(ref, "#BALANCE") =>
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.balance)), Nil, Nil)
    case solidity.Identifier(ref, "this") if ref < 0 =>
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.addressThis)), Nil, Nil)
    case solidity.Identifier(ref, "tx") if ref < 0 =>
      val contents = Seq(
        (ContentVar(Seq("origin"), Nil, boogie.Address(), Signedness.Neither), Nil),
        (ContentVar(Seq("gasprice"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil)
      )
      // TODO: make #tx a special variable in BoogieBuilder
      ExpressionTranslationResult(Seq(AggregatedVarGroup("#tx", Nil, contents, Nil, Nil)), Nil, Nil)
    case solidity.Identifier(ref, "msg") if ref < 0 =>
      val contents = Seq(
        (ContentVar(Seq("data"), Nil, boogie.Reference(), Signedness.Neither), Nil),
        (ContentVar(Seq("sender"), Nil, boogie.Address(), Signedness.Neither), Nil),
        (ContentVar(Seq("value"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil)
      )
      // TODO: make #msg a special variable in BoogieBuilder
      ExpressionTranslationResult(Seq(AggregatedVarGroup("#msg", Nil, contents, Nil, Nil)), Nil, Nil)
    case solidity.Identifier(ref, "block") if ref < 0 =>
      val contents = Seq(
        (ContentVar(Seq("coinbase"), Nil, boogie.Address(), Signedness.Neither), Nil),
        (ContentVar(Seq("difficulty"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil),
        (ContentVar(Seq("gaslimit"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil),
        (ContentVar(Seq("number"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil),
        (ContentVar(Seq("timestamp"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil)
      )
      // TODO: make #block a special variable in BoogieBuilder
      ExpressionTranslationResult(Seq(AggregatedVarGroup("#block", Nil, contents, Nil, Nil)), Nil, Nil)
    case solidity.Identifier(ref, name) if ref <= 0 =>
      // NOTE: if the identifier has no referencedDeclaration, it must be a quantified variable
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.quantifiedVar(name))), Nil, Nil)
    case solidity.Identifier(ref, name) if ref > 0 =>
      ctx.nodeMap(ref) match
        case varDecl @ solidity.VariableDeclaration(name, _, typeName) =>
          val boogieVars           = encodeBoogieVars(typeNameToTranslatorType(typeName))
          val contents             = boogieVars.contents.map((_, Nil))
          val sums                 = boogieVars.sums.map((_, Nil))
          val lengths              = boogieVars.lengths.map((_, Nil))
          lazy val isFunctionParam = variableIsFunctionParameter(varDecl)
          val rootName = (translationOptions.replaceFuncArgsToOrigin, isFunctionParam) match
            case (true, true) => BoogieBuilder.originalParam(BoogieBuilder.identWithId(name, ref))
            case _            => BoogieBuilder.identWithId(name, ref)
          val varGroup = AggregatedVarGroup(rootName, Nil, contents, sums, lengths)
          ExpressionTranslationResult(Seq(varGroup), Nil, Nil)
        case solidity.EnumDefinition(_, members) =>
          val contents =
            for member <- members
            yield (
              ContentVar(
                Seq(member.asInstanceOf[solidity.EnumValue].name),
                Nil,
                boogie.BoogieInt(),
                Signedness.Unsigned
              ),
              Nil
            )
          ExpressionTranslationResult(
            Seq(AggregatedVarGroup(BoogieBuilder.identWithId(name, ref), Nil, contents, Nil, Nil)),
            Nil,
            Nil
          )
        case fallback @ _ => pprint.pprintln(fallback); assert(false)
    case solidity.Literal(value, _) if value.matches("0x[a-fA-F0-9]+") =>
      // this is an address; convert it to integer value
      ExpressionTranslationResult(Seq(ScalarExpression(BigInt(value.substring(2), 16).toString)), Nil, Nil)
    case solidity.Literal(_, "string") =>
      // unsupported; always create a fresh reference (integer type) for string literals
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
    case solidity.Literal(value, "number") =>
      val sepRemoved = value.flatMap {
        case '_' => ""
        case ch  => ch.toString
      }
      // this is a number, maybe in scientific notation
      """(\d+)e(\d+)""".r.findAllMatchIn(sepRemoved).toSeq match
        case Seq(m) =>
          val evaluated = BigInt(m.group(1)).pow((m.group(2)).toInt).toString
          ExpressionTranslationResult(Seq(ScalarExpression(boogie.Literal(evaluated))), Nil, Nil)
        case Nil          => ExpressionTranslationResult(Seq(ScalarExpression(boogie.Literal(sepRemoved))), Nil, Nil)
        case fallback @ _ => throw RuntimeException("Should not happen:\n" + pprint.tokenize(fallback).mkString("\n"))
    case solidity.Literal(value, "bool") =>
      // directly copy boolean values
      ExpressionTranslationResult(Seq(ScalarExpression(value)), Nil, Nil)
    case solidity.UnaryOperation("!", subExpr) =>
      val subResult = translateExpression(subExpr)
      ExpressionTranslationResult(
        Seq(ScalarExpression(boogie.UnaryOperation("!", subResult.expr))),
        subResult.sideEffects,
        subResult.localVars
      )
    case solidity.UnaryOperation(op @ ("++" | "--"), subExpr) =>
      // TODO: distinguish i++ and ++i
      // change to aggregated data type also changes sum variables
      val subResult = translateExpression(subExpr)
      val updated = op match
        case "++" => (expr: boogie.Expression) => expr #+ 1
        case "--" => (expr: boogie.Expression) => expr #- 1
      val contentUpdate = subResult.expr #:= updated(subResult.expr)
      val sumVars       = subResult.results.head.asInstanceOf[AggregatedVarGroup].sumsManagingOnlyContent
      val sumVarUpdates = for sumVar <- sumVars yield sumVar #:= updated(sumVar)
      ExpressionTranslationResult(
        Seq(ScalarExpression(subResult.expr #- 1)),
        subResult.sideEffects ++ (contentUpdate +: sumVarUpdates),
        subResult.localVars
      )
    case solidity.UnaryOperation("delete", subExpr) =>
      val subRes        = translateExpression(subExpr)
      val initRelations = initializeExpr(subExpr)
      // TODO: what about length
      ExpressionTranslationResult(
        Nil,
        subRes.sideEffects ++ initRelations.map(boogie.Assume(Seq(":reason \"deleted to initial value\""), _)),
        subRes.localVars
      )
    case solidity.BinaryOperation("**", lhsExpr, rhsExpr) =>
      val lhsRes = translateExpression(lhsExpr)
      val rhsRes = translateExpression(rhsExpr)
      // we only support exponential of literals
      (lhsRes.expr, rhsRes.expr) match
        case (boogie.Literal(lhsValue), boogie.Literal(rhsValue)) =>
          val evaluated = boogie.Literal(BigInt(lhsValue.toInt).pow(rhsValue.toInt).toString)
          ExpressionTranslationResult(Seq(ScalarExpression(evaluated)), Nil, Nil)
        case fallback @ _ =>
          pprint.pprintln(fallback); throw RuntimeException("only support power of literals")
    case solidity.BinaryOperation(op @ ("+" | "-" | "*"), lhsExpr, rhsExpr) =>
      val lhsRes = translateExpression(lhsExpr)
      val rhsRes = translateExpression(rhsExpr)
      // for these kinds of operators, we check for overflows and underflows
      def isAllLiteral(e: solidity.ASTNode): Boolean = e match
        case solidity.Literal(_, _)            => true
        case solidity.BinaryOperation(_, l, r) => isAllLiteral(l) && isAllLiteral(r)
        case _                                 => false
      val boogieBinOp = boogie.BinaryOperation(op, lhsRes.expr, rhsRes.expr)
      val overflowCheck = (translationOptions.checkArithmeticOverflow, isAllLiteral(expr)) match
        case (false, _)   => Nil
        case (true, true) => Nil
        case (true, false) =>
          val valueAssumptions =
            valueRangeAssumptions(lhsRes.expr, lhsExpr) ++ valueRangeAssumptions(rhsRes.expr, rhsExpr)
          val checks = checkOverflow(expr, boogieBinOp, expr.srcBegin, expr.srcEnd)
          valueAssumptions ++ checks
      ExpressionTranslationResult(
        Seq(ScalarExpression(boogieBinOp)),
        lhsRes.sideEffects ++ rhsRes.sideEffects ++ overflowCheck,
        lhsRes.localVars ++ rhsRes.localVars
      )
    case solidity.BinaryOperation(
          op @ ("/" | "%" | "&&" | "||" | ">" | "<" | ">=" | "<=" | "==" | "!="),
          lhsExpr,
          rhsExpr
        ) =>
      val lhsRes = translateExpression(lhsExpr)
      val rhsRes = translateExpression(rhsExpr)
      val boogieOp = op match
        case "/" => "div"
        case "%" => "mod"
        case op  => op
      ExpressionTranslationResult(
        Seq(ScalarExpression(boogie.BinaryOperation(boogieOp, lhsRes.expr, rhsRes.expr))),
        lhsRes.sideEffects ++ rhsRes.sideEffects,
        lhsRes.localVars ++ rhsRes.localVars
      )
    case solidity.Conditional(cond, trueExpr, falseExpr) =>
      // TODO: only scalar type is considered
      val result     = BoogieBuilder.nextTempVar()
      val resultType = simpleTranslatorTypeToBoogieType(typeOfExpression(trueExpr))
      val condRes    = translateExpression(cond)
      val trueRes    = translateExpression(trueExpr)
      val falseRes   = translateExpression(falseExpr)
      val structure = boogie.IfElse(
        condRes.expr,
        trueRes.sideEffects :+ (result #:= trueRes.expr),
        falseRes.sideEffects :+ (result #:= falseRes.expr)
      )
      ExpressionTranslationResult(
        Seq(ScalarExpression(result)),
        condRes.sideEffects :+ structure,
        (result, resultType) +: (condRes.localVars ++ trueRes.localVars ++ falseRes.localVars)
      )
    case solidity.FunctionCall(_, _, _) => translateFunctionCall(expr.asInstanceOf[solidity.FunctionCall])
    case solidity.Assignment("=", _, _) => translateAssignment(expr.asInstanceOf[solidity.Assignment])
    case solidity.Assignment(op @ ("+=" | "-=" | "*="), lhsExpr, rhsExpr) =>
      val lhsRes = translateExpression(lhsExpr)
      val rhsRes = translateExpression(rhsExpr)
      val updated = op match
        case "+=" => (l: boogie.Expression, r: boogie.Expression) => l #+ r
        case "-=" => (l: boogie.Expression, r: boogie.Expression) => l #- r
        case "*=" => (l: boogie.Expression, r: boogie.Expression) => l #* r
      val boogieRhs = updated(lhsRes.expr, rhsRes.expr)
      val overflowCheck = translationOptions.checkArithmeticOverflow match
        case false => Nil
        case true =>
          val valueAssumptions =
            valueRangeAssumptions(lhsRes.expr, lhsExpr) ++ valueRangeAssumptions(rhsRes.expr, rhsExpr)
          val checks = checkOverflow(lhsExpr, boogieRhs, expr.srcBegin, expr.srcEnd)
          valueAssumptions ++ checks
      val sumVars = lhsRes.results.head.asInstanceOf[AggregatedVarGroup].sumsManagingOnlyContent
      val sumVarUpdates =
        for sumVar <- sumVars yield sumVar #:= ((sumVar #- lhsRes.expr) #+ boogieRhs)
      ExpressionTranslationResult(
        Nil,
        lhsRes.sideEffects ++ rhsRes.sideEffects ++ overflowCheck ++ sumVarUpdates :+ (lhsRes.expr #:= boogieRhs),
        lhsRes.localVars ++ rhsRes.localVars
      )
    case solidity.IndexAccess(baseExpr, _) if typeOfExpression(baseExpr) == TranslatorType.BytesRef =>
      // return a havoced uint
      var tempVar = BoogieBuilder.nextTempVar()
      var havoc   = boogie.Havoc(tempVar)
      var byteVal = boogie.Assume(Seq(":reason \"this is a byte\""), (tempVar #>= 0) #&& (tempVar #< 8))
      ExpressionTranslationResult(
        Seq(ScalarExpression(tempVar)),
        Seq(havoc, byteVal),
        Seq((tempVar, boogie.BoogieInt()))
      )
    case solidity.IndexAccess(baseExpr, indexExpr) =>
      val baseRes  = translateExpression(baseExpr)
      val indexRes = translateExpression(indexExpr)
      assert(baseRes.results.length == 1 && baseRes.results.head.isInstanceOf[AggregatedVarGroup])
      val varGroup        = baseRes.singleAggregatedVarGroup
      val lengths         = varGroup.lengths
      val indexedVarGroup = withIndex(varGroup, indexRes.expr)
      val indexOutOfBoundsCheck = (translationOptions.checkArrayIndexOutOfBounds, typeOfExpression(baseExpr)) match
        case (false, _) => Nil
        case (true, _: TranslatorType.Array) =>
          val valueAssumptions     = valueRangeAssumptions(indexRes.expr, indexExpr)
          val (lengthVar, indices) = lengths.find((lv, indices) => lv.indices.length == indices.length).get
          val baseExpr = boogie.Identifier(
            BoogieBuilder.nameOfLengthVar(
              (varGroup.rootName +: lengthVar.members).mkString(BoogieBuilder.memberSep),
              lengthVar.arrayDepth
            )
          )
          val withIndices = indices.foldLeft[boogie.Expression](baseExpr)(_(_))
          val checks      = checkArrayIndexOutOfBounds(indexRes.expr, withIndices, expr.srcBegin, expr.srcEnd)
          valueAssumptions ++ checks
        case (true, _) => Nil
      ExpressionTranslationResult(
        Seq(indexedVarGroup),
        baseRes.sideEffects ++ indexRes.sideEffects ++ indexOutOfBoundsCheck,
        baseRes.localVars ++ indexRes.localVars
      )
    case solidity.MemberAccess(expr, "length", _) if typeOfExpression(expr).isInstanceOf[TranslatorType.Array] =>
      val res                                            = translateExpression(expr)
      val AggregatedVarGroup(rootName, _, _, _, lengths) = res.singleAggregatedVarGroup
      val (lengthVar, indices) = lengths.find((lv, indices) => lv.indices.length == indices.length).get
      val baseExpr = boogie.Identifier(
        BoogieBuilder.nameOfLengthVar(
          (rootName +: lengthVar.members).mkString(BoogieBuilder.memberSep),
          lengthVar.arrayDepth
        )
      )
      val withIndices = indices.foldLeft[boogie.Expression](baseExpr)(_(_))
      ExpressionTranslationResult(
        Seq(ScalarExpression(withIndices)),
        res.sideEffects,
        res.localVars
      )
    case solidity.MemberAccess(expr, "length", _) if typeOfExpression(expr) == TranslatorType.BytesRef =>
      // length of bytes are not handled
      var tempVar      = BoogieBuilder.nextTempVar()
      var havoc        = boogie.Havoc(tempVar)
      var lengthNonNeg = boogie.Assume(Seq(":reason \"length is non-negative\""), tempVar #>= 0)
      ExpressionTranslationResult(
        Seq(ScalarExpression(tempVar)),
        Seq(havoc, lengthNonNeg),
        Seq((tempVar, boogie.BoogieInt()))
      )
    case solidity.MemberAccess(expr, "balance", _) if typeOfExpression(expr) == TranslatorType.Address =>
      val res                = translateExpression(expr)
      val ret                = BoogieBuilder.nextTempVar()
      val assignBalanceToRet = ret #:= BoogieBuilder.balance
      val havocRet           = boogie.Havoc(ret)
      val balanceNonNeg      = boogie.Assume(Seq(":reason \"balance is non-negative\""), ret #>= 0)
      val computeRet = boogie.IfElse(res.expr #== BoogieBuilder.addressThis, Seq(assignBalanceToRet), Seq(havocRet))
      ExpressionTranslationResult(
        Seq(ScalarExpression(ret)),
        res.sideEffects ++ Seq(computeRet, balanceNonNeg),
        res.localVars :+ (ret, boogie.BoogieInt())
      )
    case solidity.MemberAccess(expr, "interfaceId", _) if typeOfExpression(expr) == TranslatorType.InterfaceMeta =>
      // unsupported; always create a fresh reference (integer type) for string literals
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
    case solidity.MemberAccess(expr, memberName, _) =>
      val res                                                            = translateExpression(expr)
      val AggregatedVarGroup(rootName, members, contents, sums, lengths) = res.singleAggregatedVarGroup
      val selectedMembers                                                = members :+ memberName
      val selectedContents = for (cv, indices) <- contents if cv.members.startsWith(selectedMembers) yield (cv, indices)
      val selectedSums     = for (sv, indices) <- sums if sv.members.startsWith(selectedMembers) yield (sv, indices)
      val selectedLengths  = for (lv, indices) <- lengths if lv.members.startsWith(selectedMembers) yield (lv, indices)
      val selectedGroup = AggregatedVarGroup(rootName, selectedMembers, selectedContents, selectedSums, selectedLengths)
      ExpressionTranslationResult(
        Seq(selectedGroup),
        res.sideEffects,
        res.localVars
      )
    case solidity.TupleExpression(components) =>
      val results = for component <- components yield translateExpression(component)
      assert(results.forall(_.results.length == 1))
      ExpressionTranslationResult(
        results.map(_.results.head),
        results.flatMap(_.sideEffects),
        results.flatMap(_.localVars)
      )
    case fallback @ _ => pprint.pprintln(fallback); ???

def translateFunctionCall(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    funcCall: solidity.FunctionCall
): ExpressionTranslationResult =
  funcCall.expression match
    case solidity.FunctionCallOptions(expr, options) =>
      // TODO: refactor translateFunctionoCall so don't need to construct a fake one
      val fakeCall = solidity.FunctionCall(expr, funcCall.arguments, funcCall.kind)
      translateFunctionCall(fakeCall)
    // TODO: handle value in options
    case solidity.Identifier(id, name) if funcCall.kind == "functionCall" && id > 0 =>
      // this is the most boring one
      val argResults  = funcCall.arguments.map(translateExpression(_))
      val args        = argResults.flatMap(_.flatten)
      val sideEffects = argResults.flatMap(_.sideEffects)
      // assign return value to temp variables
      val funcDef = ctx.nodeMap(key = id).asInstanceOf[solidity.FunctionDefinition]
      val retTempGroups =
        for returnParam <- funcDef.returnParameters.asInstanceOf[solidity.ParameterList].parameters
        yield
          val retTempVar = BoogieBuilder.nextTempVar()
          val result = encodeBoogieVars(
            typeNameToTranslatorType(returnParam.asInstanceOf[solidity.VariableDeclaration].typeName)
          )
          AggregatedVarGroup(
            retTempVar,
            Nil,
            result.contents.map((_, Nil)),
            result.sums.map((_, Nil)),
            result.lengths.map((_, Nil))
          )
      val retTempVars = for group <- retTempGroups yield
        val contents = for (cv, indices) <- group.contents yield (cv.buildName(group.rootName), cv.boogieType)
        val sums     = for (sv, indices) <- group.sums yield (sv.buildName(group.rootName), sv.boogieType)
        val lengths  = for (lv, indices) <- group.lengths yield (lv.buildName(group.rootName), lv.boogieType)
        contents ++ sums ++ lengths
      ExpressionTranslationResult(
        retTempGroups,
        sideEffects :+ BoogieBuilder.invocationWithImplicitParams(
          Seq(
            BoogieBuilder.messageAttr(funcCall.srcBegin, funcCall.srcEnd, "this invocation may violate a precondition")
          ),
          BoogieBuilder.identWithId(name, id),
          args,
          retTempVars.flatten.map(_._1)
        ),
        argResults.flatMap(_.localVars) ++ retTempVars.flatten
      )
    case solidity.Identifier(id, _) if funcCall.kind == "typeConversion" && id > 0 =>
      // TODO: assuming converting address to contract
      translateExpression(funcCall.arguments.head)
    case solidity.Identifier(id, _) if funcCall.kind == "structConstructorCall" && id > 0 =>
      val solidity.StructDefinition(members) = ctx.nodeMap(id).asInstanceOf[solidity.StructDefinition]
      val argumentsRes                       = funcCall.arguments.map(translateExpression)
      val tempVar                            = BoogieBuilder.nextTempVar()
      val (assignments, contentVars, sumVars, lengthVars) =
        val subResults = for (member, argRes) <- members zip argumentsRes yield
          val decl         = member.asInstanceOf[solidity.VariableDeclaration]
          val encodeResult = encodeBoogieVars(typeNameToTranslatorType(decl.typeName))
          val contentVars  = encodeResult.contents.map(cv => (cv.copy(members = decl.name +: cv.members), Nil))
          val sumVars      = encodeResult.sums.map(sv => (sv.copy(members = decl.name +: sv.members), Nil))
          val lengthVars   = encodeResult.lengths.map(lv => (lv.copy(members = decl.name +: lv.members), Nil))
          val memberGroup  = AggregatedVarGroup(tempVar, Seq(decl.name), contentVars, sumVars, lengthVars)
          val assignments = argRes.results match
            case Seq(argGroup: AggregatedVarGroup) =>
              makeAggregatedAssignment(memberGroup, argGroup)
            case Seq(argExpr: ScalarExpression) =>
              Seq(memberGroup.onlyContent #:= argExpr.expr)
          (assignments, contentVars, sumVars, lengthVars)
        (
          subResults.flatMap(_._1),
          subResults.flatMap(_._2),
          subResults.flatMap(_._3),
          subResults.flatMap(_._4)
        )
      ExpressionTranslationResult(
        Seq(AggregatedVarGroup(tempVar, Nil, contentVars, sumVars, lengthVars)),
        argumentsRes.flatMap(_.sideEffects) ++ assignments,
        argumentsRes.flatMap(_.localVars) ++
          contentVars.map((cv, _) => (cv.buildName(tempVar), cv.boogieType)) ++
          sumVars.map((sv, _) => (sv.buildName(tempVar), sv.boogieType)) ++
          lengthVars.map((lv, _) => (lv.buildName(tempVar), lv.boogieType))
      )
    case solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName(name)) if name.matches("""uint(\d*)""") =>
      translateExpression(funcCall.arguments.head)
    case solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("address")) =>
      // address(arg)
      translateExpression(funcCall.arguments.head)
    case solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName(name)) if name.matches("""bytes(\d*)""") =>
      // bytes(arg)
      // TODO: side effects
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
    case solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("string")) =>
      // string(arg)
      // TODO: side effects
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
    case solidity.Identifier(id, "require") if id < 0 =>
      // require(expr)
      val res = translateExpression(funcCall.arguments.head)
      ExpressionTranslationResult(
        Nil,
        res.sideEffects :+ boogie.Assume(Seq(":reason \"user defined requirement\""), res.expr),
        res.localVars
      )
    case solidity.Identifier(id, "assert") if id < 0 =>
      // assert(expr)
      val res = translateExpression(funcCall.arguments.head)
      val assert =
        BoogieBuilder.assert(res.expr, funcCall.srcBegin, funcCall.srcEnd, "this assertion may be violated")
      ExpressionTranslationResult(Nil, res.sideEffects :+ assert, res.localVars)
    case solidity.Identifier(id, "revert") if id < 0 =>
      // revert()
      ExpressionTranslationResult(Nil, Seq(boogie.Assume(Seq(":reason \"transaction reverted\""), false)), Nil)
    case solidity.Identifier(id, "selfdestruct") if id < 0 =>
      // selfdestruct(...)
      ExpressionTranslationResult(Nil, Seq(BoogieBuilder.destructed #:= true), Nil)
    case solidity.Identifier(id, "blockhash") if id < 0 =>
      callStubFunction(BoogieBuilder.blockhash, funcCall.arguments, Seq(boogie.Reference()))
    case solidity.Identifier(id, "keccak256") if id < 0 =>
      callStubFunction(BoogieBuilder.keccak256, funcCall.arguments, Seq(boogie.Reference()))
    case solidity.Identifier(id, "type") if id < 0 =>
      funcCall.arguments match
        case Seq(solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("uint" | "uint256"))) =>
          val contents = Seq(
            (ContentVar(Seq("min"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil),
            (ContentVar(Seq("max"), Nil, boogie.BoogieInt(), Signedness.Unsigned), Nil)
          )
          // TODO: make #uint special
          ExpressionTranslationResult(Seq(AggregatedVarGroup("#uint256", Nil, contents, Nil, Nil)), Nil, Nil)
        case Seq(solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("int" | "int256"))) =>
          val contents = Seq(
            (ContentVar(Seq("min"), Nil, boogie.BoogieInt(), Signedness.Signed), Nil),
            (ContentVar(Seq("max"), Nil, boogie.BoogieInt(), Signedness.Signed), Nil)
          )
          // TODO: make #uint special
          ExpressionTranslationResult(Seq(AggregatedVarGroup("#int256", Nil, contents, Nil, Nil)), Nil, Nil)
        case fallback @ _ => pprint.pprintln(fallback); ???
    case solidity.Identifier(_, ident) if ident.startsWith("#") =>
      // this is a verification-specific annotation
      translateSpecExpression(ident, funcCall.arguments)
    case solidity.MemberAccess(
          solidity.Identifier(id, "abi"),
          "encode" | "encodeWithSelector" | "encodeWithSignature",
          _
        ) if id < 0 =>
      // TODO: argument side effects
      callStubFunction(BoogieBuilder.abiEncode, funcCall.arguments, Seq(boogie.Reference()))
    case solidity.MemberAccess(solidity.Identifier(id, "abi"), "decode", _) if id < 0 =>
      // TODO: only simple types are considered
      val decodedExprs =
        for expr <- funcCall.arguments(1).asInstanceOf[solidity.TupleExpression].components
        yield expr match
          case solidity.ElementaryTypeNameExpression(typeName) =>
            val tempVar = BoogieBuilder.nextTempVar()
            val decls   = Seq((tempVar, simpleTranslatorTypeToBoogieType(typeNameToTranslatorType(typeName))))
            (decls, ScalarExpression(tempVar))
          case solidity.Identifier(refId, _) =>
            // NOTE: this node is synthesized
            val udt        = solidity.UserDefinedTypeName(refId)
            val tempVar    = BoogieBuilder.nextTempVar()
            val boogieVars = encodeBoogieVars(typeNameToTranslatorType(udt))
            val contents   = for content <- boogieVars.contents yield (content.buildName(tempVar), content.boogieType)
            val sums       = for sum <- boogieVars.sums yield (sum.buildName(tempVar), sum.boogieType)
            val lengths    = for length <- boogieVars.lengths yield (length.buildName(tempVar), length.boogieType)
            val decls      = contents ++ sums ++ lengths
            (
              decls,
              AggregatedVarGroup(
                tempVar,
                Nil,
                boogieVars.contents.map((_, Nil)),
                boogieVars.sums.map((_, Nil)),
                boogieVars.lengths.map((_, Nil))
              )
            )
          case fallback @ _ => pprint.pprintln(fallback); assert(false)
      ExpressionTranslationResult(
        decodedExprs.map((_, result) => result),
        decodedExprs.flatMap((decls, _) => decls.map((name, _) => boogie.Havoc(name))),
        decodedExprs.flatMap((decls, _) => decls)
      )
    case ma: solidity.MemberAccess if ma.memberName == "push" && funcCall.arguments.isEmpty =>
      // TODO: check if expr is actually an array
      val solArray = ma.expression
      // TODO: consider side-effects
      val bplLength = lengthVarsOfExpr(solArray)
      ExpressionTranslationResult(
        Nil,
        initializeExpr(solidity.IndexAccess(solArray, solidity.MemberAccess(solArray, "length", None)))
          .map(boogie.Assume(Nil, _)) :+
          (bplLength #:= bplLength #+ 1),
        Nil
      )
    case ma: solidity.MemberAccess if ma.memberName == "push" && funcCall.arguments.length == 1 =>
      // TODO: check if expr is actually an array
      val result        = translateExpression(ma.expression)
      val arrayVarGroup = result.singleAggregatedVarGroup
      val elem          = translateExpression(funcCall.arguments.head)
      val (lv, indices) = arrayVarGroup.lengths.find((lv, indices) => lv.indices.length == indices.length).get
      val arrayLength   = lv.buildExpr(arrayVarGroup.rootName, indices)
      // assign the element to array[length]
      val indexed = withIndex(arrayVarGroup, arrayLength)
      val assignments = elem.results.head match
        case rhs: AggregatedVarGroup => makeAggregatedAssignment(indexed, rhs)
        case rhs: ScalarExpression   => Seq(indexed.onlyContent #:= rhs.expr)
      // increase length
      val lengthIncrease = arrayLength #:= arrayLength #+ 1
      ExpressionTranslationResult(
        Nil,
        result.sideEffects ++ elem.sideEffects ++ assignments :+ lengthIncrease,
        result.localVars ++ elem.localVars
      )
    case ma: solidity.MemberAccess if ma.memberName == "pop" =>
      val solArray = ma.expression
      // TODO: consider side-effects
      val bplLength = lengthVarsOfExpr(solArray)
      ExpressionTranslationResult(Nil, Seq(bplLength #:= bplLength #- 1), Nil)
    case solidity.MemberAccess(expr, "transfer", _) if typeOfExpression(expr) == TranslatorType.Address =>
      // TODO: check if expr is actually an address
      // TODO: add statement to assert transfer will success
      val addr = expr
      // TODO : consider side-effects
      val addrRes  = translateExpression(addr)
      val valueRes = translateExpression(funcCall.arguments.head)
      // first, decrease balance of current contract
      val decThis = BoogieBuilder.balance #:= BoogieBuilder.balance #- valueRes.expr
      // next, if addr is equal to current contract, increase balance of current contract
      val incThisConditionally = boogie.If(
        BoogieBuilder.addressThis #== addrRes.expr,
        Seq(BoogieBuilder.balance #:= BoogieBuilder.balance #+ valueRes.expr)
      )
      ExpressionTranslationResult(Nil, Seq(decThis, incThisConditionally), addrRes.localVars ++ valueRes.localVars)
    case solidity.MemberAccess(expr, "call" | "staticcall", _) if typeOfExpression(expr) == TranslatorType.Address =>
      callStubFunction(
        BoogieBuilder.addrCall,
        expr +: funcCall.arguments,
        Seq(boogie.BoogieBoolean(), boogie.Reference())
      )
    case solidity.MemberAccess(expr, memberName, refId) =>
      typeOfExpression(expr) match
        case TranslatorType.Interface(_) =>
          val returnParams = ctx
            .nodeMap(refId.get)
            .asInstanceOf[solidity.FunctionDefinition]
            .returnParameters
            .asInstanceOf[solidity.ParameterList]
            .parameters
            .map(param => typeNameToTranslatorType(param.asInstanceOf[solidity.VariableDeclaration].typeName))
          val argResults  = funcCall.arguments.map(translateExpression(_))
          val args        = argResults.flatMap(_.flatten)
          val sideEffects = argResults.flatMap(_.sideEffects)
          // assign return value to temp variables
          val retTempGroups = for returnParam <- returnParams yield
            val retTempVar = BoogieBuilder.nextTempVar()
            val result     = encodeBoogieVars(returnParam)
            AggregatedVarGroup(
              retTempVar,
              Nil,
              result.contents.map((_, Nil)),
              result.sums.map((_, Nil)),
              result.lengths.map((_, Nil))
            )
          val retTempVars = for group <- retTempGroups yield
            val contents = for (cv, indices) <- group.contents yield (cv.buildName(group.rootName), cv.boogieType)
            val sums     = for (sv, indices) <- group.sums yield (sv.buildName(group.rootName), sv.boogieType)
            val lengths  = for (lv, indices) <- group.lengths yield (lv.buildName(group.rootName), lv.boogieType)
            contents ++ sums ++ lengths
          ExpressionTranslationResult(
            retTempGroups,
            sideEffects :+ BoogieBuilder.invocationWithImplicitParams(
              Seq(
                BoogieBuilder
                  .messageAttr(funcCall.srcBegin, funcCall.srcEnd, "this invocation may violate a precondition")
              ),
              BoogieBuilder.interfaceStub(memberName, refId.get),
              args,
              retTempVars.flatten.map(_._1)
            ),
            argResults.flatMap(_.localVars) ++ retTempVars.flatten
          )
        case TranslatorType.Address if memberName == "send" =>
          val addrRes  = translateExpression(expr)
          val valueRes = translateExpression(funcCall.arguments.head)
          // this is the variable indicating whether the sending will succeed
          // first, decrease balance of current contract
          val decThis = BoogieBuilder.balance #:= BoogieBuilder.balance #- valueRes.expr
          // next, if addr is equal to current contract, increase balance of current contract
          val incThisConditionally = boogie.If(
            BoogieBuilder.addressThis #== addrRes.expr,
            Seq(BoogieBuilder.balance #:= BoogieBuilder.balance #+ valueRes.expr)
          )
          // this is send, not transfer; need a boolean function for result
          val sendResult = BoogieBuilder.nextTempVar()
          // TODO: restore balance if not succeed
          ExpressionTranslationResult(
            Seq(ScalarExpression(sendResult)),
            addrRes.sideEffects ++ valueRes.sideEffects ++ Seq(decThis, incThisConditionally),
            addrRes.localVars ++ valueRes.localVars ++ Seq((sendResult, boogie.BoogieBoolean()))
          )
        case fallback @ _ => pprint.pprintln((fallback, memberName)); ???
    case solidity.NewExpression(solidity.ElementaryTypeName("bytes")) =>
      // TODO: side effects
      ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
    case solidity.NewExpression(typeName @ solidity.UserDefinedTypeName(refId)) =>
      ctx.nodeMap(refId) match
        case _: solidity.ContractDefinition =>
          // TODO: side effects of ctor args
          ExpressionTranslationResult(Seq(ScalarExpression(BoogieBuilder.nextRef())), Nil, Nil)
        case fallback @ _ => pprint.pprintln(fallback); ???
    case solidity.NewExpression(typeName @ solidity.ArrayTypeName(elemTypeName)) =>
      val tempVar      = BoogieBuilder.nextTempVar()
      val encodeResult = encodeBoogieVars(typeNameToTranslatorType(typeName))
      val (contents, contentLocalVars) =
        encodeResult.contents.map(content => ((content, Nil), (content.buildName(tempVar), content.boogieType))).unzip
      val (sums, sumLocalVars) =
        encodeResult.sums.map(sum => ((sum, Nil), (sum.buildName(tempVar), sum.boogieType))).unzip
      val (lengths, lengthLocalVars) =
        encodeResult.lengths.map(length => ((length, Nil), (length.buildName(tempVar), length.boogieType))).unzip
      // semantics of new: length
      val lengthExpr       = funcCall.arguments.head
      val lengthTranslated = translateExpression(lengthExpr)
      val topLength        = encodeResult.lengths.find(lv => lv.arrayDepth == 0).get
      val lengthAssumption = boogie.Assume(
        Seq(":reason \"length of new array\""),
        topLength.buildExpr(tempVar, Nil) #== lengthTranslated.expr
      )
      // semantics of new: content and non-top
      val index = BoogieBuilder.quantifiedVar("idx")
      val contentNonTopAssumptions =
        for relation <- initAggregated(typeNameToTranslatorType(elemTypeName), tempVar, Nil, Seq(index), 1)
        yield boogie.Assume(
          Seq(":reason \"initial value of new array\""),
          boogie.Forall(
            Seq((index, boogie.BoogieInt())),
            ((index #>= 0) #&& (index #< lengthTranslated.expr)) #==> relation
          )
        )
      // semantics of new: sum
      val topSums = encodeResult.sums.filter(sv => sv.depth == 0)
      val sumAssumptions =
        for topSum <- topSums
        yield boogie.Assume(Seq(":reason \"top-level sum of new array\""), topSum.buildExpr(tempVar, Nil) #== 0)
      ExpressionTranslationResult(
        Seq(AggregatedVarGroup(tempVar, Nil, contents, sums, lengths)),
        lengthTranslated.sideEffects ++ contentNonTopAssumptions ++ sumAssumptions :+ lengthAssumption,
        contentLocalVars ++ sumLocalVars ++ lengthLocalVars
      )
    case _ =>
      // TODO: implement other function call cases
      pprint.pprintln(funcCall)
      throw NotImplementedError()
