package translator

import dsl.given
import dsl.*

import scala.annotation.tailrec

enum PathNode:
  case Member(member: String)
  case MapKey(keyTypeName: String)
  case ArrayIndex()

// TODO: use type directly
def valueRangeAssumptions(using
    ctx: SourceUnitContext
)(bplExpr: boogie.Expression, solExpr: solidity.ASTNode): Seq[boogie.Statement] =
  val exprType = typeOfExpression(solExpr)
  exprType match
    case TranslatorType.SignedInt(width) =>
      Seq(
        boogie.Assume(
          Seq(":reason \"int value in valid range\""),
          (bplExpr #>= BoogieBuilder.intMin(width)) #&& (bplExpr #<= BoogieBuilder.intMax(width))
        )
      )
    case TranslatorType.UnsignedInt(width) =>
      Seq(
        boogie.Assume(
          Seq(":reason \"uint value in valid range\""),
          (bplExpr #>= BoogieBuilder.uintMin(width)) #&& (bplExpr #<= BoogieBuilder.uintMax(width))
        )
      )
    case TranslatorType.UnsignedIntUnbound =>
      Seq(
        boogie.Assume(
          Seq(":reason \"unbound uint value in valid range\""),
          bplExpr #>= 0
        )
      )
    case _ => Nil

// TODO: use type directly
def checkOverflow(using
    ctx: SourceUnitContext
)(solExpr: solidity.ASTNode, bgExpr: boogie.Expression, srcBegin: Int, srcEnd: Int): Seq[boogie.Statement] =
  typeOfExpression(solExpr) match
    case TranslatorType.UnsignedInt(width) =>
      Seq(
        BoogieBuilder.assert(
          bgExpr #>= BoogieBuilder.uintMin(width),
          srcBegin,
          srcEnd,
          f"arithmetic operation may underflow (unsigned; width=$width)"
        ),
        BoogieBuilder.assert(
          bgExpr #<= BoogieBuilder.uintMax(width),
          srcBegin,
          srcEnd,
          f"arithmetic operation may overflow (unsigned; width=$width)"
        )
      )
    case TranslatorType.SignedInt(width) =>
      Seq(
        BoogieBuilder.assert(
          bgExpr #>= BoogieBuilder.intMin(width),
          srcBegin,
          srcEnd,
          f"arithmetic operation may underflow (signed; width=$width)"
        ),
        BoogieBuilder.assert(
          bgExpr #<= BoogieBuilder.intMax(width),
          srcBegin,
          srcEnd,
          f"arithmetic operation may overflow (signed; width=$width)"
        )
      )
    case TranslatorType.LiteralInt(_) => Nil
    case unexpected                   => pprint.pprintln(unexpected); ???

def checkArrayIndexOutOfBounds(using
    ctx: SourceUnitContext
)(bgIdx: boogie.Expression, bgLength: boogie.Expression, srcBegin: Int, srcEnd: Int): Seq[boogie.Statement] =
  Seq(
    BoogieBuilder.assert(bgIdx #>= 0, srcBegin, srcEnd, "index may be negative during array access"),
    BoogieBuilder.assert(bgIdx #< bgLength, srcBegin, srcEnd, "index may exceed length during array access")
  )

/** Deconstruct an expression to its variable declaration and a chain of accessing pattern.
  *
  * For example, expression `x[0].m[42]` will be destructed into `(x, [0, "m", 42])`.
  *
  * TODO: replace with deconstructAccessChain_
  */
def deconstructAccessChain(using ctx: SourceUnitContext)(
    expr: solidity.ASTNode
): (solidity.VariableDeclaration, Seq[String | solidity.ASTNode]) =
  expr match
    case solidity.Identifier(refId, _) => (ctx.nodeMap(refId).asInstanceOf[solidity.VariableDeclaration], Nil)
    case solidity.IndexAccess(base, index) =>
      val (varDecl, prev) = deconstructAccessChain(base)
      (varDecl, prev :+ index)
    case solidity.MemberAccess(expression, memberName, _) =>
      val (varDecl, prev) = deconstructAccessChain(expression)
      (varDecl, prev :+ memberName)
    case _ => throw new NotImplementedError(f"TODO: ${expr.getClass}")

def deconstructAccessChain_(using ctx: SourceUnitContext)(
    expr: solidity.ASTNode
): (solidity.VariableDeclaration, Seq[String], Seq[solidity.ASTNode]) =
  expr match
    case solidity.Identifier(refId, _) => (ctx.nodeMap(refId).asInstanceOf[solidity.VariableDeclaration], Nil, Nil)
    case solidity.IndexAccess(base, index) =>
      val (varDecl, members, indices) = deconstructAccessChain_(base)
      (varDecl, members, indices :+ index)
    case solidity.MemberAccess(expression, memberName, _) =>
      val (varDecl, members, indices) = deconstructAccessChain_(expression)
      (varDecl, members :+ memberName, indices)
    case _ => throw new NotImplementedError(f"TODO: ${expr.getClass}")

def typeOfAccessChain(using
    ctx: SourceUnitContext
)(typeName: solidity.ASTNode, accessChain: Seq[String | solidity.ASTNode]): solidity.ASTNode =
  accessChain.foldLeft[solidity.ASTNode](typeName) {
    case (solidity.ArrayTypeName(bt), _: solidity.ASTNode) => bt
    case (solidity.Mapping(_, vt), _: solidity.ASTNode)    => vt
    case (solidity.UserDefinedTypeName(refId), member: String) =>
      val members = ctx
        .nodeMap(refId)
        .asInstanceOf[solidity.StructDefinition]
        .members
        .map(_.asInstanceOf[solidity.VariableDeclaration])
      val varDecl = members.find(_.name == member).get
      varDecl.typeName
    case _ => throw new NotImplementedError(f"$typeName, $accessChain")
  }

/** Get the length expression of current expr.
  *
  * Type of the returning expression is always an integer.
  */
def lengthVarsOfExpr(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    expr: solidity.ASTNode
): boogie.Expression =
  val (varDecl, chain) = deconstructAccessChain(expr)
  // TODO: consider side-effects
  val bplIndices = chain.collect { case n: solidity.ASTNode => n }.map(translateExpression).map(_.expr)
  @tailrec
  def getArrayDepth(solType: solidity.ASTNode, accDepth: Int, chain: Seq[String | solidity.ASTNode]): Int =
    (solType, chain) match
      case (_, Nil)                                => accDepth
      case (solidity.Mapping(_, vt), _ :: tail)    => getArrayDepth(vt, accDepth, tail)
      case (solidity.ArrayTypeName(bt), _ :: tail) => getArrayDepth(bt, accDepth + 1, tail)
      case (solidity.UserDefinedTypeName(refId), member :: tail) =>
        val members = ctx
          .nodeMap(refId)
          .asInstanceOf[solidity.StructDefinition]
          .members
          .asInstanceOf[Seq[solidity.VariableDeclaration]]
        val selected = members.find(decl => decl.name == member).get
        getArrayDepth(selected.typeName, accDepth, tail)
      case _ => throw NotImplementedError("unexpected case")
  val arrayDepth = getArrayDepth(varDecl.typeName, 0, chain)
  val members    = chain.collect { case s: String => s }
  bplIndices.foldLeft[boogie.Expression](
    BoogieBuilder.nameOfLengthVar(
      (BoogieBuilder.identWithId(varDecl.name, varDecl.id.get) +: members).mkString(BoogieBuilder.memberSep),
      arrayDepth
    )
  )(_(_))

def initializeExpr(using ctx: SourceUnitContext)(
    expr: solidity.ASTNode
): Seq[boogie.Expression] =
  given TranslationOptions = TranslationOptions.featureless
  val (varDecl, chain)     = deconstructAccessChain(expr)
  typeOfAccessChain(varDecl.typeName, chain) match
    case solidity.ElementaryTypeName(name) if name.matches("""u?int(\d*)""") =>
      val lhs = translateExpression(expr).expr
      Seq(lhs #== 0)
    case solidity.ElementaryTypeName("address") =>
      val lhs = translateExpression(expr).expr
      Seq(lhs #== 0)
    case solidity.ElementaryTypeName("bool") =>
      val lhs = translateExpression(expr).expr
      Seq(lhs #== false)
    case solidity.ElementaryTypeName(name) if name.matches("""bytes(\d*)""") =>
      Nil
    case solidity.ElementaryTypeName("string") =>
      Nil
    case solidity.ArrayTypeName(_) =>
      val length = lengthVarsOfExpr(expr)
      Seq(length #== 0)
    case solidity.Mapping(_, _) =>
      Nil
    case solidity.UserDefinedTypeName(refId) =>
      for
        member <- ctx.nodeMap(refId).asInstanceOf[solidity.StructDefinition].members
        access = solidity.MemberAccess(expr, member.asInstanceOf[solidity.VariableDeclaration].name, None)
        result <- initializeExpr(access)
      yield result
    case _v => throw new NotImplementedError(_v.toString)

def initializeDecl(using ctx: SourceUnitContext)(decl: solidity.VariableDeclaration): Seq[boogie.Statement] =
  flattenStruct(decl.typeName) flatMap { result =>
    initializeDeclFlattened(BoogieBuilder.identWithId(decl.name, decl.id.get), result)
  }

enum Signedness:
  case Signed
  case Unsigned
  case Neither
case class FlattenResult(nodes: Seq[PathNode], typeName: solidity.ASTNode, signedness: Signedness)

def flattenStruct(using ctx: SourceUnitContext)(typeName: solidity.ASTNode): Seq[FlattenResult] =
  typeName match
    case solidity.ElementaryTypeName("int")     => Seq(FlattenResult(Nil, typeName, Signedness.Signed))
    case solidity.ElementaryTypeName("int256")  => Seq(FlattenResult(Nil, typeName, Signedness.Signed))
    case solidity.ElementaryTypeName("uint")    => Seq(FlattenResult(Nil, typeName, Signedness.Unsigned))
    case solidity.ElementaryTypeName("uint256") => Seq(FlattenResult(Nil, typeName, Signedness.Unsigned))
    case solidity.ElementaryTypeName(_)         => Seq(FlattenResult(Nil, typeName, Signedness.Neither))
    case solidity.ArrayTypeName(bt) =>
      for result <- flattenStruct(bt)
      yield FlattenResult(PathNode.ArrayIndex() +: result.nodes, result.typeName, result.signedness)
    case solidity.Mapping(solidity.ElementaryTypeName(ktName), vt) =>
      for result <- flattenStruct(vt)
      yield FlattenResult(PathNode.MapKey(ktName) +: result.nodes, result.typeName, result.signedness)
    case solidity.UserDefinedTypeName(refId) =>
      ctx.nodeMap(refId) match
        case solidity.EnumDefinition(_, _) =>
          Seq(FlattenResult(Nil, solidity.ElementaryTypeName("uint"), Signedness.Unsigned))
        case structDef: solidity.StructDefinition =>
          for
            member <- structDef.members.map(_.asInstanceOf[solidity.VariableDeclaration])
            result <- flattenStruct(member.typeName)
          yield FlattenResult(PathNode.Member(member.name) +: result.nodes, result.typeName, result.signedness)
        case contractDef: solidity.ContractDefinition =>
          Seq(FlattenResult(Nil, solidity.ElementaryTypeName("address"), Signedness.Neither))
        case unexpected => pprint.pprintln(unexpected); ???
    case _ =>
      throw new NotImplementedError(typeName.toString)

/** To initialize data, if `typeName` of `decl` is only composed of `ElementaryTypeName`, `Mapping` and `ArrayTypeName`,
  * only one statement will be generated;
  * in this case, `typeName` can always be splitted into two parts:
  * `mapping(E => mapping(E => ... => mapping(E => N)...))` where `E` is `ElementaryTypeName`, and `N` is not `Mapping`.
  *
  * - if `N` is `ElementaryTypeName`, the statement will be:
  *   `assume forall i1:E, i2:E, ..., in:E :: v[i1][i2]...[in] == 0`
  * - if `N` is `ArrayTypeName`, the statement will be:
  *   `assume forall i1:E, i2:E, ..., in:E :: length_of_v[i1][i2]...[in] == 0`
  *
  * To initialize sum, just fill evey possible value to zero.
  *
  * If `typeName` contains any structure, it will be flattened before this function is called.
  */
def initializeDeclFlattened(using
    ctx: SourceUnitContext
)(baseName: String, flattenResult: FlattenResult): Seq[boogie.Statement] =
  // initialize data (if not an array) and length (if is an array)
  val prefix = flattenResult.nodes.takeWhile {
    case _: PathNode.Member     => true
    case _: PathNode.MapKey     => true
    case _: PathNode.ArrayIndex => false
  }
  val boogieName     = (baseName +: prefix.collect { case PathNode.Member(m) => m }).mkString(BoogieBuilder.memberSep)
  val quantifierSize = prefix.collect { case _: PathNode.MapKey => () }.size
  val quantifiers    = (1 to quantifierSize).map(i => BoogieBuilder.quantifiedVar(i.toString))
  val dataAndLengthAssumption = if prefix.length == flattenResult.nodes.size then
    // no array on this path, initialize data variable
    val lhs = quantifiers.foldLeft[boogie.Expression](boogieName)(_(_))
    flattenResult.typeName match
      case solidity.ElementaryTypeName("address")                             => (quantifiers, Some(lhs #== 0))
      case solidity.ElementaryTypeName(name) if name.matches("""int(\d*)""")  => (quantifiers, Some(lhs #== 0))
      case solidity.ElementaryTypeName(name) if name.matches("""uint(\d*)""") => (quantifiers, Some(lhs #== 0))
      case solidity.ElementaryTypeName("bool")                                => (quantifiers, Some(lhs #== false))
      case solidity.ElementaryTypeName("string")                              => (quantifiers, None)
      case solidity.ElementaryTypeName(name) if name.matches("""bytes(\d*)""") =>
        (quantifiers, Some(lhs #== BoogieBuilder.nextRef()))
      case _ => throw NotImplementedError(flattenResult.typeName.toString)
  else
    // at least one array found on this path, initialize length variable instead
    val arrayLength = quantifiers.foldLeft[boogie.Expression](BoogieBuilder.nameOfLengthVar(boogieName, 0))(_(_))
    (quantifiers, Some(arrayLength #== 0))
  // initialize sum variables, simply let everything that is signed/unsigned equal to zero
  val sumAssumptions = flattenResult.signedness match
    case Signedness.Signed | Signedness.Unsigned =>
      val (members, slots) = flattenResult.nodes.foldLeft[(Seq[String], Int)]((Nil, 0)) {
        case ((members, slots), PathNode.Member(m))                          => (members :+ m, slots)
        case ((members, slots), _: PathNode.MapKey | _: PathNode.ArrayIndex) => (members, slots + 1)
      }
      val fullName        = (baseName +: members).mkString(BoogieBuilder.memberSep)
      val fullQuantifiers = (1 to slots).map(i => BoogieBuilder.quantifiedVar(i.toString))
      for init <- fullQuantifiers.inits.toSeq.tail.reverse
      yield (init, Some(init.foldLeft[boogie.Expression](BoogieBuilder.nameOfSumVar(fullName, init.size))(_(_)) #== 0))
    case _ => Nil
  (dataAndLengthAssumption +: sumAssumptions) flatMap {
    case (IndexedSeq(), Some(relation)) => Seq(boogie.Assume(Seq(":reason \"initial value\""), relation))
    case (quantifiers, Some(relation)) =>
      Seq(
        boogie.Assume(
          Seq(":reason \"initial value\""),
          boogie.Forall(quantifiers.map(q => (q, boogie.BoogieInt())), relation)
        )
      )
    case (_, None) => Nil
  }

def callStubFunction(using translationOptions: TranslationOptions, sourceUnitContext: SourceUnitContext)(
    boogieProcName: String,
    solidityParams: Seq[solidity.ASTNode],
    boogieReturnTypes: Seq[boogie.Type]
): ExpressionTranslationResult =
  val argResults  = solidityParams.map(translateExpression)
  val retTempVars = boogieReturnTypes.map(ty => (BoogieBuilder.nextTempVar(), ty))
  ExpressionTranslationResult(
    // TODO: neither?
    retTempVars.map { case (name, ty) =>
      AggregatedVarGroup(name, Nil, Seq((ContentVar(Nil, Nil, ty, Signedness.Neither), Nil)), Nil, Nil)
    },
    argResults.flatMap(_.sideEffects),
    retTempVars ++ argResults.flatMap(_.localVars)
  )
