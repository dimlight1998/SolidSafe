package translator

import translator.dsl.*
import translator.dsl.given

import scala.util.Failure
import scala.util.Success
import scala.util.Try

case class TranslationOptions(
    checkArithmeticOverflow: Boolean,
    checkArrayIndexOutOfBounds: Boolean,
    replaceFuncArgsToOrigin: Boolean
)
object TranslationOptions:
  def featureless = TranslationOptions(false, false, false)

case class SourceUnitContext(
    astRoot: solidity.ASTNode,
    nodeMap: Map[Int, solidity.ASTNode],
    rawText: Option[String]
)

case class FunctionContext(
    currentProcReturnParams: Seq[(String, boogie.Type)]
)

case class SpecificationContext(
    forallQuantified: Map[String, solidity.ASTNode]
)

case class FunctionTranslationResult(visibility: String, kind: String, proc: boogie.Procedure)

case class VariableTranslationResult(
    boogieDecls: Seq[(String, boogie.Type, VariableTranslationResult.BoogieVariableKind)]
)
object VariableTranslationResult:
  enum BoogieVariableKind:
    case SumOfUnsigned(baseDecl: (String, boogie.Type), depth: Int)
    case SumOfSigned(baseDecl: (String, boogie.Type), depth: Int)
    case ArrayLength(arrayDepth: Int, totalDepth: Int)
    case PlainStateVariable()

/** This is the top-level translator.
  *
  * The last contract in the file, and all contracts inherited by it, will be translated.
  * We require all contracts definition be inherited by the last contract.
  */
def translateSourceUnit(using translationOptions: TranslationOptions)(
    sourceUnit: solidity.SourceUnit,
    rawText: String
): Seq[boogie.TopLevel] =
  // preparations
  val nodeMap             = Map.from(sourceUnit.collect { case node if node.id.nonEmpty => node.id.get -> node })
  val sourceUnitCtx       = SourceUnitContext(sourceUnit, nodeMap, Some(rawText))
  given SourceUnitContext = sourceUnitCtx
  // pass: translate interface functions
  val interfaceFunctions = translateInterfaceFunctionDefinitions(sourceUnit)
  // pass: translate enum definitions
  val enumDefinitions = translateEnumDefinitions(sourceUnit)
  // compute contracts that are directly or indirectly inherited by the last contract in the file;
  // only these contracts will be translated
  val allContractIds = sourceUnit.collect {
    case contractDef: solidity.ContractDefinition if contractDef.contractKind == "contract" =>
      contractDef.id.get
  }
  def getDirectlyInherited(id: Int): Set[Int] =
    val contractDef = sourceUnitCtx.nodeMap(id).asInstanceOf[solidity.ContractDefinition]
    contractDef.baseContracts
      .map(spec =>
        spec
          .asInstanceOf[solidity.InheritanceSpecifier]
          .baseName
          .asInstanceOf[solidity.IdentifierPath]
          .referencedDeclaration
      )
      .toSet
  def getIndirectlyInherited(id: Int): Set[Int] =
    val directly = getDirectlyInherited(id)
    if directly.isEmpty then Set(id) else Set(id) ++ directly.flatMap(getIndirectlyInherited)
  val contractInheritedByLast = getIndirectlyInherited(allContractIds.last)
  val effectiveContracts =
    contractInheritedByLast.toSeq.sorted.map(id => nodeMap(id).asInstanceOf[solidity.ContractDefinition])
  // pass: translate global variables
  val effectiveStateVars = for
    contractDef <- effectiveContracts
    varDecl     <- contractDef.subnodes[solidity.VariableDeclaration] if varDecl.stateVariable
  yield varDecl
  val globalVars     = effectiveStateVars.flatMap(sv => translateVariableDeclaration(sv).boogieDecls)
  val globalVarsInit = effectiveStateVars.flatMap(gv => initializeDecl(gv))
  // pass: remember contract invariants
  val contractInvs = for
    contractDef <- effectiveContracts
    docLine <- contractDef.documentation match
      case None => Nil
      case Some(doc) =>
        val sdoc = doc.asInstanceOf[solidity.StructuredDocumentation]
        utils.computeLocForStructuredDocument(sdoc.text, sdoc.srcBegin, sdoc.srcEnd - 1)
    forallExpr <- parseSpecString(contractDef, docLine(0), Some(docLine(1))) match
      case Success((natspec.SpecificationType.Invariant, inv)) => Some((inv, docLine(1), docLine(2)))
      case Success(_)                                          => ???
      case Failure(exception)                                  => throw exception
  yield forallExpr
  // pass: translate functions and record public ones
  // some functions are overridden; ids of these functions will be stored in a blacklist
  val overriddenFuncIds = for
    funcDef    <- sourceUnit.subnodes[solidity.FunctionDefinition]
    baseFuncId <- funcDef.baseFunctions.toSeq.flatten
  yield baseFuncId
  val finalFuncIds =
    sourceUnit.subnodes[solidity.FunctionDefinition].map(funcDef => funcDef.id.get).toSet.removedAll(overriddenFuncIds)
  val finalMap = (for
    id     <- finalFuncIds
    baseId <- nodeMap(id).asInstanceOf[solidity.FunctionDefinition].baseFunctions.toSeq.flatten
  yield baseId -> id).toMap
  val procedures = effectiveContracts.flatMap { contractDef =>
    contractDef.collect {
      case funcDef: solidity.FunctionDefinition if finalMap contains funcDef.id.get =>
        val implFunc = nodeMap(finalMap(funcDef.id.get)).asInstanceOf[solidity.FunctionDefinition]
        translateFunctionDefinition(contractDef, implFunc, Some(funcDef.id.get))
      case funcDef: solidity.FunctionDefinition =>
        translateFunctionDefinition(contractDef, funcDef, Some(funcDef.id.get))
    }
  }
  val placeholderConstructors = effectiveContracts.flatMap { contractDef =>
    val ctors = contractDef.collect {
      case funcDef: solidity.FunctionDefinition if funcDef.kind == "constructor" => ()
    }
    if ctors.isEmpty then
      Seq(
        FunctionTranslationResult(
          "constructor",
          "public",
          boogie.Procedure(
            BoogieBuilder.constructorOfContract(contractDef.name),
            Seq(":inline 1"),
            BoogieBuilder.implicitParams,
            Nil,
            Nil,
            Nil,
            Nil,
            Nil
          )
        )
      )
    else Nil
  }
  // pass: build the contract initializer
  val initializerProc =
    boogie.Procedure(BoogieBuilder.initializer, Seq(":inline 1"), Nil, Nil, Nil, Nil, Nil, globalVarsInit)
  // pass: build the life-cycle procedure
  val publicNonConstructors =
    procedures
      .filter { case FunctionTranslationResult(visibility, kind, _) =>
        (Set("public", "external") contains visibility) && kind != "constructor"
      }
      .map(_._3)
  val lastConstructor = effectiveContracts.last.collect {
    case funcDef: solidity.FunctionDefinition if funcDef.kind == "constructor" =>
      translateFunctionDefinition(effectiveContracts.last, funcDef, None)
  } match
    case head :: _ => head
    case Nil =>
      FunctionTranslationResult(
        "public",
        "constructor",
        boogie.Procedure(
          BoogieBuilder.constructorOfContract(effectiveContracts.last.name),
          Seq(":inline 1"),
          BoogieBuilder.implicitParams,
          Nil,
          Nil,
          Nil,
          Nil,
          Nil
        )
      )
  val branches = publicNonConstructors.map { proc =>
    val explicitParams = BoogieBuilder.explicitParamsWithPrefix(proc.name, proc.params).map(_._1)
    (
      boogie.NonDetermined(),
      explicitParams.map({ name => boogie.Havoc(boogie.Identifier(name)) })
        :+ BoogieBuilder.invocationWithImplicitParams(
          Nil,
          proc.name,
          explicitParams.map(boogie.Identifier.apply),
          BoogieBuilder
            .normalizedReturnParamsWithPrefix(proc.name, proc.returnParams)
            .map { case (name, _) => boogie.Identifier(name) }
        )
    )
  }
  // this is a collection of invariants which states that lengths of arrays should be a valid uint256
  // these invariants are free invariants since practically array won't be that large
  val lengthOfArrayInvariants = globalVars.collect {
    case (name, _, VariableTranslationResult.BoogieVariableKind.ArrayLength(_, 0)) =>
      (name #>= BoogieBuilder.uintMin(256)) #&& (name #<= BoogieBuilder.uintMax(256))
    case (name, _, VariableTranslationResult.BoogieVariableKind.ArrayLength(_, totalDepth)) =>
      val indices     = (1 to totalDepth) map (i => BoogieBuilder.quantifiedVar(i.toString))
      val withIndices = indices.foldLeft[boogie.Expression](name)(_(_))
      // TODO: assuming all indices are int
      val qvars = indices map ((_, boogie.BoogieInt()))
      boogie.Forall(
        qvars,
        (withIndices #>= BoogieBuilder.uintMin(256)) #&& (withIndices #<= BoogieBuilder.uintMax(256))
      )
  }
  // this is a collection of invariants which states that the sum of unsigned state variables should be non-negative
  // note that sum of uint256 can exceed the max value of uint256
  // these invariants are free invariants since this property needs the external semantics of "sum"
  val sumOfUnsignedNonNegInvariants = globalVars.collect {
    case (name, _, VariableTranslationResult.BoogieVariableKind.SumOfUnsigned(_, 0)) =>
      name #>= 0
    case (name, _, VariableTranslationResult.BoogieVariableKind.SumOfUnsigned(_, depth)) =>
      val indices     = (1 to depth) map (i => BoogieBuilder.quantifiedVar(i.toString))
      val withIndices = indices.foldLeft[boogie.Expression](name)(_(_))
      // TODO: assuming all indices are int
      val qvars = indices map ((_, boogie.BoogieInt()))
      boogie.Forall(qvars, withIndices #>= 0)
  }
  // this is a collection of invariants which states that the sum of unsigned state variables,
  // should be greater or equal to each of its components
  // these invariants are free invariants since this property needs the external semantics of "sum"
  val sumOfUnsignedGeComponentInvariants = globalVars.collect {
    case (declName, _, VariableTranslationResult.BoogieVariableKind.SumOfUnsigned((baseName, baseType), depth)) =>
      def getDepth(typeName: boogie.Type): Int = typeName match
        case boogie.Mapping(_, vt) => 1 + getDepth(vt)
        case _                     => 0
      val baseDepth       = getDepth(baseType)
      val indices         = (1 to baseDepth) map (i => BoogieBuilder.quantifiedVar(i.toString))
      val baseWithIndices = indices.foldLeft[boogie.Expression](baseName)(_(_))
      val sumWithIndices  = indices.take(depth).foldLeft[boogie.Expression](declName)(_(_))
      // TODO: assuming all indices are int
      val qvars = indices map ((_, boogie.BoogieInt()))
      boogie.Forall(qvars, sumWithIndices #>= baseWithIndices)
  }
  val lifeCycleStmtsPart1 = Seq(
    boogie.Havoc(BoogieBuilder.msgSender),
    boogie.Havoc(BoogieBuilder.msgValue),
    boogie.Havoc(BoogieBuilder.msgData),
    boogie.Assume(Seq(":reason \"contract is not destructed on initialized\""), BoogieBuilder.destructed #== false),
    boogie.Assume(Seq(":reason \"msg.sender should be valid\""), BoogieBuilder.msgSender #!= 0),
    boogie
      .Assume(
        Seq(":reason \"msg.value should be a valid uint\""),
        BoogieBuilder.msgValue #>= BoogieBuilder.uintMin(256)
      ),
    boogie
      .Assume(
        Seq(":reason \"msg.value should be a valid uint\""),
        BoogieBuilder.msgValue #<= BoogieBuilder.uintMax(256)
      ),
    boogie.Call(Nil, BoogieBuilder.initializer, Nil, Nil)
  )
  val lifeCycleStmtsPart2 =
    val proc = lastConstructor.proc
    BoogieBuilder.invocationWithImplicitParams(
      Nil,
      proc.name,
      BoogieBuilder
        .explicitParamsWithPrefix(proc.name, proc.params)
        .map((name, _) => boogie.Identifier(name)),
      Nil
    )
  val lifeCycleStmtsPart3 = Seq(
    boogie.Location(
      "lifecycle",
      Seq(
        boogie.While(
          boogie.UnaryOperation("!", BoogieBuilder.destructed),
          // TODO: side effects for annotation
          contractInvs.map { case (inv, start, stop) =>
            (Seq(BoogieBuilder.messageAttr(start, stop, "contract_inv")), translateForallExpression(inv, false))
          },
          lengthOfArrayInvariants ++ sumOfUnsignedNonNegInvariants ++ sumOfUnsignedGeComponentInvariants,
          Seq(
            boogie.Havoc(BoogieBuilder.msgSender),
            boogie.Havoc(BoogieBuilder.msgValue),
            boogie.Havoc(BoogieBuilder.msgData),
            boogie.Assume(Seq(":reason \"msg.sender should be valid\""), BoogieBuilder.msgSender #!= 0),
            boogie.Assume(
              Seq(":reason \"msg.value should be a valid uint\""),
              BoogieBuilder.msgValue #>= BoogieBuilder.uintMin(256)
            ),
            boogie.Assume(
              Seq(":reason \"msg.value should be a valid uint\""),
              BoogieBuilder.msgValue #<= BoogieBuilder.uintMax(256)
            ),
            boogie.Assign(BoogieBuilder.blockTimestampLast, BoogieBuilder.blockTimestamp),
            boogie.Havoc(BoogieBuilder.blockTimestamp),
            boogie.Assume(
              Seq(":reason \"block.timestamp is monotonic\""),
              BoogieBuilder.blockTimestamp #>= BoogieBuilder.blockTimestampLast
            ),
            boogie.Condition(branches)
          )
        )
      )
    )
  )
  val lifeCycleStmts = lifeCycleStmtsPart1 ++ (lifeCycleStmtsPart2 +: lifeCycleStmtsPart3)
  val explicitParams = procedures flatMap { functionTranslationResult =>
    val proc = functionTranslationResult.proc
    BoogieBuilder.explicitParamsWithPrefix(proc.name, proc.params)
  }
  val normalizedReturnParams = publicNonConstructors.flatMap { proc =>
    BoogieBuilder.normalizedReturnParamsWithPrefix(proc.name, proc.returnParams)
  }
  val lifeCycleProc =
    boogie.Procedure(
      BoogieBuilder.lifeCycle,
      Nil,
      Nil,
      Nil,
      Nil,
      Nil,
      (BoogieBuilder.msgSender, boogie.Address())
        +: (BoogieBuilder.msgValue, boogie.BoogieInt())
        +: (BoogieBuilder.msgData, boogie.Reference())
        +: (explicitParams ++ normalizedReturnParams),
      lifeCycleStmts
    )

  val uintMin    = 0
  val uintMax    = BigInt(2).pow(256) - 1
  val uint128Min = 0
  val uint128Max = BigInt(2).pow(128) - 1
  val intMin     = -BigInt(2).pow(255)
  val intMax     = BigInt(2).pow(255) - 1
  val rangeLines =
    for width <- 8 to 256 by 8
    yield Seq(
      f"const ${BoogieBuilder.intMin(width)}: int;",
      f"axiom ${BoogieBuilder.intMin(width)} == ${-BigInt(2).pow(width - 1)};",
      f"const ${BoogieBuilder.intMax(width)}: int;",
      f"axiom ${BoogieBuilder.intMax(width)} == ${BigInt(2).pow(width - 1) - 1};",
      f"const ${BoogieBuilder.uintMin(width)}: int;",
      f"axiom ${BoogieBuilder.uintMin(width)} == 0;",
      f"const ${BoogieBuilder.uintMax(width)}: int;",
      f"axiom ${BoogieBuilder.uintMax(width)} == ${BigInt(2).pow(width) - 1};"
    )
  val preludeLines = (rangeLines.flatten ++ Seq(
    "type address = int;",
    "type reference = int;",
    "var #balance: int;",
    "var #this: int;",
    "var #tx.origin: address;",
    "var #tx.gasprice: int;",
    "var #block.timestamp: int;",
    "var #block.timestamp.last: int;",
    "var #block.number: int;",
    "var #block.coinbase: address;",
    "var #block.difficulty: int;",
    "var #block.gaslimit: int;",
    "var #destructed: bool;",
    "procedure boogie_si_record_0(#: int);",
    "procedure boogie_si_record_1(#: bool);",
    f"procedure {:inline 1} ${BoogieBuilder.external}() {}",
    f"procedure {:inline 1} ${BoogieBuilder.addrCall}(addr: address, calldata: reference) returns (success: bool, retdata: reference) { call ${BoogieBuilder.external}(); havoc success; havoc retdata; }",
    f"procedure {:inline 1} ${BoogieBuilder.blockhash}(blocknumber: int) returns (hash: reference) { havoc hash; }"
  )).map(text => boogie.PreludeText(text))
  val globalVarDecls = globalVars.map((name, ty, _) => boogie.GlobalVariableDecl(name, ty))
  val topLevelProcs =
    interfaceFunctions ++ (initializerProc +: (placeholderConstructors ++ procedures).map(_.proc) :+ lifeCycleProc)
  preludeLines ++ enumDefinitions ++ globalVarDecls ++ topLevelProcs

def translateInterfaceFunctionDefinitions(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    sourceUnit: solidity.SourceUnit
): Seq[boogie.Procedure] =
  for
    contractDef <- sourceUnit.subnodes[solidity.ContractDefinition]
    functionDef <- contractDef.subnodes[solidity.FunctionDefinition]
  yield
    val rawParams = functionDef.parameters
      .asInstanceOf[solidity.ParameterList]
      .parameters
      .map(p => translateVariableDeclaration(p.asInstanceOf[solidity.VariableDeclaration]))
      .flatMap(res => res.boogieDecls)
      .map((name, ty, _) => (name, ty))
    val rawReturnParams = functionDef.returnParameters
      .asInstanceOf[solidity.ParameterList]
      .parameters
      .map(p => translateVariableDeclaration(p.asInstanceOf[solidity.VariableDeclaration]))
      .flatMap(res => res.boogieDecls)
      .map((name, ty, _) => (name, ty))
    val params       = BoogieBuilder.implicitParams ++ BoogieBuilder.normalizedParams(rawParams)
    val returnParams = BoogieBuilder.normalizedReturnParams(rawReturnParams)
    boogie.Procedure(
      BoogieBuilder.interfaceStub(functionDef.name, functionDef.id.get),
      Seq(":inline 1"),
      params,
      returnParams,
      Nil,
      Nil,
      Nil,
      boogie.Call(Nil, BoogieBuilder.external, Nil, Nil) +: returnParams.map((rpName, _) => boogie.Havoc(rpName))
    )

def translateEnumDefinitions(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    sourceUnit: solidity.SourceUnit
): Seq[boogie.TopLevel] =
  for
    enumDefinition <- sourceUnit.subnodes[solidity.EnumDefinition]
    (member, idx)  <- enumDefinition.members.zipWithIndex
    name = BoogieBuilder.identWithId(enumDefinition.name, enumDefinition.id.get) + BoogieBuilder.memberSep + member
      .asInstanceOf[solidity.EnumValue]
      .name
    constDecl = boogie.GlobalConstantDecl(name, boogie.BoogieInt())
    axiom     = boogie.Axiom(name #== idx)
    topLevel <- Seq(constDecl, axiom)
  yield topLevel

// funcId can disagree with funcDef.id due to overridding;
// None means this is a constructor
def translateFunctionDefinition(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    contractDef: solidity.ContractDefinition,
    funcDef: solidity.FunctionDefinition,
    funcId: Option[Int]
): FunctionTranslationResult =
  val (funcName, usedFuncId) = (funcDef.kind, funcDef.name) match
    case ("constructor", _) => (BoogieBuilder.constructorOfContract(contractDef.name), None)
    case (_, "")            => (BoogieBuilder.fallback, None)
    case (_, _)             => (funcDef.name, funcId)
  val rawParams = funcDef.parameters
    .asInstanceOf[solidity.ParameterList]
    .parameters
    .map(p => translateVariableDeclaration(p.asInstanceOf[solidity.VariableDeclaration]))
    .flatMap(res => res.boogieDecls)
    .map((name, ty, _) => (name, ty))
  // show int and bool when using corral
  val printStmts = rawParams.flatMap {
    case (name, boogie.BoogieInt()) =>
      Seq(boogie.Call(Seq(":cexpr \"" + name + "\""), "boogie_si_record_0", Seq(boogie.Identifier(name)), Nil))
    case (name, boogie.BoogieBoolean()) =>
      Seq(boogie.Call(Seq(":cexpr \"" + name + "\""), "boogie_si_record_1", Seq(boogie.Identifier(name)), Nil))
    case _ => Nil
  }
  // boogie disallowing modifying arguments; make a copy of them
  val originalParams = rawParams.map { case (name, ty) => (BoogieBuilder.originalParam(name), ty) }
  val paramCopyStmts =
    originalParams.zip(rawParams).map { case ((original, _), (modifiable, _)) => modifiable #:= original }
  val rawReturnParams = funcDef.returnParameters
    .asInstanceOf[solidity.ParameterList]
    .parameters
    .map(p => translateVariableDeclaration(p.asInstanceOf[solidity.VariableDeclaration]))
    .flatMap(res => res.boogieDecls)
    .map((name, ty, _) => (name, ty))
  val params       = BoogieBuilder.implicitParams ++ BoogieBuilder.normalizedParams(originalParams)
  val returnParams = BoogieBuilder.normalizedReturnParams(rawReturnParams)
  val funcCtx      = FunctionContext(returnParams)
  // translate the body itself; use a StatementTranslationResult for all results
  // TODO: rename StatementTranslationResult to a sensible name
  val bodyResult = funcDef.body match
    case None => StatementTranslationResult(Nil, Nil)
    case Some(body) =>
      val results = body
        .asInstanceOf[solidity.Block]
        .statements
        .map(translateStatement(using translationOptions, ctx, funcCtx, None))
      StatementTranslationResult(results.flatMap(_._1), results.flatMap(_._2))
  // process modifiers:
  // - if this is a normal function, these modifiers are actually modifiers
  // - if this ia a constructor, these modifiers are constructor invocations
  //   constructor invocations order is the reverse of linearizedBaseContracts
  val modifierProcessed = funcDef.kind match
    case "constructor" =>
      // constructor: generate invocations to base constructor
      val invocationOrder = contractDef.linearizedBaseContracts.reverse.init
      val explicitBaseCtorCallArgs =
        funcDef.modifiers
          .map(invocation =>
            invocation
              .asInstanceOf[solidity.ModifierInvocation]
              .modifierName
              .asInstanceOf[solidity.IdentifierPath]
              .name -> invocation.asInstanceOf[solidity.ModifierInvocation].arguments.get
          )
          .toMap
      val invocations = for refId <- invocationOrder yield
        val baseContractDef = ctx.nodeMap(refId).asInstanceOf[solidity.ContractDefinition]
        val args            = explicitBaseCtorCallArgs.getOrElse(baseContractDef.name, Nil)
        val translatedArgs  = args.map(arg => translateExpression(arg))
        val sideEffects     = translatedArgs.flatMap(_.sideEffects)
        val baseCtorCall =
          BoogieBuilder.invocationWithImplicitParams(
            Nil,
            BoogieBuilder.constructorOfContract(baseContractDef.name),
            translatedArgs.map(_.expr),
            Nil
          )
        sideEffects :+ baseCtorCall
      StatementTranslationResult(invocations.flatten ++ bodyResult.statements, bodyResult.localVars)
    case _ =>
      // normal function: replace placeholders in modifiers
      funcDef.modifiers.foldLeft[StatementTranslationResult](bodyResult)((acc, modifier) =>
        val refId = modifier
          .asInstanceOf[solidity.ModifierInvocation]
          .modifierName
          .asInstanceOf[solidity.IdentifierPath]
          .referencedDeclaration
        val definition = ctx.nodeMap(refId).asInstanceOf[solidity.ModifierDefinition]
        translateStatement(using translationOptions, ctx, funcCtx, Some(acc))(definition.body)
      )
  // if this function is payable, add msg.value to balance
  // otherwise, require msg.value equal to zero
  val stateMutStmt = funcDef.visibility match
    case "public" | "external" =>
      funcDef.stateMutability match
        case "payable"    => Seq(BoogieBuilder.balance #:= BoogieBuilder.balance #+ BoogieBuilder.msgValue)
        case "nonpayable" => Seq(boogie.Assume(Seq(":reason \"function is nonpayable\""), BoogieBuilder.msgValue #== 0))
        case "view"       => Nil
        case "pure"       => Nil
        case _s           => pprint.pprintln(_s); ???
    case _ => Nil
  // preconditions and postconditions
  val (requires, ensures) = funcDef.documentation match
    case Some(doc @ solidity.StructuredDocumentation(text)) =>
      val docLines = utils.computeLocForStructuredDocument(text, doc.srcBegin, doc.srcEnd)
      val parsed   = docLines.map(docLine => parseSpecString(funcDef, docLine(0), Some(docLine(1))))
      parsed.collect { case Failure(e: utils.NatspecSpecException) => throw e }
      val withStartStop = parsed.zip(docLines)
      (
        withStartStop.collect { case (Success((natspec.SpecificationType.Precondition, expr)), (_, start, stop)) =>
          (
            Seq(BoogieBuilder.messageAttr(start, stop, "this precondition is violated by some invocation")),
            translateForallExpression(expr, true)
          )
        },
        withStartStop.collect { case (Success((natspec.SpecificationType.Postcondition, expr)), (_, start, stop)) =>
          (
            Seq(BoogieBuilder.messageAttr(start, stop, "this postcondition may be violated")),
            translateForallExpression(expr, true)
          )
        }
      )
    case _ => (Nil, Nil)
  FunctionTranslationResult(
    funcDef.visibility,
    funcDef.kind,
    boogie.Procedure(
      usedFuncId match
        case None     => funcName
        case Some(id) => BoogieBuilder.identWithId(funcName, id)
      ,
      Seq(":inline 1"),
      params,
      returnParams,
      requires,
      ensures,
      rawParams ++ modifierProcessed.localVars,
      printStmts ++ paramCopyStmts ++ stateMutStmt ++ modifierProcessed.statements
    )
  )

  /** This function translate variable declarations; works for:
    * - state variables
    * - function parameters
    * - local variables
    *
    * Each variable may generate multiple boogie variables:
    * - data: stores user data
    * - lengths: tracks length of arrays (one boogie variable for each level of an array)
    * - sums: tracks sums of aggregated data types
    */
def translateVariableDeclaration(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    varDecl: solidity.VariableDeclaration
): VariableTranslationResult =
  import VariableTranslationResult.BoogieVariableKind as Kind
  val result     = encodeBoogieVars(typeNameToTranslatorType(varDecl.typeName))
  val nameWithId = BoogieBuilder.identWithId(varDecl.name, varDecl.id.get)
  val contents   = for cv <- result.contents yield (cv.buildName(nameWithId), cv.boogieType, Kind.PlainStateVariable())
  val sums = for sv @ SumVar(_, _, depth, assoc) <- result.sums yield
    val name      = sv.buildName(nameWithId)
    val assocInfo = (assoc.buildName(nameWithId), assoc.boogieType)
    val kind = assoc.signedness match
      case Signedness.Signed   => Kind.SumOfSigned(assocInfo, depth)
      case Signedness.Unsigned => Kind.SumOfUnsigned(assocInfo, depth)
      case fallback @ _        => pprint.pprintln(fallback); ???
    (name, sv.boogieType, kind)
  val lengths =
    for lv @ LengthVar(_, indices, arrayDepth) <- result.lengths
    yield (lv.buildName(nameWithId), lv.boogieType, Kind.ArrayLength(arrayDepth, indices.length))
  VariableTranslationResult(contents ++ sums ++ lengths)

def translateAssignment(using translationOptions: TranslationOptions, ctx: SourceUnitContext)(
    assignment: solidity.Assignment
): ExpressionTranslationResult =
  val leftResults  = translateExpression(assignment.leftHandSide)
  val rightResults = translateExpression(assignment.rightHandSide)
  require(leftResults.results.length == rightResults.results.length)
  val assignments = leftResults.results.zip(rightResults.results).flatMap {
    case (ScalarExpression(lhs), ScalarExpression(rhs)) =>
      // TODO: update sum vars managing lhs
      Seq(lhs #:= rhs)
    case (lhs: AggregatedVarGroup, ScalarExpression(rhs)) =>
      val updates = for sumVar <- lhs.sumsManagingOnlyContent yield sumVar #:= sumVar #- lhs.onlyContent #+ rhs
      updates :+ (lhs.onlyContent #:= rhs)
    case (ScalarExpression(lhs), rhs: AggregatedVarGroup) =>
      // TODO: update sum vars managing lhs
      Seq(lhs #:= rhs.onlyContent)
    case (lhs: AggregatedVarGroup, rhs: AggregatedVarGroup) =>
      makeAggregatedAssignment(lhs, rhs)
  }
  ExpressionTranslationResult(
    Nil,
    leftResults.sideEffects ++ rightResults.sideEffects ++ assignments,
    leftResults.localVars ++ rightResults.localVars
  )

def parseSpecString(using sourceUnitCtx: SourceUnitContext)(
    assocNode: solidity.ASTNode,
    spec: String,
    offset: Option[Int]
): Try[(natspec.SpecificationType, natspec.ForallExpression)] =
  import natspec.SpecificationParser as SP
  import natspec.SpecificationType
  SP.parseSpecification(spec) match
    case SP.Success(Some((specType, forallExpr)), _) =>
      // assign identifiers with referencedDeclaration
      val specCtx = SpecificationContext(forallExpr.quantified)
      Try(
        forallExpr.expr.collect { case identifier @ solidity.Identifier(_, name) =>
          identifier.referencedDeclaration = resolveIdentifier(using sourceUnitCtx, specCtx)(assocNode, name) match
            case ResolutionResult.SolidityVariable(varDecl) => varDecl.id.get
            case ResolutionResult.Failure(_) =>
              val regex = ("\\b" + name + "\\b").r
              val start = regex.findFirstMatchIn(spec).get.start
              val stop  = start + name.length
              throw utils.SpecSemanticException(
                Seq(utils.DiagnosticStartStop(f"undefined identifier `$name`", start + offset.get, stop + offset.get))
              )
            case _ => -1
        }
      ) match
        case Success(_)         => Success((specType, forallExpr))
        case Failure(exception) => Failure(exception)
    case SP.Success(None, _) =>
      Failure(utils.NatspecNotSpecException())
    case SP.Failure(msg, next) =>
      Failure(utils.SpecSyntaxException(Seq(utils.DiagnosticOffset(msg, offset.getOrElse(0) + next.offset))))
    case SP.Error(msg, next) => ???

def translateSpecExpression(using
    translationOptions: TranslationOptions,
    ctx: SourceUnitContext
)(specIdentifier: String, arguments: Seq[solidity.ASTNode]): ExpressionTranslationResult =
  specIdentifier match
    case s if "#SUM(#*)".r matches s =>
      // TODO: check if syntax is valid (is every index hole filled?)
      arguments match
        case Seq(e) =>
          def analyze(e: solidity.ASTNode): (Seq[String], Seq[boogie.Expression]) =
            e match
              case solidity.Identifier(ref, name) => (Seq(BoogieBuilder.identWithId(name, ref)), Nil)
              case solidity.IndexAccess(base, index) =>
                val (baseNames, baseIndices) = analyze(base)
                // TODO: consider side effects
                val indexRes = translateExpression(index)
                (baseNames, baseIndices :+ indexRes.expr)
              case solidity.MemberAccess(expr, memberName, refId) =>
                val (exprNames, exprIndices) = analyze(expr)
                (exprNames :+ memberName, exprIndices)
              case fallback @ _ => pprint.pprintln(fallback); ???
          val (names, indices) = analyze(e)
          val boogieName =
            BoogieBuilder.nameOfSumVar(names.mkString(BoogieBuilder.memberSep), s.stripPrefix("#SUM").length)
          ExpressionTranslationResult(
            Seq(ScalarExpression(indices.foldLeft[boogie.Expression](boogieName)(_(_)))),
            Nil,
            Nil
          )
        case _ =>
          throw IllegalArgumentException(f"only one argument is allowed for #SUM; found: $arguments")

def translateForallExpression(using ctx: SourceUnitContext)(
    forallExpr: natspec.ForallExpression,
    replaceFuncArgsToOrigin: Boolean
): boogie.Forall =
  // TODO: handle side-effects
  given TranslationOptions = TranslationOptions(false, false, replaceFuncArgsToOrigin)
  boogie.Forall(
    forallExpr.quantified.toSeq.map { case (name, ty) =>
      (BoogieBuilder.quantifiedVar(name), translateElementaryTypeName(ty))
    },
    translateExpression(forallExpr.expr).expr
  )

def translateElementaryTypeName(typeName: solidity.ElementaryTypeName): boogie.Type =
  typeName.name match
    case name if name.matches("""int(\d*)""")   => boogie.BoogieInt()
    case name if name.matches("""uint(\d*)""")  => boogie.BoogieInt()
    case "address"                              => boogie.Address()
    case "bool"                                 => boogie.BoogieBoolean()
    case "string"                               => boogie.Reference()
    case name if name.matches("""bytes(\d*)""") => boogie.Reference()
    case _                                      => throw NotImplementedError(f"TODO: $typeName")

def simpleTranslatorTypeToBoogieType(translatorType: TranslatorType): boogie.Type =
  translatorType match
    case TranslatorType.SignedInt(_)   => boogie.BoogieInt()
    case TranslatorType.UnsignedInt(_) => boogie.BoogieInt()
    case TranslatorType.EnumInt(_)     => boogie.BoogieInt()
    case TranslatorType.Boolean        => boogie.BoogieBoolean()
    case TranslatorType.Address        => boogie.Address()
    case TranslatorType.StringRef      => boogie.Reference()
    case TranslatorType.BytesRef       => boogie.Reference()
    case fallback @ _                  => pprint.pprintln(fallback); assert(false)
