package translator

enum ResolutionResult:
  case SolidityVariable(varDecl: solidity.VariableDeclaration)
  case SpecificationVirtual(name: String)
  case ForallQuantifiedVariable(name: String)
  case Failure(name: String)

def resolveIdentifier(using
    sourceUnitCtx: SourceUnitContext,
    specCtx: SpecificationContext
)(assocNode: solidity.ASTNode, ident: String): ResolutionResult =
  assocNode match
    // TODO: this is not a spec virtual, may be introduce a new kind of result
    case _ if Set("block", "this", "msg") contains ident => ResolutionResult.SpecificationVirtual(ident)
    case _ if ident contains "#"                         => ResolutionResult.SpecificationVirtual(ident)
    case _ if specCtx.forallQuantified contains ident    => ResolutionResult.ForallQuantifiedVariable(ident)
    case _ =>
      val (baseContracts, nodesFromRoot) = getAssociatedNode(assocNode)
      val localResult = nodesFromRoot.foldRight[Option[solidity.VariableDeclaration]](None) {
        case (_, res @ Some(_)) => res
        case (node, _)          => node.subnodes[solidity.VariableDeclaration].find(_.name == ident)
      }
      lazy val stateResult = baseContracts.foldLeft[Option[solidity.VariableDeclaration]](None) {
        case (res @ Some(_), _) => res
        case (_, contractDef)   => getStateVariableOfContract(contractDef).find(vd => vd.name == ident)
      }
      (localResult, stateResult) match
        case (Some(vd), _) => ResolutionResult.SolidityVariable(vd)
        case (_, Some(vd)) => ResolutionResult.SolidityVariable(vd)
        case _             => ResolutionResult.Failure(ident)

def getAssociatedNode(using ctx: SourceUnitContext)(
    assocNode: solidity.ASTNode
): (Seq[solidity.ContractDefinition], Seq[solidity.ASTNode]) =
  var nodesFromRoot: Seq[solidity.ASTNode] = Nil
  var associatedEncountered                = false
  ctx.astRoot.visit(
    node =>
      (associatedEncountered, node.id == assocNode.id) match
        case (true, _) => ()
        case (false, true) =>
          associatedEncountered = true
          nodesFromRoot :+= node
        case (false, false) =>
          nodesFromRoot :+= node
    ,
    node =>
      associatedEncountered match
        case true  => ()
        case false => nodesFromRoot = nodesFromRoot.init
  )
  val contractDef = nodesFromRoot
    .findLast {
      case _: solidity.ContractDefinition => true
      case _                              => false
    }
    .get
    .asInstanceOf[solidity.ContractDefinition]
  val baseContracts =
    contractDef.linearizedBaseContracts.map(refId => ctx.nodeMap(refId).asInstanceOf[solidity.ContractDefinition])
  (baseContracts, nodesFromRoot)

def getStateVariableOfContract(contractDef: solidity.ContractDefinition): Seq[solidity.VariableDeclaration] =
  contractDef.nodes.collect { case vd: solidity.VariableDeclaration => vd }

def variableIsFunctionParameter(using ctx: SourceUnitContext)(varDecl: solidity.VariableDeclaration): Boolean =
  ctx.astRoot
    .subnodes[solidity.FunctionDefinition]
    .flatMap(fd => fd.parameters.asInstanceOf[solidity.ParameterList].parameters)
    .asInstanceOf[Seq[solidity.VariableDeclaration]]
    .flatMap(_.id.toSeq)
    .contains(varDecl.id.get)
