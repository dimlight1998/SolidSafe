package translator

import translator.dsl.*
import translator.dsl.given

import scala.util.Failure
import scala.util.Success
import scala.util.Try

case class StatementTranslationResult(
    statements: Seq[boogie.Statement],
    localVars: Seq[(String, boogie.Type)]
)

def translateStatement(using
    translationOptions: TranslationOptions,
    sourceUnitCtx: SourceUnitContext,
    funcCtx: FunctionContext,
    placeholderHandler: Option[StatementTranslationResult]
)(
    stmt: solidity.ASTNode
): StatementTranslationResult =
  stmt match
    case solidity.PlaceholderStatement() => placeholderHandler.get
    case solidity.Block(statements) =>
      val (statementsList, localVarsList) =
        statements.map(translateStatement).map(result => (result.statements, result.localVars)).unzip
      StatementTranslationResult(statementsList.flatten, localVarsList.flatten)
    case solidity.VariableDeclarationStatement(decls, None) =>
      val boogieDecls =
        for
          decl    <- decls
          varDecl <- decl.toSeq
          decl    <- translateVariableDeclaration(varDecl.asInstanceOf[solidity.VariableDeclaration]).boogieDecls
          (name, ty, _) = decl
        yield (name, ty)
      StatementTranslationResult(Nil, boogieDecls)
    case solidity.VariableDeclarationStatement(decls, Some(initExpr)) =>
      val initRes = translateExpression(initExpr)
      val results = decls.zip(initRes.results) map {
        case (None, group: AggregatedVarGroup) =>
          val boogieTypes =
            group.contents.map(_._1.boogieType) ++ group.sums.map(_._1.boogieType) ++ group.lengths.map(_._1.boogieType)
          val boogieDecls = boogieTypes.map(ty => (BoogieBuilder.nextTempVar(), ty))
          val boogieInits = boogieDecls.zip(group.flatten).map { case ((v, _), expr) => v #:= expr }
          (boogieDecls, boogieInits)
        case (Some(decl: solidity.VariableDeclaration), group: AggregatedVarGroup) =>
          val declRes = translateVariableDeclaration(decl)
          declRes.boogieDecls.zip(group.flatten).map { case ((name, ty, _), expr) => ((name, ty), name #:= expr) }.unzip
        case (Some(decl: solidity.VariableDeclaration), scalar: ScalarExpression) =>
          val declRes = translateVariableDeclaration(decl)
          assert(declRes.boogieDecls.length == 1)
          val (boogieName, ty, _) = declRes.boogieDecls.head
          val boogieInit          = boogieName #:= scalar.expr
          (Seq((boogieName, ty)), Seq(boogieInit))
        case fallback @ _ => pprint.pprintln(fallback); assert(false)
      }
      val boogieDecls = results.flatMap(_._1)
      val boogieInits = results.flatMap(_._2)
      StatementTranslationResult(initRes.sideEffects ++ boogieInits, initRes.localVars ++ boogieDecls)
    case exprStmt: solidity.ExpressionStatement =>
      val result = translateExpression(exprStmt.expression)
      StatementTranslationResult(result.sideEffects, result.localVars)
    case ifStmt: solidity.IfStatement =>
      val condRes = translateExpression(ifStmt.condition)
      val trueRes = translateStatement(ifStmt.trueBody)
      val (boogieIf, falseLocalVars) = ifStmt.falseBody match
        case None => (boogie.If(condRes.expr, trueRes.statements), Nil)
        case Some(stmts) =>
          val falseRes = translateStatement(stmts)
          (boogie.IfElse(condRes.expr, trueRes.statements, falseRes.statements), falseRes.localVars)
      StatementTranslationResult(
        condRes.sideEffects ++ Seq(boogieIf),
        condRes.localVars ++ trueRes.localVars ++ falseLocalVars
      )
    case whileStmt: solidity.WhileStatement =>
      val rawInvariants: Seq[(natspec.ForallExpression, Int, Int)] = whileStmt.documentation match
        case None => Nil
        case Some(doc) =>
          for
            line <- doc.split("\n").toSeq.map(_.strip)
            startLoc = sourceUnitCtx.rawText.get.indexOf(line)
            res <- parseSpecString(whileStmt, line, Some(startLoc)) match
              case Success((natspec.SpecificationType.Invariant, inv)) => Seq(inv)
              case Success(_)                                          => ???
              case Failure(utils.NatspecNotSpecException())            => Nil
              case Failure(e)                                          => throw e
          yield (res, startLoc, startLoc + line.length)
      val invariants = rawInvariants.map(inv => (translateForallExpression(inv(0), false), inv(1), inv(2)))
      val condition  = translateExpression(whileStmt.condition)
      val body       = translateStatement(whileStmt.body)
      // TODO: consider side-effects of condition
      StatementTranslationResult(
        Seq(
          boogie.While(
            condition.expr,
            invariants.map((forall, start, stop) => (Seq(BoogieBuilder.messageAttr(start, stop, "loop_inv")), forall)),
            Nil,
            body.statements
          )
        ),
        condition.localVars ++ body.localVars
      )
    case forStmt: solidity.ForStatement =>
      val rawInvariants: Seq[(natspec.ForallExpression, Int, Int)] = forStmt.documentation match
        case None => Nil
        case Some(doc) =>
          for
            line <- doc.split("\n").toSeq.map(_.strip)
            startLoc = sourceUnitCtx.rawText.get.indexOf(line)
            res <- parseSpecString(forStmt, line, Some(startLoc)) match
              case Success((natspec.SpecificationType.Invariant, inv)) => Seq(inv)
              case Success(_)                                          => ???
              case Failure(utils.NatspecNotSpecException())            => Nil
              case Failure(e)                                          => throw e
          yield (res, startLoc, startLoc + line.length)
      val invariants = rawInvariants.map(inv => (translateForallExpression(inv(0), false), inv(1), inv(2)))
      val initExpr   = translateStatement(forStmt.initializationExpression)
      val condition  = translateExpression(forStmt.condition)
      val body       = translateStatement(forStmt.body)
      val loopExpr   = translateStatement(forStmt.loopExpression)
      // TODO: consider side-effects of condition
      StatementTranslationResult(
        (initExpr.statements :+ boogie
          .While(
            condition.expr,
            invariants.map((forall, start, stop) => (Seq(BoogieBuilder.messageAttr(start, stop, "loop_inv")), forall)),
            Nil,
            body.statements ++ loopExpr.statements
          )),
        condition.localVars ++ initExpr.localVars ++ body.localVars
      )
    case returnStmt: solidity.Return =>
      // assign to boogie's return variables, then return
      returnStmt.expression match
        case Some(expr) =>
          val result = translateExpression(expr)
          val assignRet =
            funcCtx.currentProcReturnParams.zip(result.flatten).map { case ((name, _), expr) => name #:= expr }
          StatementTranslationResult(result.sideEffects ++ assignRet :+ boogie.Return(), result.localVars)
        case None =>
          StatementTranslationResult(Seq(boogie.Return()), Nil)
    case breakStmt: solidity.Break =>
      StatementTranslationResult(Seq(boogie.Break()), Nil)
    case emitStmt: solidity.EmitStatement =>
      // TODO: side effects of expressions should be checked
      StatementTranslationResult(Nil, Nil)
    case revertStmt: solidity.RevertStatement =>
      // TODO: side effects of expressions should be checked
      StatementTranslationResult(Seq(boogie.Assume(Seq(":reason \"transaction reverted\""), false)), Nil)
    case _ =>
      println(stmt)
      // TODO: implement other statements
      ???
