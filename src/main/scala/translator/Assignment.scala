package translator

import boogie.Expression
import translator.dsl.*
import translator.dsl.given

def makeAggregatedAssignment(using
    translationOptions: TranslationOptions,
    ctx: SourceUnitContext
)(lhs: AggregatedVarGroup, rhs: AggregatedVarGroup): Seq[boogie.Assign] =
  // sum variables managing lhs should be updated from rhs
  val managingSumUpdates =
    def getTotal(group: AggregatedVarGroup): Seq[(Boolean, Seq[String], Seq[Expression])] =
      val membersGroups = group.contents.foldLeft[Seq[Seq[String]]](Nil) { case (acc, (cv, _)) =>
        if acc contains cv.members then acc else acc :+ cv.members
      }
      val results = for members <- membersGroups yield
        // try to find in contents
        val tryContent =
          group.contents
            .filter((cv, _) => cv.members == members && cv.signedness != Signedness.Neither)
            .find((cv, idxs) => cv.indices.length == idxs.length)
        tryContent match
          case Some((cv, indices)) => Some((true, cv.members, indices))
          case None =>
            val trySum =
              group.sums.filter((sv, _) => sv.members == members).find((sv, idxs) => sv.indices.length == idxs.length)
            trySum match
              case None                => None
              case Some((sv, indices)) => Some((false, sv.members, indices))
      results.flatten
    val totalsL = getTotal(lhs)
    val totalsR = getTotal(rhs)
    def buildExpr(rootName: String, members: Seq[String], depth: Option[Int], indices: Seq[Expression]) =
      val baseName = (rootName +: members).mkString(BoogieBuilder.memberSep)
      val name = depth match
        case Some(d) => BoogieBuilder.nameOfSumVar(baseName, d)
        case None    => baseName
      indices.foldLeft[boogie.Expression](name)(_(_))
    for
      ((isContentL, membersL, indicesL), (isContentR, membersR, indicesR)) <- totalsL zip totalsR
      if indicesL.length > 0
      topL = buildExpr(lhs.rootName, membersL, if isContentL then None else Some(indicesL.length), indicesL)
      topR = buildExpr(rhs.rootName, membersR, if isContentR then None else Some(indicesR.length), indicesR)
      prefix <- indicesL.inits.toSeq.tail
    yield
      val managing = buildExpr(lhs.rootName, membersL, Some(prefix.length), prefix)
      managing #:= (managing #- topL) #+ topR
  // content of lhs should be copied from rhs
  val contentAssignments =
    lhs.contents.zip(rhs.contents).map { case ((cvL, indicesL), (cvR, indicesR)) =>
      val exprL = cvL.buildExpr(lhs.rootName, indicesL)
      val exprR = cvR.buildExpr(rhs.rootName, indicesR)
      exprL #:= exprR
    }
  // sum variables managed by lhs should be copied from rhs
  val managedSumAssignments =
    lhs.sums.zip(rhs.sums).map { case ((svL, indicesL), (svR, indicesR)) =>
      val exprL = svL.buildExpr(lhs.rootName, indicesL)
      val exprR = svR.buildExpr(rhs.rootName, indicesR)
      exprL #:= exprR
    }
  // length variables managed by lhs should be copied from rhs
  val lengthAssignments =
    lhs.lengths.zip(rhs.lengths).map { case ((lvL, indicesL), (lvR, indicesR)) =>
      val exprL = lvL.buildExpr(lhs.rootName, indicesL)
      val exprR = lvR.buildExpr(rhs.rootName, indicesR)
      exprL #:= exprR
    }
  managingSumUpdates ++ contentAssignments ++ managedSumAssignments ++ lengthAssignments
