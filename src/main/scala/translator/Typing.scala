package translator

enum TranslatorType:
  // represents expressions without types, e.g. delete
  case NoType
  // represents the result of type(some_interface)
  case InterfaceMeta
  case EnumMeta(bound: Int)
  // multi-value returned by functions; at least two
  case MultiPacked(
      first: TranslatorType,
      second: TranslatorType,
      rest: Seq[TranslatorType]
  )
  case SignedInt(width: Int)
  // used for #SUM
  case SignedIntUnbound
  case UnsignedInt(width: Int)
  // used for #SUM
  case UnsignedIntUnbound
  case LiteralInt(value: BigInt)
  case EnumInt(bound: Int)
  case Boolean
  case Address
  case StringRef
  case BytesRef
  case Array(elem: TranslatorType)
  case Mapping(key: TranslatorType, value: TranslatorType)
  case Struct(members: Seq[(String, TranslatorType)])
  case Interface(funcs: Seq[(Int, String, Seq[TranslatorType], Seq[TranslatorType])])

  // TODO: implement this function
  def join(rhs: TranslatorType): TranslatorType = this

def typeNameToTranslatorType(using ctx: SourceUnitContext)(typeName: solidity.ASTNode): TranslatorType =
  import TranslatorType as TT
  typeName match
    case solidity.ElementaryTypeName("int")                            => TT.SignedInt(256)
    case solidity.ElementaryTypeName(name) if name.startsWith("int")   => TT.SignedInt(name.stripPrefix("int").toInt)
    case solidity.ElementaryTypeName("uint")                           => TT.UnsignedInt(256)
    case solidity.ElementaryTypeName(name) if name.startsWith("uint")  => TT.UnsignedInt(name.stripPrefix("uint").toInt)
    case solidity.ElementaryTypeName("bool")                           => TT.Boolean
    case solidity.ElementaryTypeName("address")                        => TT.Address
    case solidity.ElementaryTypeName("string")                         => TT.StringRef
    case solidity.ElementaryTypeName(name) if name.startsWith("bytes") => TT.BytesRef
    case solidity.ArrayTypeName(bt)                                    => TT.Array(typeNameToTranslatorType(bt))
    case solidity.Mapping(kt, vt) => TT.Mapping(typeNameToTranslatorType(kt), typeNameToTranslatorType(vt))
    case solidity.UserDefinedTypeName(refId) =>
      ctx.nodeMap(refId) match
        case solidity.EnumDefinition(_, members) =>
          TT.EnumInt(members.length)
        case solidity.StructDefinition(members) =>
          TT.Struct(members.map(mem =>
            val valDecl = mem.asInstanceOf[solidity.VariableDeclaration]
            (valDecl.name, typeNameToTranslatorType(valDecl.typeName))
          ))
        case contractDef: solidity.ContractDefinition => contractDefToIntefaceType(contractDef)
        case fallback @ _                             => pprint.pprintln(fallback); ???
    case fallback @ _ => pprint.pprintln(fallback); ???

def contractDefToIntefaceType(using ctx: SourceUnitContext)(
    contractDef: solidity.ContractDefinition
): TranslatorType.Interface =
  // collect functions
  val funcSigs = for
    baseRefId <- contractDef.linearizedBaseContracts
    contract = ctx.nodeMap(baseRefId).asInstanceOf[solidity.ContractDefinition]
    function <- contract.collect { case funcDef: solidity.FunctionDefinition => funcDef }
  yield (
    function.id.get,
    function.name,
    function.parameters
      .asInstanceOf[solidity.ParameterList]
      .parameters
      .map(param => typeNameToTranslatorType(param.asInstanceOf[solidity.VariableDeclaration].typeName)),
    function.returnParameters
      .asInstanceOf[solidity.ParameterList]
      .parameters
      .map(param => typeNameToTranslatorType(param.asInstanceOf[solidity.VariableDeclaration].typeName)),
  )
  TranslatorType.Interface(funcSigs)

def typeOfExpression(using ctx: SourceUnitContext)(expr: solidity.ASTNode): TranslatorType =
  import TranslatorType as TT
  expr match
    case solidity.Identifier(_, "this") => TT.Address
    case solidity.Identifier(refId, _) if refId > 0 =>
      ctx.nodeMap(refId) match
        case varDecl: solidity.VariableDeclaration    => typeNameToTranslatorType(varDecl.typeName)
        case contractDef: solidity.ContractDefinition => contractDefToIntefaceType(contractDef)
        case enumDef: solidity.EnumDefinition         => TT.EnumMeta(enumDef.members.length)
        case fallback @ _                             => pprint.pprintln(fallback); ???
    case solidity.Literal(value, "number")    => TT.LiteralInt(BigInt(value))
    case solidity.Literal(_, "bool")          => TT.Boolean
    case solidity.Literal(_, "string")        => TT.StringRef
    case solidity.UnaryOperation("!", sub)    => typeOfExpression(sub)
    case solidity.UnaryOperation("++", sub)   => typeOfExpression(sub)
    case solidity.UnaryOperation("--", sub)   => typeOfExpression(sub)
    case solidity.UnaryOperation("delete", _) => TT.NoType
    case solidity.BinaryOperation(op, lhs, rhs) if Set("+", "-", "*", "/", "**") contains op =>
      typeOfExpression(lhs) join typeOfExpression(rhs)
    case solidity.FunctionCall(solidity.Identifier(_, name), _, "functionCall") if name.startsWith("#SUM") =>
      // TODO: this may be an int or uint
      TT.UnsignedIntUnbound
    case solidity.FunctionCall(solidity.ElementaryTypeNameExpression(typeName), _, "typeConversion") =>
      typeNameToTranslatorType(typeName)
    case solidity.FunctionCall(solidity.ElementaryTypeNameExpression(typeName), _, "functionCall") =>
      typeNameToTranslatorType(typeName)
    case solidity.FunctionCall(solidity.Identifier(refId, _), _, "typeConversion") if refId > 0 =>
      ctx.nodeMap(refId) match
        case contractDef: solidity.ContractDefinition => contractDefToIntefaceType(contractDef)
        case fallback @ _                             => pprint.pprintln(fallback); ???
    case solidity.FunctionCall(solidity.Identifier(refId, _), _, "functionCall") if refId > 0 =>
      val funcDef = ctx.nodeMap(refId).asInstanceOf[solidity.FunctionDefinition]
      val translatorTypes =
        for param <- funcDef.returnParameters.asInstanceOf[solidity.ParameterList].parameters
        yield
          val typeName = param.asInstanceOf[solidity.VariableDeclaration].typeName
          typeNameToTranslatorType(typeName)
      translatorTypes match
        case Nil                     => TT.NoType
        case t :: Nil                => t
        case first :: second :: rest => TT.MultiPacked(first, second, rest)
    case solidity.FunctionCall(solidity.MemberAccess(expr, memberName, refId), params, "functionCall") =>
      typeOfExpression(expr) match
        case TT.Interface(_) =>
          val retTypes = ctx
            .nodeMap(refId.get)
            .asInstanceOf[solidity.FunctionDefinition]
            .returnParameters
            .asInstanceOf[solidity.ParameterList]
            .parameters
            .map(param => typeNameToTranslatorType(param.asInstanceOf[solidity.VariableDeclaration].typeName))
          retTypes match
            case Nil                     => TT.NoType
            case t :: Nil                => t
            case first :: second :: rest => TT.MultiPacked(first, second, rest)
        case fallback @ _ => pprint.pprintln(fallback); ???
    case solidity.FunctionCall(solidity.Identifier(funcId, "type"), Seq(solidity.Identifier(argId, _)), "functionCall")
        if funcId < 0 =>
      ctx.nodeMap(argId) match
        case _: solidity.ContractDefinition => TT.InterfaceMeta
        case fallback @ _                   => pprint.pprintln(fallback); assert(false)
    case solidity.IndexAccess(base, index) =>
      typeOfExpression(base) match
        case TT.Mapping(_, value) => value
        case TT.Array(elem)       => elem
        case fallback @ _         => pprint.pprintln(fallback); ???
    case solidity.MemberAccess(solidity.Identifier(_, "msg"), "value", _)       => TT.UnsignedInt(256)
    case solidity.MemberAccess(solidity.Identifier(_, "msg"), "sender", _)      => TT.Address
    case solidity.MemberAccess(solidity.Identifier(_, "block"), "timestamp", _) => TT.UnsignedInt(256)
    case solidity.MemberAccess(solidity.Identifier(_, "block"), "number", _)    => TT.UnsignedInt(256)
    case solidity.MemberAccess(solidity.Identifier(_, "tx"), "gasprice", _)     => TT.UnsignedInt(256)
    case solidity.MemberAccess(
          solidity.FunctionCall(
            solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName("address")),
            Seq(solidity.Identifier(_, "this")),
            _
          ),
          "balance",
          _
        ) =>
      TT.UnsignedInt(256)
    // TODO: check if this is an array
    case solidity.MemberAccess(_, "length", _) => TT.UnsignedInt(256)
    case solidity.MemberAccess(solidity.FunctionCall(solidity.Identifier(id, "type"), args, "functionCall"), _, _) =>
      // type(xxx).{min, max}
      args match
        case Seq(solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName(name)))
            if name matches """uint(\d*)""" =>
          TT.UnsignedInt(256)
        case Seq(solidity.ElementaryTypeNameExpression(solidity.ElementaryTypeName(name)))
            if name matches """int(\d*)""" =>
          TT.SignedInt(256)
        case fallback @ _ => pprint.pprintln(fallback); ???
    case solidity.MemberAccess(expr, memberName, _) =>
      // general member access
      val exprType = typeOfExpression(expr)
      exprType match
        case TT.EnumMeta(bound) =>
          TT.EnumInt(bound)
        case TT.Struct(members) =>
          members.find((name, _) => name == memberName) match
            case None          => ???
            case Some((_, ty)) => ty
        case fallback @ _ => pprint.pprintln(fallback); ???
    case solidity.TupleExpression(components) =>
      components match
        case Nil         => TT.NoType
        case elem :: Nil => typeOfExpression(elem)
        case first :: second :: rest =>
          val firstType  = typeOfExpression(first)
          val secondType = typeOfExpression(second)
          val restType   = rest.map(typeOfExpression)
          TT.MultiPacked(firstType, secondType, restType)
    case fallback @ _ => pprint.pprintln(fallback); ???
