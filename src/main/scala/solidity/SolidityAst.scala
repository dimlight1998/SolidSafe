package solidity

import io.circe.*
import io.circe.generic.auto.*

import scala.reflect.TypeTest

abstract sealed class ASTNode:
  var id: Option[Int] = None
  var srcBegin: Int   = 0
  var srcEnd: Int     = 0

  def childrenOf(node: ASTNode): Seq[ASTNode] =
    node match
      case SourceUnit(nodes)          => nodes
      case PragmaDirective()          => Nil
      case StructuredDocumentation(_) => Nil
      case ContractDefinition(_, baseContracts, _, documentation, nodes, _) =>
        baseContracts ++ documentation.toSeq ++ nodes
      case InheritanceSpecifier(baseName)              => Seq(baseName)
      case IdentifierPath(_, _)                        => Nil
      case StructDefinition(members)                   => members
      case ModifierDefinition(_, parameters, body)     => Seq(parameters, body)
      case ModifierInvocation(modifierName, arguments) => modifierName +: arguments.toSeq.flatten
      case EventDefinition(_, parameters)              => Seq(parameters)
      case ErrorDefinition(_, parameters)              => Seq(parameters)
      case EnumDefinition(name, members)               => members
      case EnumValue(_)                                => Nil
      case VariableDeclaration(_, _, typeName)         => Seq(typeName)
      case FunctionDefinition(_, _, _, _, _, modifiers, documentation, body, parameters, returnParameters) =>
        modifiers ++ documentation.toSeq ++ body.toSeq :+ parameters :+ returnParameters
      case ElementaryTypeName(_)                                    => Nil
      case ArrayTypeName(baseType)                                  => Seq(baseType)
      case Mapping(keyType, valueType)                              => Seq(keyType, valueType)
      case UserDefinedTypeName(_)                                   => Nil
      case ParameterList(parameters)                                => parameters
      case Block(statements)                                        => statements
      case VariableDeclarationStatement(declarations, initialValue) => declarations.flatten ++ initialValue.toSeq
      case ExpressionStatement(expression)                          => Seq(expression)
      case IfStatement(condition, trueBody, falseBody)              => Seq(condition, trueBody) ++ falseBody.toSeq
      case WhileStatement(_, condition, body)                       => Seq(condition, body)
      case ForStatement(_, initializationExpression, condition, loopExpression, body) =>
        Seq(initializationExpression, condition, loopExpression, body)
      case EmitStatement(eventCall)                                => Seq(eventCall)
      case RevertStatement(errorCall)                              => Seq(errorCall)
      case PlaceholderStatement()                                  => Nil
      case Conditional(condition, trueExpression, falseExpression) => Seq(condition, trueExpression, falseExpression)
      case NewExpression(typeName)                                 => Seq(typeName)
      case FunctionCall(expression, arguments, _)                  => expression +: arguments
      case FunctionCallOptions(expression, options)                => expression +: options
      case IndexAccess(baseExpression, indexExpression)            => Seq(baseExpression, indexExpression)
      case MemberAccess(expression, _, _)                          => Seq(expression)
      case ElementaryTypeNameExpression(typeName)                  => Seq(typeName)
      case Identifier(_, _)                                        => Nil
      case UnaryOperation(_, subExpression)                        => Seq(subExpression)
      case BinaryOperation(_, leftExpression, rightExpression)     => Seq(leftExpression, rightExpression)
      case Literal(_, _)                                           => Nil
      case TupleExpression(components)                             => components
      case Assignment(_, leftHandSide, rightHandSide)              => Seq(leftHandSide, rightHandSide)
      case Return(expression)                                      => expression.toSeq
      case Break()                                                 => Nil

  def collect[T](f: PartialFunction[ASTNode, T]): Seq[T] =
    lazy val subResults = childrenOf(this).flatMap(_.collect(f))
    f.isDefinedAt(this) match
      case true  => f(this) +: subResults
      case false => subResults

  def visit[T, U](enter: ASTNode => T, leave: ASTNode => U): Seq[(T, U)] =
    val enterRes    = enter(this)
    val childrenRes = childrenOf(this).flatMap(_.visit(enter, leave))
    val leaveRes    = leave(this)
    (enterRes, leaveRes) +: childrenRes

  def subnodes[T <: ASTNode](using TypeTest[ASTNode, T]): Seq[T] =
    collect { case node: T => node }

  def flatMap[T](f: PartialFunction[ASTNode, Seq[T]]): Seq[T] =
    collect(f).flatten

  def withFilter(pred: ASTNode => Boolean): Seq[ASTNode] =
    collect { case node if pred(node) => node }

case class SourceUnit(nodes: Seq[ASTNode])       extends ASTNode
case class PragmaDirective()                     extends ASTNode
case class StructuredDocumentation(text: String) extends ASTNode
case class ContractDefinition(
    name: String,
    baseContracts: Seq[ASTNode],
    linearizedBaseContracts: Seq[Int],
    documentation: Option[ASTNode],
    nodes: Seq[ASTNode],
    contractKind: String
) extends ASTNode
case class InheritanceSpecifier(baseName: ASTNode)                                      extends ASTNode
case class IdentifierPath(name: String, referencedDeclaration: Int)                     extends ASTNode
case class StructDefinition(members: Seq[ASTNode])                                      extends ASTNode
case class ModifierDefinition(name: String, parameters: ASTNode, body: ASTNode)         extends ASTNode
case class ModifierInvocation(modifierName: ASTNode, arguments: Option[Seq[ASTNode]])   extends ASTNode
case class EventDefinition(name: String, parameters: ASTNode)                           extends ASTNode
case class ErrorDefinition(name: String, parameters: ASTNode)                           extends ASTNode
case class EnumDefinition(name: String, members: Seq[ASTNode])                          extends ASTNode
case class EnumValue(name: String)                                                      extends ASTNode
case class VariableDeclaration(name: String, stateVariable: Boolean, typeName: ASTNode) extends ASTNode
case class FunctionDefinition(
    name: String,
    visibility: String,
    kind: String,
    baseFunctions: Option[Seq[Int]],
    stateMutability: String,
    modifiers: Seq[ASTNode],
    documentation: Option[ASTNode],
    body: Option[ASTNode],
    parameters: ASTNode,
    returnParameters: ASTNode
) extends ASTNode
case class ElementaryTypeName(name: String)                extends ASTNode
case class ArrayTypeName(baseType: ASTNode)                extends ASTNode
case class Mapping(keyType: ASTNode, valueType: ASTNode)   extends ASTNode
case class UserDefinedTypeName(referencedDeclaration: Int) extends ASTNode
case class ParameterList(parameters: Seq[ASTNode])         extends ASTNode
case class Block(statements: Seq[ASTNode])                 extends ASTNode
case class VariableDeclarationStatement(declarations: Seq[Option[ASTNode]], initialValue: Option[ASTNode])
    extends ASTNode
case class ExpressionStatement(expression: ASTNode)                                         extends ASTNode
case class IfStatement(condition: ASTNode, trueBody: ASTNode, falseBody: Option[ASTNode])   extends ASTNode
case class WhileStatement(documentation: Option[String], condition: ASTNode, body: ASTNode) extends ASTNode
case class ForStatement(
    documentation: Option[String],
    initializationExpression: ASTNode,
    condition: ASTNode,
    loopExpression: ASTNode,
    body: ASTNode
) extends ASTNode
case class EmitStatement(eventCall: ASTNode)                                                         extends ASTNode
case class RevertStatement(errorCall: ASTNode)                                                       extends ASTNode
case class PlaceholderStatement()                                                                    extends ASTNode
case class Conditional(condition: ASTNode, trueExpression: ASTNode, falseExpression: ASTNode)        extends ASTNode
case class NewExpression(typeName: ASTNode)                                                          extends ASTNode
case class FunctionCall(expression: ASTNode, arguments: Seq[ASTNode], kind: String)                  extends ASTNode
case class FunctionCallOptions(expression: ASTNode, options: Seq[ASTNode])                           extends ASTNode
case class IndexAccess(baseExpression: ASTNode, indexExpression: ASTNode)                            extends ASTNode
case class MemberAccess(expression: ASTNode, memberName: String, referencedDeclaration: Option[Int]) extends ASTNode
case class ElementaryTypeNameExpression(typeName: ASTNode)                                           extends ASTNode
// TODO: make referencedDeclaration immutable
case class Identifier(var referencedDeclaration: Int, name: String)                             extends ASTNode
case class UnaryOperation(operator: String, subExpression: ASTNode)                             extends ASTNode
case class BinaryOperation(operator: String, leftExpression: ASTNode, rightExpression: ASTNode) extends ASTNode
case class Literal(value: String, kind: String)                                                 extends ASTNode
case class TupleExpression(components: Seq[ASTNode])                                            extends ASTNode
case class Assignment(operator: String, leftHandSide: ASTNode, rightHandSide: ASTNode)          extends ASTNode
case class Return(expression: Option[ASTNode])                                                  extends ASTNode
case class Break()                                                                              extends ASTNode

implicit val decoder: Decoder[ASTNode] = (c: HCursor) =>
  def boringDispatch(nodeType: String): Decoder.Result[ASTNode] =
    nodeType match
      case "SourceUnit"                   => c.as[SourceUnit]
      case "PragmaDirective"              => c.as[PragmaDirective]
      case "StructuredDocumentation"      => c.as[StructuredDocumentation]
      case "ContractDefinition"           => c.as[ContractDefinition]
      case "InheritanceSpecifier"         => c.as[InheritanceSpecifier]
      case "IdentifierPath"               => c.as[IdentifierPath]
      case "StructDefinition"             => c.as[StructDefinition]
      case "ModifierDefinition"           => c.as[ModifierDefinition]
      case "ModifierInvocation"           => c.as[ModifierInvocation]
      case "EventDefinition"              => c.as[EventDefinition]
      case "ErrorDefinition"              => c.as[ErrorDefinition]
      case "EnumDefinition"               => c.as[EnumDefinition]
      case "EnumValue"                    => c.as[EnumValue]
      case "VariableDeclaration"          => c.as[VariableDeclaration]
      case "FunctionDefinition"           => c.as[FunctionDefinition]
      case "ElementaryTypeName"           => c.as[ElementaryTypeName]
      case "ArrayTypeName"                => c.as[ArrayTypeName]
      case "Mapping"                      => c.as[Mapping]
      case "UserDefinedTypeName"          => c.as[UserDefinedTypeName]
      case "ParameterList"                => c.as[ParameterList]
      case "Block"                        => c.as[Block]
      case "VariableDeclarationStatement" => c.as[VariableDeclarationStatement]
      case "ExpressionStatement"          => c.as[ExpressionStatement]
      case "IfStatement"                  => c.as[IfStatement]
      case "WhileStatement"               => c.as[WhileStatement]
      case "ForStatement"                 => c.as[ForStatement]
      case "EmitStatement"                => c.as[EmitStatement]
      case "RevertStatement"              => c.as[RevertStatement]
      case "PlaceholderStatement"         => c.as[PlaceholderStatement]
      case "Conditional"                  => c.as[Conditional]
      case "NewExpression"                => c.as[NewExpression]
      case "FunctionCall"                 => c.as[FunctionCall]
      case "FunctionCallOptions"          => c.as[FunctionCallOptions]
      case "IndexAccess"                  => c.as[IndexAccess]
      case "MemberAccess"                 => c.as[MemberAccess]
      case "ElementaryTypeNameExpression" => c.as[ElementaryTypeNameExpression]
      case "Identifier"                   => c.as[Identifier]
      case "UnaryOperation"               => c.as[UnaryOperation]
      case "BinaryOperation"              => c.as[BinaryOperation]
      case "Literal"                      => c.as[Literal]
      case "TupleExpression"              => c.as[TupleExpression]
      case "Assignment"                   => c.as[Assignment]
      case "Return"                       => c.as[Return]
      case "Break"                        => c.as[Break]
      case nodeType                       => Left(DecodingFailure(f"node type $nodeType is not handled yet", c.history))

  def parseSrc(src: String): (Int, Int) =
    val slices = src.split(":")
    (slices(0).toInt, slices(1).toInt)

  for
    nodeType <- c.downField("nodeType").as[String]
    node     <- boringDispatch(nodeType)
    id       <- c.downField("id").as[Int]
    src      <- c.downField("src").as[String]
    (byteBegin, len) = parseSrc(src)
  yield
    node.id = Some(id)
    node.srcBegin = byteBegin
    node.srcEnd = byteBegin + len
    node
