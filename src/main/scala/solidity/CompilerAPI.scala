package solidity

import io.circe.parser.parse

import scala.util.Failure
import scala.util.Success
import scala.util.Try

def compileSolidityToJson(
    solcBinaryFilePath: String,
    solidityFilePath: String
): Try[String] =
  val (exitCode, stdout, stderr) = utils.runProcess(Seq(solcBinaryFilePath, "--combined-json", "ast", solidityFilePath))
  exitCode match
    case 0 => Success(stdout)
    case _ => Failure(utils.SolcErrorException(parseSolcErrorOutput(stderr)))

def jsonStringToJsonObj(jsonString: String): Try[io.circe.JsonObject] =
  parse(jsonString) match
    case Left(err) => Failure(err)
    case Right(parsed) =>
      parsed.asObject match
        case None      => Failure(RuntimeException(f"Unable to treat json string as an object:\n$jsonString"))
        case Some(obj) => Success(obj)

def getSingleAstJson(jsonObject: io.circe.JsonObject): Try[io.circe.Json] =
  (for
    sourcesJson <- jsonObject("sources")
    sourcesObj  <- sourcesJson.asObject
    keysList = sourcesObj.keys.toList
    onlyKey <- keysList match
      case Nil         => None
      case head :: Nil => Some(head)
      case _           => None
    sourceJson <- sourcesObj(onlyKey)
    sourceObj  <- sourceJson.asObject
    astJson    <- sourceObj("AST")
    astObj     <- astJson.asObject
  yield astObj) match
    case None       => Failure(RuntimeException(f"invalid json for translation:\n$jsonObject"))
    case Some(json) => Success(json.toJson)

def parseSolcErrorOutput(error: String): Seq[utils.Diagnostic] =
  val lines = error.linesIterator.toSeq
  val diagnostics = lines.zip(lines.tail).flatMap { case (line1, line2) =>
    line1.matches("^Error: .*") match
      case true =>
        val segments = line2.split(":")
        val line     = segments(segments.length - 2)
        val column   = segments(segments.length - 1)
        Seq(utils.DiagnosticLineColumn(line1, line.toInt - 1, column.toInt))
      case false => Nil
  }
  diagnostics
