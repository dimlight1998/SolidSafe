package natspec

import scala.util.parsing.combinator.*
import solidity as sol

enum SpecificationType:
  case Invariant
  case Precondition
  case Postcondition

case class ForallExpression(quantified: Map[String, sol.ElementaryTypeName], expr: sol.ASTNode)

/** Reference: https://github.com/solidity-parser/antlr/blob/master/Solidity.g4
  */
object SpecificationParser extends RegexParsers with PackratParsers:
  case class Location(posBegin: Int, posEnd: Int)
  private def alternatives[T](parsers: Parser[T]*): Parser[T] = parsers.reduceLeft(_ | _)
  lazy val specification: PackratParser[Option[(SpecificationType, ForallExpression)]] = alternatives(
    "@notice" ~> "invariant" ~> forallExpression ^^ { case expr => Some((SpecificationType.Invariant, expr)) },
    "@notice" ~> "precondition" ~> forallExpression ^^ { case expr => Some((SpecificationType.Precondition, expr)) },
    "@notice" ~> "postcondition" ~> forallExpression ^^ { case expr => Some((SpecificationType.Postcondition, expr)) },
    "@notice" ~> not(alternatives("invariant", "precondition", "postcondition")) ~> regex(".*".r) ^^ { case _ => None},
    not("@notice" ~> alternatives("invariant", "precondition", "postcondition") ~> regex(".*".r)) ^^ { case _ => None }
  )
  lazy val forallExpression: PackratParser[ForallExpression] = alternatives(
    "forall" ~> rep1sep((identifier <~ ":") ~ elementaryTypeName, ",") ~ ("::" ~> expression) ^^ {
      case quantified ~ expr =>
        ForallExpression((quantified map { case ident ~ typeName => (ident.name, typeName) }).toMap, expr)
    },
    expression ^^ { case expr => ForallExpression(Map.empty, expr) }
  )
  lazy val expression: PackratParser[sol.ASTNode] = expressionLogicalImply
  lazy val expressionLogicalImply: PackratParser[sol.ASTNode] =
    expressionLogicalOr ~ ("==>" ~> expressionLogicalOr) ^^ { case lhs ~ rhs =>
      sol.BinaryOperation("||", sol.UnaryOperation("!", lhs), rhs)
    } | expressionLogicalOr
  lazy val expressionLogicalOr: PackratParser[sol.ASTNode] =
    expressionLogicalAnd ~ rep("||" ~> expressionLogicalAnd) ^^ { case lhs ~ rest =>
      rest.foldLeft(lhs)((acc, elem) => sol.BinaryOperation("||", acc, elem))
    } | expressionLogicalAnd
  lazy val expressionLogicalAnd: PackratParser[sol.ASTNode] =
    expressionEqCmp ~ rep("&&" ~> expressionEqCmp) ^^ { case lhs ~ rest =>
      rest.foldLeft(lhs)((acc, elem) => sol.BinaryOperation("&&", acc, elem))
    } | expressionEqCmp
  lazy val expressionEqCmp: PackratParser[sol.ASTNode] =
    expressionOrdCmp ~ ("==" | "!=") ~ expressionOrdCmp ^^ { case lhs ~ op ~ rhs =>
      sol.BinaryOperation(op, lhs, rhs)
    } | expressionOrdCmp
  lazy val expressionOrdCmp: PackratParser[sol.ASTNode] =
    expressionArithAddSub ~ ("<=" | ">=" | "<" | ">") ~ expressionArithAddSub ^^ { case lhs ~ op ~ rhs =>
      sol.BinaryOperation(op, lhs, rhs)
    } | expressionArithAddSub
  lazy val expressionArithAddSub: PackratParser[sol.ASTNode] =
    expressionLogicalNot ~ ("+" | "-") ~ expressionLogicalNot ^^ { case lhs ~ op ~ rhs =>
      sol.BinaryOperation(op, lhs, rhs)
    } | expressionLogicalNot
  lazy val expressionLogicalNot: PackratParser[sol.ASTNode] =
    "!" ~> expressionWithoutOp ^^ { case expr => sol.UnaryOperation("!", expr) } | expressionWithoutOp
  lazy val expressionWithoutOp: PackratParser[sol.ASTNode] = alternatives(
    (expression <~ "[") ~ (expression <~ "]") ^^ { case base ~ index => sol.IndexAccess(base, index) },
    (expression <~ ".") ~ identifier ^^ { case expr ~ ident => sol.MemberAccess(expr, ident.name, None) },
    (expression <~ "(") ~ (functionCallArguments <~ ")") ^^ { case func ~ args =>
      sol.FunctionCall(func, args, "functionCall")
    },
    ("(" ~> expression) <~ ")"
  ) | primaryExpression
  lazy val elementaryTypeName: PackratParser[sol.ElementaryTypeName] =
    alternatives("address", "bool", "int", "uint") ^^ { case name => sol.ElementaryTypeName(name) }
  lazy val primaryExpression: PackratParser[sol.ASTNode] = alternatives(
    elementaryTypeName ^^ (sol.ElementaryTypeNameExpression(_)),
    booleanLiteral,
    hexLiteral,
    numberLiteral,
    identifier
  )
  lazy val functionCallArguments: PackratParser[Seq[sol.ASTNode]] = repsep(expression, ",")
  def booleanLiteral: PackratParser[sol.Literal]                  = ("true" | "false") ^^ (v => sol.Literal(v, "bool"))
  def hexLiteral: PackratParser[sol.Literal]    = regex("""0x[0-9a-fA-F]+""".r) ^^ (v => sol.Literal(v, "address"))
  def numberLiteral: PackratParser[sol.Literal] = regex("""[0-9]+""".r) ^^ (sol.Literal(_, "number"))
  // an extra character '#' is added for verification-only identifiers
  def identifier: PackratParser[sol.Identifier] = regex("""[a-zA-Z$_#][a-zA-Z0-9$_#]*""".r) ^^ (sol.Identifier(-1, _))

  /** Parse a specification string. Identifiers are NOT associated with referenced declarations. */
  def parseSpecification(spec: String): ParseResult[Option[(SpecificationType, ForallExpression)]] =
    parseAll(specification, spec)
