class AggregatedInvariant extends munit.FunSuite:
  def runOn(solidityPath: String): (Int, String, String) =
    // generate a random path under /tmp
    val boogiePath = s"/tmp/${java.util.UUID.randomUUID()}.bpl"
    solidityToBoogie(using "env/solc-static-linux")(
      translator.TranslationOptions(false, false, false),
      solidityPath,
      boogiePath
    )
    boogie.invokeBoogie("env/boogie", boogiePath)

  def check(fileName: String, numVerified: Int, numErrors: Int) =
    test(f"$fileName should have exactly $numVerified verified and $numErrors errors") {
      val (code, stdout, stderr) = runOn(f"contracts/Aggregated-Invariant/$fileName")
      assert(code == 0)
      val verificationResult = stdout.split("\n").last
      val regex              = """^.*(\d+) verified, (\d+) errors?$""".r
      val matchData          = regex.findAllIn(verificationResult).matchData.toSeq
      assert(matchData.length == 1)
      matchData foreach { m =>
        assertEquals(m.group(1).toInt, numVerified)
        assertEquals(m.group(2).toInt, numErrors)
      }
      assert(stderr.isEmpty())
    }

  check("Ballot.sol", 1, 0)
  check("BlindAuction.sol", 1, 0)
  check("Crowdfund.sol", 1, 0)
  check("EnglishAuction.sol", 1, 0)
  check("ERC20.sol", 1, 0)
