import scala.util.Failure
import scala.util.Success
import scala.util.Try
class CveOverflow extends munit.FunSuite:
  def runOn(solidityPath: String): Try[(Int, String, String)] =
    // generate a random path under /tmp
    val boogiePath = s"/tmp/${java.util.UUID.randomUUID()}.bpl"
    for _ <- solidityToBoogie(using "env/solc-static-linux")(
        translator.TranslationOptions(true, false, false),
        solidityPath,
        boogiePath
      )
    yield boogie.invokeBoogie("env/boogie", boogiePath)

  def check(cveId: String, numVerified: Int, numErrors: Int) =
    test(f"$cveId should have exactly $numVerified verified and $numErrors errors") {
      runOn(s"contracts/CVE-Overflow/$cveId/annotated.sol") match
        case Success((code, stdout, stderr)) =>
          assert(code == 0)
          val verificationResult = stdout.split("\n").last
          val regex              = """^.*(\d+) verified, (\d+) errors?$""".r
          val matchData          = regex.findAllIn(verificationResult).matchData.toSeq
          assert(matchData.length == 1)
          matchData foreach { m =>
            assertEquals(m.group(1).toInt, numVerified)
            assertEquals(m.group(2).toInt, numErrors)
          }
          assert(stderr.isEmpty())
        case _ => assert(false)
    }

  def unsupported(cveId: String) =
    test(f"cannot handle $cveId") {
      runOn(s"contracts/CVE-Overflow/$cveId/annotated.sol") match
        case Success(_) => assert(false)
        case _          => assert(true)
    }

  check("CVE-2018-13068", 0, 2)
  check("CVE-2018-13069", 0, 3)
  check("CVE-2018-13070", 0, 3)
  check("CVE-2018-13071", 0, 10)
  check("CVE-2018-13072", 0, 4)
  check("CVE-2018-13073", 0, 5)
  check("CVE-2018-13074", 0, 2)
  check("CVE-2018-13075", 0, 4)
  check("CVE-2018-13076", 0, 4)
  check("CVE-2018-13077", 0, 3)
  check("CVE-2018-13078", 0, 1)
  check("CVE-2018-13079", 0, 3)
  check("CVE-2018-13080", 0, 1)
  check("CVE-2018-13081", 0, 5)
  check("CVE-2018-13082", 0, 3)
  check("CVE-2018-13083", 0, 2)
  check("CVE-2018-13084", 0, 8)
  check("CVE-2018-13085", 0, 9)
  check("CVE-2018-13086", 0, 3)
  check("CVE-2018-13087", 0, 3)
  check("CVE-2018-13088", 0, 3)
  check("CVE-2018-13089", 0, 3)
  check("CVE-2018-13090", 0, 3)
  unsupported("CVE-2018-13091")
  check("CVE-2018-13092", 0, 2)
