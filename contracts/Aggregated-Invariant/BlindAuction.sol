// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

/// @notice invariant forall a: address :: (block.timestamp >= biddingEnd || pendingReturns[a] == 0)
contract BlindAuction {
    // NOTE: bytes32 are replaced with address
    struct Bid {
        address blindedBid;
        uint deposit;
    }

    address payable public beneficiary;
    uint public biddingEnd;
    uint public revealEnd;
    bool public ended;

    mapping(address => Bid[]) public bids;

    address public highestBidder;
    uint public highestBid;

    // Allowed withdrawals of previous bids
    mapping(address => uint) pendingReturns;

    constructor(
        uint biddingTime,
        uint revealTime,
        address payable beneficiaryAddress
    ) {
        beneficiary = beneficiaryAddress;
        biddingEnd = block.timestamp + biddingTime;
        revealEnd = biddingEnd + revealTime;
    }

    /// Place a blinded bid with `blindedBid` =
    /// keccak256(abi.encodePacked(value, fake, secret)).
    /// The sent ether is only refunded if the bid is correctly
    /// revealed in the revealing phase. The bid is valid if the
    /// ether sent together with the bid is at least "value" and
    /// "fake" is not true. Setting "fake" to true and sending
    /// not the exact amount are ways to hide the real bid but
    /// still make the required deposit. The same address can
    /// place multiple bids.
    function bid(address blindedBid)
        external
        payable
    {
        if (block.timestamp >= biddingEnd) revert();
        bids[msg.sender].push();
        uint last = bids[msg.sender].length - 1;
        bids[msg.sender][last].blindedBid = blindedBid;
        bids[msg.sender][last].deposit = msg.value;
    }

    /// Reveal your blinded bids. You will get a refund for all
    /// correctly blinded invalid bids and for all bids except for
    /// the totally highest.
    function reveal(
        uint[] calldata values,
        bool[] calldata fakes,
        address[] calldata secrets
    )
        external
    {
        if (block.timestamp <= biddingEnd) revert();
        if (block.timestamp >= revealEnd) revert();
        uint length = bids[msg.sender].length;
        require(values.length == length);
        require(fakes.length == length);
        require(secrets.length == length);

        uint refund;
        for (uint i = 0; i < length; i++) {
            uint value = values[i];
            bool fake = fakes[i];
            // HACK: cannot translate this
            // if (bidToCheck.blindedBid != keccak256(abi.encodePacked(value, fake, secret))) {
            //     // Bid was not actually revealed.
            //     // Do not refund deposit.
            //     continue;
            // }
            refund += bids[msg.sender][i].deposit;
            if (!fake && bids[msg.sender][i].deposit >= value) {
                if (placeBid(msg.sender, value))
                    refund -= value;
            }
            // Make it impossible for the sender to re-claim
            // the same deposit.
            bids[msg.sender][i].blindedBid = address(0);
        }
        payable(msg.sender).transfer(refund);
    }

    /// Withdraw a bid that was overbid.
    function withdraw() external {
        uint amount = pendingReturns[msg.sender];
        if (block.timestamp < biddingEnd) {
            assert(amount == 0);
            return;
        }
        if (amount > 0) {
            // It is important to set this to zero because the recipient
            // can call this function again as part of the receiving call
            // before `transfer` returns (see the remark above about
            // conditions -> effects -> interaction).
            pendingReturns[msg.sender] = 0;

            payable(msg.sender).transfer(amount);
        }
    }

    /// End the auction and send the highest bid
    /// to the beneficiary.
    function auctionEnd()
        external
    {
        if (block.timestamp <= revealEnd) revert();
        if (ended) revert();
        ended = true;
        beneficiary.transfer(highestBid);
    }

    // This is an "internal" function which means that it
    // can only be called from the contract itself (or from
    // derived contracts).
    function placeBid(address bidder, uint value) internal
            returns (bool success)
    {
        if (value <= highestBid) {
            return false;
        }
        if (highestBidder != address(0)) {
            // Refund the previously highest bidder.
            pendingReturns[highestBidder] += highestBid;
        }
        highestBid = value;
        highestBidder = bidder;
        return true;
    }
}
