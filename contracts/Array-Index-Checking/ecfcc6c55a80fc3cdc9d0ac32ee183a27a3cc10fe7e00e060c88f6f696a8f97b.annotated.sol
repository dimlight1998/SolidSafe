// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner,
        address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0),
            "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
interface ERC20 {
    function balanceOf(address account) external view returns (uint256);
    function transfer(address account, uint256 amount) external;
}
interface ERC721 {
    function ownerOf(uint256 tokenId) external returns (address);
}
/// @title Royalties for Non-Fungible Tokens
/// @dev Contract for the split between community royalties and dev royalties
///
contract Royalties is Ownable {
    uint256 public communityClaimed = 0;
    uint256 public creatorClaimed = 0;
    uint256 public creatorRoyalties = 10; // percentage from total NFT price
    uint256 public communityRoyalties = 0; // percentage from total NFT price
    uint256 public collectionSize = 10000;
    address public tokenFeesAddress;
    address public creatorAddress;
    address public artist;
    mapping(uint256 => uint256) private communityClaims;
    mapping(address => uint256) private addressClaims;
    event CommunityClaimed(uint256 tokenID);
    event CreatorClaimed(uint256 amount);
    event Received(address, uint);
    constructor() {
        tokenFeesAddress = 0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7;
        creatorAddress = 0xf29fD03Df2Cb7F81d8Ae4d10A76f8b1C898786BD;
        artist = 0xc0702Ae0374F83fc3bA71CE2B30A323b09EC19da;
    }
    receive() external payable {
        emit Received(msg.sender, msg.value);
    }
    /// @dev set royalties address (weth)
    function setTokenFeesAddress(address _tokenFeesAddress) external onlyOwner {
        tokenFeesAddress = _tokenFeesAddress;
    }
    /// @dev set creator address, can be another contract
    function setCreatorAddress(address _creatorAddress) external onlyOwner {
        creatorAddress = _creatorAddress;
    }
    /// @dev set only smaller collection size, can't increase size
    function setCollectionSize(uint256 _collectionSize) external onlyOwner {
        require(_collectionSize < collectionSize,
            "Cannot increase collection size");
        collectionSize = _collectionSize;
    }
    /// @dev set creator royalties
    function setCreatorRoyalties(uint256 _creatorRoyalties) external onlyOwner {
        creatorRoyalties = _creatorRoyalties;
    }
    /// @dev set creator royalties
    function setCommunityRoyalties(uint256 _communityRoyalties)
        external
        onlyOwner
    {
        communityRoyalties = _communityRoyalties;
    }
    /// @dev get total royalties
    /// @return total royalties
    function getTotalRoyalties() public view returns (uint256) {
        return creatorRoyalties + communityRoyalties;
    }
    /// @dev get royalties split
    /// @return creator roylaties
    /// @return community royalties
    function getRoyalties() public view returns (uint256, uint256) {
        return (creatorRoyalties, communityRoyalties);
    }
    /// @dev get total collected
    /// @return total collected
    function getTotalCollected() public view returns (uint256) {
        uint256 balance = ERC20(tokenFeesAddress).balanceOf(address(this));
        return balance + creatorClaimed + communityClaimed;
    }
    /// @dev get creator balance
    /// @return creator total balance
    function getCreatorBalance() public view returns (uint256) {
        uint256 _creatorRoyalties = (creatorRoyalties * 100) /
            getTotalRoyalties();
        return (getTotalCollected() * _creatorRoyalties) / 100 - creatorClaimed;
    }
    /// @dev get single token total royalties
    /// @return single token royalties
    function getTokenTotalRoyalties() public view returns (uint256) {
        uint256 _communityRoyalties = (communityRoyalties * 100) /
            getTotalRoyalties();
        return
            ((getTotalCollected() * _communityRoyalties) / 100) /
            collectionSize;
    }
    /// @dev get single token balance
    /// @return single token balance
    function getTokenBalance(uint256 tokenID) public view returns (uint256) {
        return getTokenTotalRoyalties() - communityClaims[tokenID];
    }
    /// @dev get token balances for each token from an array of tokenIDs
    function getTokensBalance(uint256[] memory tokenIDs)
        public
        view
        returns (uint256)
    {
        uint256 totalBalance = 0;
        for (uint256 i = 0; i < tokenIDs.length; i++) {
            uint256 balance = getTokenBalance(tokenIDs[i]);
            totalBalance = (totalBalance + balance);
        }
        return totalBalance;
    }
    /// @dev get address tot claims
    /// @return address total claims
    function getAddressClaims(address account) public view returns (uint256) {
        return addressClaims[account];
    }
    /// @dev claim community royalties per token id
    function claimCommunity(uint256 tokenID) public {
        emit CommunityClaimed(tokenID);                
    }
    /// @dev claim community from an array of tokenIDs
    function claimCommunityBatch(uint256[] calldata tokenIDs) external {
        for (uint256 i = 0; i < tokenIDs.length; i++) {
            claimCommunity(tokenIDs[i]);
        }
    }
    /// @dev claim creator royalties
    function claimCreator() external {
        uint256 balance = getCreatorBalance();
        require(balance > 0, "No balance to claim");
        creatorClaimed = creatorClaimed + balance;
        ERC20(tokenFeesAddress).transfer(creatorAddress, balance/2);
        ERC20(tokenFeesAddress).transfer(artist, balance/2);
        emit CreatorClaimed(balance);
    }
    function withdraw() external {
        uint256 balance = address(this).balance;
        (bool os,) = payable(creatorAddress).call{value: balance/2}("");
        require(os);
        (bool transfer,) = payable(artist).call{value: balance/2}("");
        require(transfer);
    }
}