pragma abicoder               v2;
contract C {
    function f(uint256[] calldata s) public pure returns (bytes memory) {
        return abi.encode(s);
    }
    function g(uint256[][2] calldata s, uint256 which) public view returns (bytes memory) {
        return f(s[which]);
    }
    function h(uint8[] calldata s) public pure returns (bytes memory) {
        return abi.encode(s);
    }
    function i(uint8[][2] calldata s, uint256 which) public view returns (bytes memory) {
        return h(s[which]);
    }
    function j(bytes calldata s) public pure returns (bytes memory) {
        return abi.encode(s);
    }
    function k(bytes[2] calldata s, uint256 which) public view returns (bytes memory) {
        return j(s[which]);
    }
}
// ====
// EVMVersion: >homestead
// ----
// f(uint256[]): 32, 3, 42, 23, 87 -> 32, 160, 32, 3, 42, 23, 87
// h(uint8[]): 32, 3, 42, 23, 87 -> 32, 160, 32, 3, 42, 23, 87
// j(bytes): 32, 3, hex"AB11FF" -> 32, 96, 32, 3, left(0xAB11FF)
