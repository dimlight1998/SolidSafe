// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender,
        address recipient,
        uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
contract StarshipWLCrowdsale {
  // Alpha Starship Token
  IERC20 public token;
  IERC20 public usdToken;
  // Alpha Starship Whitelisted Crowdsale price in dollar. Warning : this price is in dollar, not wei
  // This varable is the only amount that is not in wei in the contract
  uint256 tokenPrice;
  // Maximum amount of tokens that can be bought per address
  uint256 maximumPurchase;
  // Administrator
  address admin;
  // Amount of wei raised
  uint256 public weiRaised;
  // Multi-signature wallet used to revieve the funds
  address wallet;
  // Whitelist
  mapping (address => bool) public isWhitelisted;
  // Maximum tokens
  mapping (address => uint256) public tokensPurchased;
  event TokenPurchase(address indexed purchaser,
    address indexed beneficiary,
    uint256 value,
    uint256 amount);
  constructor(address[] memory _addrs, address _wallet, address _token, address _usdToken, uint256 _price, uint256 _maxPurchase) {
    require(_token != address(0));
    usdToken = IERC20(_usdToken);
    token = IERC20(_token);
    tokenPrice = _price;
    maximumPurchase = _maxPurchase;
    wallet = _wallet;
    // Adding the addresses to the Whitelist
    for(uint256 i = 0; i < _addrs.length; i++){
      isWhitelisted[_addrs[i]] = true;
    }
    admin = msg.sender;
  }
  modifier onlyWhitelisted {
    require(isWhitelisted[msg.sender] == true, "User is not Whitelisted");
    _;
  }
  // -----------------------------------------
  // Crowdsale external interface
  // -----------------------------------------
  receive() external payable {
    revert(); 
  }
  function buyTokens(address _beneficiary, uint256 _weiAmount) onlyWhitelisted public {
    require(_weiAmount > 0, "Amount of tokens purchased must be positive");
    require(tokensPurchased[msg.sender] + _getTokenAmount(_weiAmount) <= maximumPurchase, "Cannot purchase any more tokens");
    _preValidatePurchase(_beneficiary, _weiAmount);
    // calculate token amount to be created
    uint256 tokenAmount = _getTokenAmount(_weiAmount);
    // update state
    weiRaised = weiRaised + _weiAmount;
    _processPurchase(_beneficiary, _weiAmount, tokenAmount);
    emit TokenPurchase(msg.sender,
      _beneficiary,
      _weiAmount,
      tokenAmount);
    _updatePurchasingState(_beneficiary, _weiAmount);
    _postValidatePurchase(_beneficiary, _weiAmount);
  }
  // -----------------------------------------
  // Internal interface (extensible)
  // -----------------------------------------
  function _preValidatePurchase(address _beneficiary,
    uint256 _weiAmount)
    internal
  {
    require(_beneficiary != address(0));
    require(_weiAmount != 0);
  }
  function _postValidatePurchase(address _beneficiary,
    uint256 _weiAmount)
    internal
  {
    // optional override
  }
  function _deliverTokens(address _beneficiary,
    uint256 _tokenAmount)
    internal
  {
    token.transfer(_beneficiary, _tokenAmount);
  }
  function _processPurchase(address _beneficiary,
    uint256 _usdTokenAmount,
    uint256 _tokenAmount)
    internal
  {
    // Making the user pay
    require(usdToken.transferFrom(msg.sender, wallet, _usdTokenAmount), "Deposit failed");
    // Increasing purchased tokens
    tokensPurchased[msg.sender] += _tokenAmount;
    // Delivering the tokens
    _deliverTokens(_beneficiary, _tokenAmount);
  }
  function _updatePurchasingState(address _beneficiary,
    uint256 _weiAmount)
    internal
  {
    // optional override
  }
  function _getTokenAmount(uint256 _weiAmount)
    internal view returns (uint256)
  {
    return _weiAmount/tokenPrice; // Amount in wei
  }
  function addWLAddress(address _addr) external {
    require(msg.sender == admin, "You must be admin to execute this function");
    isWhitelisted[_addr] = true;
  }
  function endCrodwsale() external {
    require(msg.sender == admin, "You must be admin to execute this function");
    token.transfer(admin, token.balanceOf(address(this)));
    selfdestruct(payable(admin));
}
}