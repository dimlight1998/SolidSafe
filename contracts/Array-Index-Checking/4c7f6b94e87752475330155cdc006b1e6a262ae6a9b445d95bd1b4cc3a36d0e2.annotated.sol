// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
interface IERC721Receiver {
    function onERC721Received(address operator,
        address from,
        uint256 tokenId,
        bytes calldata data) external returns (bytes4);
}
// File: @openzeppelin/contracts/utils/introspection/IERC165.sol
pragma solidity ^0.8.0;
interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
// File: @openzeppelin/contracts/token/ERC721/IERC721.sol
pragma solidity ^0.8.0;
interface IERC721 is IERC165 {
    event Transfer(address indexed from,
        address indexed to,
        uint256 indexed tokenId);
    event Approval(address indexed owner,
        address indexed approved,
        uint256 indexed tokenId);
    event ApprovalForAll(address indexed owner,
        address indexed operator,
        bool approved);
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId) external;
    function transferFrom(address from,
        address to,
        uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function getApproved(uint256 tokenId)
        external
        view
        returns (address operator);
    function setApprovalForAll(address operator, bool _approved) external;
    function isApprovedForAll(address owner, address operator)
        external
        view
        returns (bool);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId,
        bytes calldata data) external;
}
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _setOwner(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _setOwner(newOwner);
    }
    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
pragma solidity ^0.8.0;
contract NftStaker {
    IERC721 public parentNFT;
    mapping(address => uint256[]) public stakedTokens;
    mapping(address => uint256) public stakedAmount;
    constructor() {
        parentNFT = IERC721(0x7E209eCcD0814200D39474B9553e63E5590bBD07);
    }
    function stake(uint256[] memory _tokenIds) public {
        for (uint256 i = 0; i < _tokenIds.length; i++) {
            parentNFT.safeTransferFrom(msg.sender, address(this), _tokenIds[i]);
            stakedAmount[msg.sender] = stakedAmount[msg.sender] + 1;
            stakedTokens[msg.sender].push(_tokenIds[i]);
        }
    }
    function unstake(uint256[] memory _tokenIds) public {
        for (uint256 i = 0; i < _tokenIds.length; i++) {
            parentNFT.safeTransferFrom(address(this), msg.sender, _tokenIds[i]);
            stakedAmount[msg.sender] = stakedAmount[msg.sender] - 1;
            for (uint256 j = 0; j < stakedTokens[msg.sender].length; j++) {
                if (_tokenIds[i] == stakedTokens[msg.sender][j]) {
                    stakedTokens[msg.sender][j] = stakedTokens[msg.sender][stakedTokens[msg.sender].length - 1];
                    stakedTokens[msg.sender].pop();
                }
            }
        }
    }
    function onERC721Received(address, address, uint256, bytes calldata) external returns(bytes4) {
        return bytes4(keccak256("onERC721Received(address,address,uint256,bytes)"));
    } 
}