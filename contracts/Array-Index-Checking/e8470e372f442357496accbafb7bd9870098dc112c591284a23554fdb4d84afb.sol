// File: @openzeppelin/contracts/utils/Context.sol
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File: @openzeppelin/contracts/access/Ownable.sol
// OpenZeppelin Contracts (last updated v4.7.0) (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    function _checkOwner() internal view virtual {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: @openzeppelin/contracts/token/ERC20/IERC20.sol
// OpenZeppelin Contracts (last updated v4.6.0) (token/ERC20/IERC20.sol)
pragma solidity ^0.8.0;
interface IERC20 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from,
        address to,
        uint256 amount) external returns (bool);
}
// File: src/migration.sol
pragma solidity ^0.8.9;
pragma abicoder v2;
// File: data.sol
interface Staking {
    struct UserInfo {
        uint256 vote;
        uint256 amount;
        uint256 rewardDebt;
        uint256 xGNDrewardDebt;
    }
    function userInfo(uint256 _pid, address _user) external view returns (UserInfo memory);
    function pendingGND(uint256 _pid, address _user) external view returns (uint256);
    function pendingxGND(uint256 _pid, address _user) external view returns (uint256);
}
contract Data is Ownable {
    Staking public stake;
    constructor(address _stakingAddress) {
        stake = Staking(_stakingAddress);
    }
    function getUserStaked(address _user, uint256 _pid) external view returns (uint256) {
        return stake.userInfo(_pid, _user).amount;
    }
    function updateStake(address _stakingAddress) external onlyOwner {
        stake = Staking(_stakingAddress);
    }
}
// File: migration.sol
contract Migration is Ownable {
    Data public dataContract;
    mapping(uint256 => address) public pidToMigrateToken;
    mapping(address => bool) public hasMigrated;
    IERC20 public GNDToken;
    IERC20 public xGNDToken;
    Staking public stake;
    constructor(Data _dataContract, IERC20 _GNDToken, IERC20 _xGNDToken, Staking _stake) {
        dataContract = _dataContract;
        stake =  _stake;
        GNDToken = _GNDToken;
        xGNDToken = _xGNDToken;
    }
    function getgndpending(address _user, uint256 _pid) external view returns (uint256) {
        return stake.pendingGND(_pid, _user);
    }
    function getxgndpending(address _user, uint256 _pid) external view returns (uint256) {
        return stake.pendingxGND(_pid, _user);
    }
    function migrate(uint256[] calldata pids) external {
        require(!hasMigrated[msg.sender], "User has already migrated");
        for (uint256 i = 0; i < pids.length; i++) {
            _migrateSinglePid(msg.sender, pids[i]);
        }
        hasMigrated[msg.sender] = true;
    }
    function migrateSinglePid(uint256 pid) external {
        require(!hasMigrated[msg.sender], "User has already migrated");
        _migrateSinglePid(msg.sender, pid);
        hasMigrated[msg.sender] = true;
    }
    function _migrateSinglePid(address user, uint256 pid) internal {
        require(pid != 0 && pid != 1 && pid != 2 && pid != 4 && pid != 12 && pid != 15, "Invalid pid");
        uint256 pendingGNDAmount;
        uint256 pendingxGNDAmount;
        uint256 stakedAmount = dataContract.getUserStaked(user, pid);
        if (pid == 16){
            pendingGNDAmount = stakedAmount*5e18/6605e18;
            pendingGNDAmount = stakedAmount*5e18/6605e18;
        }
        else {
            pendingGNDAmount = stake.pendingGND(pid, user);
            pendingxGNDAmount = stake.pendingxGND(pid, user);
        }
        if (stakedAmount > 0) {
            IERC20 token = IERC20(pidToMigrateToken[pid]);
            token.transfer(user, stakedAmount);
        }
        if (pendingGNDAmount > 0) {
            GNDToken.transfer(user, pendingGNDAmount);
        }
        if (pendingxGNDAmount > 0) {
            xGNDToken.transfer(user, pendingxGNDAmount);
        }
    }
    function updateDataContract(Data _dataContract) external onlyOwner {
        dataContract = _dataContract;
    }   
     function updateStakeContract(Staking _stakeContract) external onlyOwner {
        stake = _stakeContract;
    }
    function recoverToken(address tokenAddress) external onlyOwner {
        IERC20 token = IERC20(tokenAddress);
        uint256 balance = token.balanceOf(address(this));
        require(balance > 0, "No tokens to recover");
        token.transfer(msg.sender, balance);
    }
    function setPidToMigrateToken(uint256 pid, address token) external onlyOwner {
        require(pid != 0 && pid != 1 && pid != 2 && pid != 4 && pid != 12 && pid != 15, "Invalid pid");
        pidToMigrateToken[pid] = token;
    }
}