pragma solidity ^0.8.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender,
        address recipient,
        uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _setOwner(_msgSender());
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        _setOwner(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _setOwner(newOwner);
    }
    function _setOwner(address newOwner) private {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: ArbiLocker.sol
pragma solidity ^0.8.0;
contract ArbiLocker is Ownable {
    uint counter;
    uint public lpFee;
    uint public ethFee;
    mapping (address => uint[]) public addressToIds;
    mapping (uint => Safe) public idToSafe;
    struct Safe {
        address owner;
        address token;
        uint amount;
        uint unlock;
        bool active;
    }
    constructor() {
        ethFee = 1*(10**17); //0.1 eth
        lpFee = 100; //1%
    }
    function setBurnAmount(uint amount) onlyOwner public {
        ethFee = amount;
    }
    function setFeePercent(uint amount) onlyOwner public {
        lpFee = amount;
    }
    function depositForLP(address user, address token, uint amount, uint length) public returns(uint id) {
        //costs a set percentage of tokens deposited
        require(amount > 1000, "Deposit too low");
        require(length > 0 && length < 10000, "9999 days max lock");
        address _owner = owner();
        uint leftoverAmount;
        uint fee;
        IERC20 lp = IERC20(token);
        lp.transferFrom(msg.sender, address(this), amount);
        fee = amount / lpFee;
        leftoverAmount = amount - fee;
        require((leftoverAmount + fee) <= amount, "Error in rounding");
        lp.transfer(_owner, fee);
        Safe memory newSafe;
        newSafe.owner = user;
        newSafe.token = token;
        newSafe.amount = leftoverAmount;
        newSafe.unlock = block.timestamp + (length * 1 days); 
        newSafe.active = true;
        addressToIds[msg.sender].push(counter);
        idToSafe[counter] = newSafe;
        counter++;
        return(counter -1);
    }
    function depositForEth(address user, address token, uint amount, uint length) public payable returns(uint id) {
        //burns a set amount of ArbiLocker tokens
        require(amount > 1000, "Deposit too low");
        require(length > 0 && length < 10000, "9999 days max lock");
        require(msg.value == ethFee,"Please include fee");
        address _owner = owner();
        IERC20 lp = IERC20(token);
        lp.transferFrom(msg.sender, _owner, amount);
        Safe memory newSafe;
        newSafe.owner = user;
        newSafe.token = token;
        newSafe.amount = amount;
        newSafe.unlock = block.timestamp + (length * 1 days);
        newSafe.active = true;
        addressToIds[msg.sender].push(counter);
        idToSafe[counter] = newSafe;
        counter++;
        return(counter -1);
    }
    function unlock(uint id) public {
        require(idToSafe[id].owner == msg.sender, "Not the owner of this safe");
        require(idToSafe[id].unlock <= block.timestamp, "Still locked");
        IERC20 lp = IERC20(idToSafe[id].token);
        lp.transfer(msg.sender, idToSafe[id].amount);
        idToSafe[id].amount = 0;
        idToSafe[id].active = false;
    }
    function getSafe(uint id) view public returns(Safe memory) {
        Safe memory res = idToSafe[id];
        return(res);
    }
    function getIds(address user) view public returns(uint[] memory) {
        uint total = addressToIds[user].length;
        uint[] memory res = new uint[](total);
        uint i;
        uint ticker;
        while(i < total) {
            address _owner = idToSafe[ticker].owner;
            if(_owner == msg.sender) { 
                res[i] = ticker; 
                i++; 
            }
            ticker++;
        }
        return(res);
    }
}