// File: @openzeppelin/contracts/utils/Context.sol
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File: @openzeppelin/contracts/access/Ownable.sol
// OpenZeppelin Contracts (last updated v4.7.0) (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    function _checkOwner() internal view virtual {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: PauseGuardian.sol
pragma solidity ^0.8;
interface Comptroller {
    function getAllMarkets() external view returns (address[] memory);
    function _setMintPaused(address qiToken, bool state) external returns (bool);
    function _setBorrowPaused(address qiToken, bool state) external returns (bool);
    function _setTransferPaused(bool state) external returns (bool);
    function _setSeizePaused(bool state) external returns (bool);
}
contract PauseGuardian is Ownable {
    Comptroller public constant comptroller = Comptroller(0x486Af39519B4Dc9a7fCcd318217352830E8AD9b4);
    constructor() {
        transferOwnership(0x30d62267874DdA4D32Bb28ddD713f77d1aa99159);
    }
    function pauseMintingAndBorrowingForAllMarkets() external onlyOwner {
        address[] memory allMarkets = comptroller.getAllMarkets();
        uint marketCount = allMarkets.length;
        for (uint i; i < marketCount; ++i) {
            comptroller._setMintPaused(allMarkets[i], true);
            comptroller._setBorrowPaused(allMarkets[i], true);
        }
    }
    function pauseMintingAndBorrowingForMarket(address qiToken) external onlyOwner {
        comptroller._setMintPaused(qiToken, true);
        comptroller._setBorrowPaused(qiToken, true);
    }
    function pauseMinting(address qiToken) external onlyOwner {
        comptroller._setMintPaused(qiToken, true);
    }
    function pauseBorrowing(address qiToken) external onlyOwner {
        comptroller._setBorrowPaused(qiToken, true);
    }
    function pauseTransfers() external onlyOwner {
        comptroller._setTransferPaused(true);
    }
    function pauseLiquidations() external onlyOwner {
        comptroller._setSeizePaused(true);
    }
}