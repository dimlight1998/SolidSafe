// SPDX-License-Identifier: MIT
// File: @openzeppelin/contracts/utils/Context.sol
// OpenZeppelin Contracts v4.4.1 (utils/Context.sol)
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        return msg.data;
    }
}
// File: @openzeppelin/contracts/access/Ownable.sol
// OpenZeppelin Contracts (last updated v4.7.0) (access/Ownable.sol)
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);
    constructor() {
        _transferOwnership(_msgSender());
    }
    modifier onlyOwner() {
        _checkOwner();
        _;
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    function _checkOwner() internal view virtual {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
    }
    function renounceOwnership() public virtual onlyOwner {
        _transferOwnership(address(0));
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        _transferOwnership(newOwner);
    }
    function _transferOwnership(address newOwner) internal virtual {
        address oldOwner = _owner;
        _owner = newOwner;
        emit OwnershipTransferred(oldOwner, newOwner);
    }
}
// File: @openzeppelin/contracts/utils/introspection/IERC165.sol
// OpenZeppelin Contracts v4.4.1 (utils/introspection/IERC165.sol)
pragma solidity ^0.8.0;
interface IERC165 {
    function supportsInterface(bytes4 interfaceId) external view returns (bool);
}
// File: @openzeppelin/contracts/token/ERC721/IERC721.sol
// OpenZeppelin Contracts (last updated v4.7.0) (token/ERC721/IERC721.sol)
pragma solidity ^0.8.0;
interface IERC721 is IERC165 {
    event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);
    event Approval(address indexed owner, address indexed approved, uint256 indexed tokenId);
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);
    function balanceOf(address owner) external view returns (uint256 balance);
    function ownerOf(uint256 tokenId) external view returns (address owner);
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId,
        bytes calldata data) external;
    function safeTransferFrom(address from,
        address to,
        uint256 tokenId) external;
    function transferFrom(address from,
        address to,
        uint256 tokenId) external;
    function approve(address to, uint256 tokenId) external;
    function setApprovalForAll(address operator, bool _approved) external;
    function getApproved(uint256 tokenId) external view returns (address operator);
    function isApprovedForAll(address owner, address operator) external view returns (bool);
}
// File: @openzeppelin/contracts/token/ERC20/IERC20.sol
// OpenZeppelin Contracts (last updated v4.6.0) (token/ERC20/IERC20.sol)
pragma solidity ^0.8.0;
interface IERC20 {
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address to, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address from,
        address to,
        uint256 amount) external returns (bool);
}
// File: contracts/hecIndex.sol
pragma solidity ^0.8.15;
pragma experimental ABIEncoderV2;
interface IwsHEC{
    function wsHECTosHEC(uint _amount) external view returns (uint);
}
interface IwfNFTs{
    function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256);
}
interface ILockFarm {
    function fnfts(uint256 _tokenId) external view returns (uint256 id ,uint256 amount ,uint256 startTime ,uint256 secs ,uint256 multiplier ,uint256 rewardDebt ,uint256 pendingReward);
}
contract hecCounter is Ownable{
    address public hecAddress = 0x5C4FDfc5233f935f20D2aDbA572F770c2E377Ab0;
    address public shecAddress = 0x75bdeF24285013387A47775828bEC90b91Ca9a5F;
    address public wshecAddress = 0x94CcF60f700146BeA8eF7832820800E2dFa92EdA;
    address public fNFTsAddress = 0x51aEafAC5E4494E9bB2B9e5176844206AaC33Aa3;
    address[] public fNFTsFarms;
    struct FNFTInfo {
        uint256 id;
        uint256 amount;
        uint256 startTime;
        uint256 secs;
        uint256 multiplier;
        uint256 rewardDebt;
        uint256 pendingReward;
    }
    function getHecAmount(address _address) public view returns (uint) {
        uint hecOwnedAmount = IERC20(hecAddress).balanceOf(_address);
        uint shecOwnedAmount = IERC20(shecAddress).balanceOf(_address);
        uint wshecOwnedAmount = IERC20(wshecAddress).balanceOf(_address);
        uint ownedwsHECtosHec = IwsHEC(wshecAddress).wsHECTosHEC(wshecOwnedAmount);
        uint totalHEC = hecOwnedAmount+shecOwnedAmount+ownedwsHECtosHec;
        uint ownedFNFTS = IERC721(fNFTsAddress).balanceOf(_address);
        if (ownedFNFTS > 0) {
            for (uint256 i = 0; i < ownedFNFTS; i++) {
                uint256 _tokenId = IwfNFTs(fNFTsAddress).tokenOfOwnerByIndex(_address, i);
                for (uint256 a = 0; a < fNFTsFarms.length; a++) {
                    (uint256 _id ,uint256 _amount ,uint256 _startTime ,uint256 _secs ,uint256 _multiplier ,uint256 _rewardDebt ,uint256 _pendingReward) = ILockFarm(fNFTsFarms[a]).fnfts(_tokenId);
                    if (_amount > 0) {
                       totalHEC += _amount;
                       break;
                    }
                }
            }
        }        
        return totalHEC;
    }
    function addFarm (address _address) public onlyOwner {
        require(_address != address(0), "address not valid");
        fNFTsFarms.push(_address);
    }
    function removeFarm(uint _index) public onlyOwner{
        require(_index < fNFTsFarms.length, "index out of bound");
        for (uint i = _index; i < fNFTsFarms.length - 1; i++) {
            fNFTsFarms[i] = fNFTsFarms[i + 1];
        }
        fNFTsFarms.pop();
    }
}