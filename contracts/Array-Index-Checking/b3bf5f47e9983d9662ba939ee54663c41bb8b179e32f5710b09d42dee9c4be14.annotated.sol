//SPDX-License-Identifier: GNU AGPLv3
pragma solidity ^0.8.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
contract Donations$420Token {
    address $420Token = 0x720b40CD711DF11E62dfD0c88797Da100d5F09e6;
    address payable smokeOutWallet;
    address payable goodBudsWallet;
    uint initialTime; //number of seconds since 1 Jan 1970 UTC (set in constructor)
    bool[] monthlyDonationStatus; //log of donation status of each month since initialTime
    event Deposit (address sender, uint amount, uint balance, uint time);
    event Donation (address wallet, uint amount, uint balance, uint time); 
    constructor () {
        //setting initialTime as time of contract creation
        initialTime = block.timestamp;
    }
    //a function that allows people to donate to this contract
    //this will be called to deposit the 6.9% of the $420 total supply
    function deposit(address token, uint amount) public payable {
        require(token == $420Token, 'Sorry, those tokens aren\'t accepted here');
        IERC20(token).transferFrom(msg.sender, address(this), amount);
        uint newBalance = IERC20($420Token).balanceOf(address(this)); 
        emit Deposit(msg.sender, amount, newBalance, block.timestamp);
    }
    function donate() public {
        uint timeSinceInitialTime = block.timestamp - initialTime;
        uint currentMonth = timeSinceInitialTime/(4 weeks);
        require(monthlyDonationStatus[currentMonth] == false, 'This month\'s donation has already completed');
        IERC20($420Token).transfer(goodBudsWallet, 4200);
        IERC20($420Token).transfer(smokeOutWallet, 4200);
        monthlyDonationStatus[currentMonth] = true;
        uint newBalance = IERC20($420Token).balanceOf(address(this)); 
        emit Donation(goodBudsWallet, 4200, newBalance, block.timestamp);
        emit Donation(smokeOutWallet, 4200, newBalance, block.timestamp);
    }
}