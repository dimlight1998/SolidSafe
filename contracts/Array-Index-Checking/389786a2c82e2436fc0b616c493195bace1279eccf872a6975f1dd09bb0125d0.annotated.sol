//SPDX-License-Identifier: MIT
// File @openzeppelin/contracts/token/ERC20/[emailprotected]
pragma solidity ^0.8.0;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount)
        external
        returns (bool);
    function allowance(address owner, address spender)
        external
        view
        returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender,
        address recipient,
        uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner,
        address indexed spender,
        uint256 value);
}
// File @openzeppelin/contracts/token/ERC20/extensions/[emailprotected]
pragma solidity ^0.8.0;
interface IERC20Metadata is IERC20 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
}
// File @openzeppelin/contracts/utils/[emailprotected]
pragma solidity ^0.8.0;
abstract contract Context {
    function _msgSender() internal view virtual returns (address) {
        return msg.sender;
    }
    function _msgData() internal view virtual returns (bytes calldata) {
        this; 
        return msg.data;
    }
}
// File @openzeppelin/contracts-upgradeable/proxy/utils/[emailprotected]
// solhint-disable-next-line compiler-version
pragma solidity ^0.8.0;
abstract contract Initializable {
    bool private _initialized;
    bool private _initializing;
    modifier initializer() {
        require(_initializing || !_initialized,
            "Initializable: contract is already initialized");
        bool isTopLevelCall = !_initializing;
        if (isTopLevelCall) {
            _initializing = true;
            _initialized = true;
        }
        _;
        if (isTopLevelCall) {
            _initializing = false;
        }
    }
}
// File @openzeppelin/contracts/access/[emailprotected]
pragma solidity ^0.8.0;
abstract contract Ownable is Context {
    address private _owner;
    event OwnershipTransferred(address indexed previousOwner,
        address indexed newOwner);
    constructor() {
        address msgSender = _msgSender();
        _owner = msgSender;
        emit OwnershipTransferred(address(0), msgSender);
    }
    function owner() public view virtual returns (address) {
        return _owner;
    }
    modifier onlyOwner() {
        require(owner() == _msgSender(), "Ownable: caller is not the owner");
        _;
    }
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }
    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0),
            "Ownable: new owner is the zero address");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}
pragma solidity ^0.8.0;
contract GenericBingo is Ownable {
    address public wallet = address(0x0000000000000000000000);
    address public constant tokenAddress = address(0xDdE78b2e7F3236E873d3a8814D84d849F19cd036);
    uint256 public cardPrice = 1e19;
    bool public isPaused = false;
    mapping(address => bool) hasBought;
    mapping(address => string) bingoData;
    address[] addresses;
    event NewBuy(address buyer);
    event NewGenerate(address indexed buyer, string data);
    constructor() {
        wallet = msg.sender;
    }
    modifier canGenerate() {
        require(isPaused == false, "The game is paused now!");
        _;
    }
    function buy(string memory _data) public canGenerate {
        require(!hasBought[msg.sender], "Already bought!");
        require(_msgSender() != wallet, "You can't use the bingoWallet!");
        addresses.push(msg.sender);
        hasBought[msg.sender] = true;
        bingoData[msg.sender] = _data;
        IERC20(tokenAddress).transferFrom(_msgSender(), wallet, cardPrice);
        emit NewBuy(msg.sender);
    }
    function isBought(address _address) public view returns (bool) {
        return hasBought[_address];
    }
    function getData(address _address) public view returns (string memory) {
        return bingoData[_address];
    }
    function getPlayerCount() public view returns (uint256){
        return addresses.length;
    }
    function restart() public onlyOwner {
        for (uint256 i = 0; i < addresses.length; i++) {
            hasBought[addresses[i]] = false;
            bingoData[addresses[i]] = "";
        }
        delete addresses;
    }
    function pause() public onlyOwner {
        isPaused = true;
    }
    function resume() public onlyOwner {
        isPaused = false;
    }
    function setWallet(address _w) public onlyOwner {
        wallet = _w;
    }
    function setPrice(uint256 _price) public onlyOwner {
        cardPrice = _price;
    }
}