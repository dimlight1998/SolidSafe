//SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;
interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);
}
interface IERC20Metadata is IERC20 {
    function name() external view returns (string memory);
    function symbol() external view returns (string memory);
    function decimals() external view returns (uint8);
}
contract Airdropper {
    //send the same amount of tokens to a bunch of addresses
    function airdropTokens(address token, address[] calldata targets, uint256 amount) external {
        //check that send will not fail for lack of funds / allowance
        uint256 totalToSend = targets.length * amount;
        uint256 userBalance = IERC20(token).balanceOf(msg.sender);
        uint256 userAllowance = IERC20(token).allowance(msg.sender, address(this));
        require(totalToSend <= userAllowance, "Airdropper: insufficient allowance. check that you have approved this contract to transfer tokens");
        require(totalToSend <= userBalance, "Airdropper: insufficient token funds in wallet");
        //perform sends
        for(uint256 j = 0; j < targets.length; j++) {
            IERC20(token).transferFrom(msg.sender, targets[j], amount);
        }
    }   
    //send different amounts of tokens to various addresses, e.g. targets[5] receives amounts[5] tokens
    function multisendTokens(address token, address[] calldata targets, uint256[] calldata amounts) external {
        //sanity check on inputs
        require(targets.length == amounts.length, "Airdropper: input length mismatch");
        //check that send will not fail for lack of funds / allowance
        uint256 totalToSend = 0;
        uint256 userBalance = IERC20(token).balanceOf(msg.sender);
        uint256 userAllowance = IERC20(token).allowance(msg.sender, address(this));
        for(uint256 i = 0; i < amounts.length; i++) {
            totalToSend += amounts[i];
        }
        require(totalToSend <= userAllowance, "Airdropper: insufficient allowance. check that you have approved this contract to transfer tokens");
        require(totalToSend <= userBalance, "Airdropper: insufficient token funds in wallet");
        //perform sends
        for(uint256 j = 0; j < amounts.length; j++) {
            IERC20(token).transferFrom(msg.sender, targets[j], amounts[j]);
        }
    }
    function multisendTokensWithSanityCheck(address token, address[] calldata targets, uint256[] calldata amounts, uint256 totalAmount) external {
        //sanity check on inputs
        require(targets.length == amounts.length, "Airdropper: input length mismatch");
        //check that send will not fail for lack of funds / allowance
        uint256 totalToSend = 0;
        uint256 userBalance = IERC20(token).balanceOf(msg.sender);
        uint256 userAllowance = IERC20(token).allowance(msg.sender, address(this));
        for(uint256 i = 0; i < amounts.length; i++) {
            totalToSend += amounts[i];
        }
        require(totalToSend <= userAllowance, "Airdropper: insufficient allowance. check that you have approved this contract to transfer tokens");
        require(totalToSend <= userBalance, "Airdropper: insufficient token funds in wallet");
        require(totalToSend == totalAmount, "Airdropper: totalAmount does not match sum of amounts array");
        //perform sends
        for(uint256 j = 0; j < amounts.length; j++) {
            IERC20(token).transferFrom(msg.sender, targets[j], amounts[j]);
        }
    }
    //similar to multisendTokensWithSanityCheck, but inputs are in whole number of tokens (not wei!)
    function wholeTokenInputMultisendTokensWithSanityCheck(address token, address[] calldata targets, uint256[] calldata amounts, uint256 totalAmount) external {
        //sanity check on inputs
        require(targets.length == amounts.length, "Airdropper: input length mismatch");
        //check that send will not fail for lack of funds / allowance
        uint256 totalToSend = 0;
        uint256 userBalance = IERC20(token).balanceOf(msg.sender);
        uint256 userAllowance = IERC20(token).allowance(msg.sender, address(this));
        uint256 decimals = IERC20Metadata(token).decimals();
        uint256 multiplier = (10 ** decimals);
        for(uint256 i = 0; i < amounts.length; i++) {
            totalToSend += amounts[i];
        }
        require(totalToSend <= userAllowance, "Airdropper: insufficient allowance. check that you have approved this contract to transfer tokens");
        require(totalToSend <= userBalance, "Airdropper: insufficient token funds in wallet");
        require(totalToSend == totalAmount, "Airdropper: totalAmount does not match sum of amounts array");
        //perform sends
        for(uint256 j = 0; j < amounts.length; j++) {
            IERC20(token).transferFrom(msg.sender, targets[j], (amounts[j] * multiplier));
        }
    }
}