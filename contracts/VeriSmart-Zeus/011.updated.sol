pragma solidity ^0.8.0;

contract owned {
  address public owner;
  constructor() {
    owner = msg.sender;
  }
  modifier onlyOwner {
    if(msg.sender != owner) revert();
    _;
  }
  function transferOwnership(address newOwner) public onlyOwner {
    owner = newOwner;
  }
}

interface tokenRecipient { function receiveApproval(address _from, uint256 _value, address _token, bytes memory _extraData) external; }

contract RTokenBase {
  /* contract info */
  string public standard = 'Token 0.1';
  string public name;
  string public symbol;
  uint8 public decimals;
  uint256 public totalSupply;

  /* maintain a balance mapping of R tokens */
  mapping(address => uint256) public balanceMap;
  mapping(address => mapping(address => uint256)) public allowance;

  /* what to do on transfers */
  event Transfer(address indexed from, address indexed to, uint256 value);

  /* Constructor */
  constructor(uint256 initialAmt, string memory tokenName, string memory tokenSymbol, uint8 decimalUnits) payable {
    balanceMap[msg.sender] = initialAmt;
    totalSupply = initialAmt;
    name = tokenName;
    symbol = tokenSymbol;
    decimals = decimalUnits;
  }

  /* send tokens */
  function transfer(address _to, uint256 _value) virtual public payable {
    if(
        (balanceMap[msg.sender] < _value) ||
        (balanceMap[_to] + _value < balanceMap[_to])
      )
      revert();
    balanceMap[msg.sender] -= _value;
    balanceMap[_to] += _value;
    emit Transfer(msg.sender, _to, _value);
  }

  /* allow other contracts to spend tokens */
  function approve(address _spender, uint256 _value) public returns (bool success) {
    allowance[msg.sender][_spender] = _value;
    return true;
  }

  /* approve and notify */
  function approveAndCall(address _spender, uint256 _value, bytes memory _stuff) public returns (bool success) {
    tokenRecipient spender = tokenRecipient(_spender);
    if(approve(_spender, _value)) {
      spender.receiveApproval(msg.sender, _value, address(this), _stuff);
      return true;
    }
  }

  /* do a transfer */
  function transferFrom(address _from, address _to, uint256 _value) virtual public payable returns (bool success) {
    if(
        (balanceMap[_from] < _value) ||
        (balanceMap[_to] + _value < balanceMap[_to]) ||
        (_value > allowance[_from][msg.sender])
      )
      revert();
    balanceMap[_from] -= _value;
    balanceMap[_to] += _value;
    allowance[_from][msg.sender] -= _value;
    emit Transfer(_from, _to, _value);
    return true;
  }

  /* trap bad sends */
  receive() external payable {
    revert();
  }
}

contract RTokenMain is owned, RTokenBase {
  uint256 public sellPrice;
  uint256 public buyPrice;

  mapping(address => bool) public frozenAccount;

  event FrozenFunds(address target, bool frozen);

  constructor(uint256 initialAmt, string memory tokenName, string memory tokenSymbol, uint8 decimals, address centralMinter)
    RTokenBase(initialAmt, tokenName, tokenSymbol, decimals) {
      if(centralMinter != address(0))
        owner = centralMinter;
      balanceMap[owner] = initialAmt;
    }

  function transfer(address _to, uint256 _value) override public payable {
    if(
        (balanceMap[msg.sender] < _value) ||
        (type(uint).max - balanceMap[_to] <= _value) ||
        (frozenAccount[msg.sender])
      )
      revert();
    balanceMap[msg.sender] -= _value;
    balanceMap[_to] += _value;
    emit Transfer(msg.sender, _to, _value);
  }

  function transferFrom(address _from, address _to, uint256 _value) override public payable returns (bool success) {
    if(
        (frozenAccount[_from]) ||
        (balanceMap[_from] < _value) ||
        (type(uint).max - balanceMap[_to] <= _value) ||
        (_value > allowance[_from][msg.sender])
      )
      revert();
    balanceMap[_from] -= _value;
    balanceMap[_to] += _value;
    allowance[_from][msg.sender] -= _value;
    emit Transfer(_from, _to, _value);
    return true;
  }

  function mintToken(address target, uint256 mintedAmount) public onlyOwner {
    balanceMap[target] += mintedAmount;
    totalSupply += mintedAmount;
    emit Transfer(address(0), address(this), mintedAmount);
    emit Transfer(address(this), target, mintedAmount);
  }

  function freezeAccount(address target, bool freeze) public onlyOwner {
    frozenAccount[target] = freeze;
    emit FrozenFunds(target, freeze);
  }

  function setPrices(uint256 newSellPrice, uint256 newBuyPrice) public onlyOwner {
    sellPrice = newSellPrice;
    buyPrice = newBuyPrice;
  }

  function buy() public payable {
    uint amount = msg.value/buyPrice;
    if(balanceMap[address(this)] < amount)
      revert();
    balanceMap[msg.sender] += amount;
    balanceMap[address(this)] -= amount;
    emit Transfer(address(this), msg.sender, amount);
  }

  function sell(uint256 amount) public {
    if(balanceMap[msg.sender] < amount)
      revert();
    balanceMap[msg.sender] -= amount;
    balanceMap[address(this)] += amount;
    if(!payable(msg.sender).send(amount*sellPrice))
      revert();
    else
      emit Transfer(msg.sender, address(this), amount);
  }
}
