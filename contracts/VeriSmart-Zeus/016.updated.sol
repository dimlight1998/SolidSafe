pragma solidity ^0.8.0;
contract Token{

  event Transfer(address indexed _from, address indexed _to, uint256 _value);

  event Approval(address indexed _onwer,address indexed _spender, uint256 _value);

  function totalSupply() virtual public returns(uint256 ts){}

  function balanceOf(address _owner) virtual public returns (uint256 balance){}

  function transfer(address _to, uint256 _value) virtual public returns(bool success){}

  function transferFrom(address _from, address _to, uint256 _value) virtual public returns (bool success){}

  function approve(address _spender, uint256 _value) virtual public returns(bool success){}

  function allowance(address _owner, uint _spender) public returns(uint256 remaining){}

}

contract StandardToken is Token{
  uint256 public _totalSupply;
  mapping(address => uint256)balances;
  mapping(address =>mapping(address=>uint256))allowed;


  function transfer(address _to, uint256 _value) virtual override public returns(bool success){
    if(balances[msg.sender]>_value && balances[_to]+_value>balances[_to]) {
      balances[msg.sender] -= _value;
      balances[_to] +=_value;
      emit Transfer(msg.sender,_to,_value);
      return true;
    } else {
      return false;
    }
  }

  function transferFrom(address _from, address _to, uint256 _value) override virtual public returns(bool success){
    if(balances[_from]>_value && allowed[_from][msg.sender]>_value && balances[_to]+_value>balances[_to]){
      balances[_from]-=_value;
      allowed[_from][msg.sender]-=_value;
      balances[_to]-=_value;
      emit Transfer(_from,_to,_value);
      return true;
    } else {
      return false;
    }
  }

  function approve(address _spender, uint256 _value) override public returns (bool success){
    allowed[msg.sender][_spender]=_value;
    emit Approval(msg.sender,_spender,_value);
    return true;
  }

  function balanceOf(address _owner) override public view returns (uint256 balance){
    return balances[_owner];
  }

  function allowance(address _onwer,address _spender) public view returns(uint256 all){
    return allowed[_onwer][_spender];
  }
}

contract NinjaToken is StandardToken{
    string public name ="NinjaToken";
    string public version="0.0.1";
    uint public decimals = 18;
    mapping(address=>string) public commit;
    
    address public founder;
    address public admin; 
    bool public fundingLock=true;  // indicate funding status activate or inactivate
    address public fundingAccount;
    uint public startBlock;        //Crowdsale startBlock
    uint public blockDuration;     // Crowdsale blocks duration
    uint public fundingExchangeRate;
    uint public price=10;
    bool public transferLock=false;  // indicate transfer status activate or inactivate

    event Funding(address sender, uint256 eth);
    event Buy(address buyer, uint256 eth);
    
    constructor(address _founder,address _admin){
        founder=_founder;
        admin=_admin;
    }
    
    function changeFunder(address _founder,address _admin) public{
        if(msg.sender!=admin) revert();
        founder=_founder;
        admin=_admin;        
    }
    
    function setFundingLock(bool _fundinglock,address _fundingAccount) public{
        if(msg.sender!=founder) revert();
        fundingLock=_fundinglock;
        fundingAccount=_fundingAccount;
    }
    
    function setFundingEnv(uint _startBlock, uint _blockDuration,uint _fundingExchangeRate) public{
        if(msg.sender!=founder) revert();
        startBlock=_startBlock;
        blockDuration=_blockDuration;
        fundingExchangeRate=_fundingExchangeRate;
    }
    
    function funding() public payable {
        if(fundingLock||block.number<startBlock||block.number>startBlock+blockDuration) revert();
        if(type(uint).max - balances[msg.sender] <= msg.value * fundingExchangeRate || msg.value>msg.value*fundingExchangeRate) revert();
        (bool ok, ) = fundingAccount.call{value: msg.value}("");
        if(!ok) revert();
        balances[msg.sender]+=msg.value*fundingExchangeRate;
        emit Funding(msg.sender,msg.value);
    }
    
    function setPrice(uint _price,bool _transferLock) public{
        if(msg.sender!=founder) revert();
        price=_price;
        transferLock=_transferLock;
    }
    
    function buy(string memory _commit) public payable{
        if(type(uint).max - balances[msg.sender] <= msg.value * price || msg.value > msg.value * price) revert();
        (bool ok,) = fundingAccount.call{value: msg.value}("");
        if(!ok) revert();
        balances[msg.sender]+=msg.value*price;
        commit[msg.sender]=_commit;
        emit Buy(msg.sender,msg.value);
    }
    
    function transfer(address _to, uint256 _value) override public returns(bool success){
        if(transferLock) revert();
        if(balances[msg.sender]>_value && type(uint).max - balances[_to] >= _value) {
          balances[msg.sender] -= _value;
          balances[_to] +=_value;
          emit Transfer(msg.sender,_to,_value);
          return true;
        } else {
          return false;
        }
    }

    function transferFrom(address _from, address _to, uint256 _value) override public returns(bool success){
        if(transferLock) revert();
        if(balances[_from]>_value && allowed[_from][msg.sender]>_value && type(uint).max - balances[_to] >= _value){
          balances[_from]-=_value;
          allowed[_from][msg.sender]-=_value;
          balances[_to]-=_value;
          emit Transfer(_from,_to,_value);
          return true;
        } else {
          return false;
        }
    }

}
