pragma solidity ^0.8.0;

contract IconomiToken {

  event Transfer(address indexed _from, address indexed _to, uint256 _value);
  event Approval(address indexed _owner, address indexed _spender, uint256 _value);
  event BlockLockSet(uint256 _value);
  event NewOwner(address _newOwner);

  modifier onlyOwner {
    if (msg.sender == owner) {
      _;
    }
  }

  modifier blockLock {
    if (!isLocked() || msg.sender == owner) {
      _;
    }
  }

  modifier checkIfToContract(address _to) {
    if(_to != address(this))  {
      _;
    }
  }

  uint256 public totalSupply;
  string public name;
  uint8 public decimals;
  string public symbol;
  string public version = '0.0.1';
  address public owner;
  uint256 public lockedUntilBlock;

  constructor(
    uint256 _initialAmount,
    string memory _tokenName,
    uint8 _decimalUnits,
    string memory _tokenSymbol,
    uint256 _lockedUntilBlock
    ) {
    balances[msg.sender] = _initialAmount;
    totalSupply = _initialAmount;
    name = _tokenName;
    decimals = _decimalUnits;
    symbol = _tokenSymbol;
    lockedUntilBlock = _lockedUntilBlock;
    owner = msg.sender;
  }

  /* Approves and then calls the receiving contract */
  function approveAndCall(address _spender, uint256 _value, bytes memory _extraData) public returns (bool success) {

    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);

    //call the receiveApproval function on the contract you want to be notified. This crafts the function signature manually so one doesn't have to include a contract in here just for this.
    //receiveApproval(address _from, uint256 _value, address _tokenContract, bytes _extraData)
    //it is assumed that when does this that the call *should* succeed, otherwise one would use vanilla approve instead.
    (bool ok, ) = _spender.call(abi.encodeWithSignature("receiveApproval(address,uint256,address,bytes)", msg.sender, _value, this, _extraData));
    if(!ok) { revert(); }
    return true;

  }

  function transfer(address _to, uint256 _value) public blockLock returns (bool success) {
    if (_to != address(this)) {
      if (balances[msg.sender] >= _value && _value > 0) {
        balances[msg.sender] -= _value;
        balances[_to] += _value;
        emit Transfer(msg.sender, _to, _value);
        return true;
      } else {
        return false;
      }
    }
  }

  function transferFrom(address _from, address _to, uint256 _value) public blockLock returns (bool success) {
    if (_to != address(this)) {
      if (balances[_from] >= _value && allowed[_from][msg.sender] >= _value && _value > 0) {
        balances[_to] += _value;
        balances[_from] -= _value;
        allowed[_from][msg.sender] -= _value;
        emit Transfer(_from, _to, _value);
        return true;
      } else {
        return false;
      }
    }
  }

  function balanceOf(address _owner) public view returns (uint256 balance) {
    return balances[_owner];
  }

  function approve(address _spender, uint256 _value) public returns (bool success) {
    allowed[msg.sender][_spender] = _value;
    emit Approval(msg.sender, _spender, _value);
    return true;
  }

  function allowance(address _owner, address _spender) public view returns (uint256 remaining) {
    return allowed[_owner][_spender];
  }

  function setBlockLock(uint256 _lockedUntilBlock) public onlyOwner returns (bool success) {
    lockedUntilBlock = _lockedUntilBlock;
    emit BlockLockSet(_lockedUntilBlock);
    return true;
  }

  function isLocked() public view returns (bool success) {
    return lockedUntilBlock > block.number;
  }

  function replaceOwner(address _newOwner) public onlyOwner returns (bool success) {
    owner = _newOwner;
    emit NewOwner(_newOwner);
    return true;
  }

  mapping (address => uint256) balances;
  mapping (address => mapping (address => uint256)) allowed;
}

/// @notice invariant #SUM(balances) == totalSupply
/// @notice invariant totalSupply <= 115792089237316195423570985008687907853269984665640564039457584007913129639935
contract IconomiTokenTest is IconomiToken {
  constructor(
    uint256 _initialAmount,
    string memory _tokenName,
    uint8 _decimalUnits,
    string memory _tokenSymbol,
    uint256 _lockedUntilBlock
    ) IconomiToken(_initialAmount, _tokenName, _decimalUnits, _tokenSymbol, _lockedUntilBlock) {
  }

  function destruct() public onlyOwner {
    selfdestruct(payable(owner));
  }
}
