pragma solidity ^0.8.0;

contract TimeLockSend {
    address sender;
    address recipient;
    uint256 created;
    uint256 deadline;
    
    constructor(address _sender, address _recipient, uint256 _deadline) payable {
        if (msg.value <= 0) {
            revert();
        }
        sender = _sender;
        recipient = _recipient;
        created = block.timestamp;
        deadline = _deadline;
    }
    
    function withdraw() public {
        if (msg.sender == recipient) {
            selfdestruct(payable(recipient));
        } else if (msg.sender == sender && block.timestamp > deadline) {
            selfdestruct(payable(sender));
        } else {
            revert();
        }
    }
    
    receive() external payable {
        revert();
    }
}

contract SafeSender {
    address owner;
    
    event TimeLockSendCreated(
        address indexed sender, 
        address indexed recipient, 
        uint256 deadline,
        address safeSendAddress
    );
    
    constructor() {
        owner = msg.sender;
    }
    
    function safeSend(address recipient, uint256 timeLimit, address payable ns) public payable returns (address) {
        if (msg.value <= 0 || type(uint).max - block.timestamp <= timeLimit) {
            revert();
        }
        uint256 deadline = block.timestamp + timeLimit;
        // NOTE: cannot translate; hacking
        // TimeLockSend newSend = (new TimeLockSend){value: msg.value}(msg.sender, recipient, deadline);
        TimeLockSend newSend = TimeLockSend(ns);
        payable(newSend).transfer(msg.value);
        if (address(newSend) == address(0)) {
            revert();
        }
        emit TimeLockSendCreated(
            msg.sender,
            recipient,
            deadline,
            address(newSend)
        );
        return address(newSend);
    }
    
    function withdraw() public {
        if (msg.sender != owner) {
            revert();
        }
        if (address(this).balance > 0 && !payable(owner).send(address(this).balance)) {
            revert();
        }
    }
    
    receive() external payable {
        // why yes, thank you.
    }
}
