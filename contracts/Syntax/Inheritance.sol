// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Foo {
    int foox;

    function foo1e() external {}

    function foo2e() external {}

    function foo3i() internal {}
}

contract Bar is Foo {
    mapping(int => int) barx;

    function bar1e() external {}

    function bar2e() external {}

    function bar3i() internal {}
}

contract Buz is Bar {
    int[] buzx;

    function buz1e() external {}

    function buz2e() external {}

    function buz3i() internal {}
}
