// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Sum {
    struct A {
        Pair m;
        bool pad;
    }
    struct B {
        bool pad;
        Pair k;
    }
    struct Pair {
        int x;
        int y;
    }
    function foo() public {
        A[] memory points;
        B memory point;
        points[0].m = point.k;

        Pair[][] memory xs;
        Pair[] memory x;
        xs[0] = x;
    }
}
