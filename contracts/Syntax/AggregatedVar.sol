// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract AggregatedVar {
    struct Transaction {
        address to;
        uint price;
        uint fee;
    }

    struct Account {
        string name;
        Transaction[] transactions;
        address addr;
    }

    struct Bank {
        Account[] accounts;
        uint age;
        string name;
        bool isArchieved;
    }

    function main() public {
        Bank[] memory banks;
        takingBank(false, banks[0], 20);
        bool a;
        Bank[][][] memory recv;
        uint b;
        (a, recv[0][1][2], b) = returningBank();
    }

    function takingBank(bool a, Bank memory bank, uint b) internal {

    }

    function returningBank() internal returns (bool a, Bank memory bank, uint b) {
        Bank[] memory banks;
        return (false, banks[0], 20);
    }
}
