// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract Foo {
    address addr;

    modifier beforeBody() {
        require(msg.sender == addr);
        _;
    }

    modifier afterBody() {
        _;
        assert(msg.sender == addr);
    }

    function foo(uint x, uint y) public beforeBody returns (uint z) {
        return x + y;
    }

    function bar(uint x, uint y) public afterBody returns (uint z) {
        return x + y;
    }
}
