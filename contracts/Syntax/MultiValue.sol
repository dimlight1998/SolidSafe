// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface Foo {
    function foo(uint x, uint y) external returns (uint z1, uint z2, uint z3);
}

contract MultiValue {
    constructor() {
        Foo a;
        uint z1;
        uint z2;
        uint z3;
        (z1, z2, z3) = a.foo(2, 3);
    }

    function bar() public {

    }
}
