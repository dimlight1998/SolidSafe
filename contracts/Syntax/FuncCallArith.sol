// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

contract FuncCallArith {
    constructor() {
        if (foo() + 3 > 0) {
            uint x;
        }
    }

    function foo() public returns (uint) {
        return 0;
    }
}
