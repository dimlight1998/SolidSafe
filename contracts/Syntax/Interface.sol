// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.0;

interface Foo {
    function foo(uint x, uint y) external returns (uint z);
}

contract Interface {
    constructor() {
        address a;
        Foo f;
        uint n = f.foo(4, 5);
        uint m = Foo(a).foo(2, 3);
    }

    function bar() public {

    }
}
